tensorflow>=2.10.0
keras>=2.10.0
matplotlib>=3.5.2
numpy>=1.23.3
qkeras
cmsml
hls4ml