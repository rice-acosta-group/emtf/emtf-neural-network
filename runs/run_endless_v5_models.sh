wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags endless-v5 --dataset phase2"
posfile="../../in/endless_v4/pos_prompt_sample.npz"
negfile="../../in/endless_v4/neg_prompt_sample.npz"
posd0file="../../in/endless_v4/pos_disp_sample.npz"
negd0file="../../in/endless_v4/neg_disp_sample.npz"

## Prompt
#model_name='endless_v5_qpt'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command --train --nodes '28,24,16' \
#  --target 'qpt' --lr 0.001 \
#  "$model_name" "$posfile" "$negfile"
#cd "$wd"
#
#model_name='endless_v5_qpt_quant'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command --train --quantize --sample quant --nodes '28,24,16' \
#  --target 'qpt' --lr 0.0001 \
#  --base-model '../endless_v5_qpt/endless_v5_qpt' \
#  "$model_name" "$posfile" "$negfile"
#cd "$wd"
#
#model_name='endless_v5_qpt_folded'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command --fold --nodes '28,24,16' \
#  --target 'qpt' \
#  --base-model '../endless_v5_qpt_quant/endless_v5_qpt_quant' \
#  "$model_name" "$posfile" "$negfile"
#cd "$wd"
#
#model_name='endless_v5_qpt_quant'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command --train --fold --quantize --sample tune --nodes '28,24,16' \
#  --target 'qpt' --lr 0.0001 \
#  --base-model '../endless_v5_qpt_folded/endless_v5_qpt_folded' \
#  "$model_name" "$posfile" "$negfile"
#cd "$wd"
#
#model_name='endless_v5_qpt_no_act'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command --fold --no-act --nodes '28,24,16' \
#  --target 'qpt' \
#  --base-model '../endless_v5_qpt_quant/endless_v5_qpt_quant' \
#  "$model_name" "$posfile" "$negfile"
#cd "$wd"

# This may not work yet - Need a model without the custom dense layers
# hls prompt NN analysis - Run it in a separate folder (need to copy .h5 files over)
# folder_name='hls_endless_v5_qpt'
# model_name='endless_v5_qpt'
# mkdir -p "$folder_name" 
# cp "$wd/$model_name/$model_name.h5" "$wd/$folder_name"
# cp "$wd/$model_name/${model_name}_weights.h5" "$wd/$folder_name"
# cd "$wd/$folder_name"
# $run_command --build-hls-model \
#   "$model_name" "$posd0file" "$negd0file"
# cd "$wd"


# Displaced
model_name='endless_disp_v5_qpt'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train --nodes '14,12,8' \
  --target 'qpt' --lr 0.001 \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='endless_disp_v5_qpt_quant'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train --quantize --sample quant --nodes '14,12,8' \
  --target 'qpt' --lr 0.0001 \
  --base-model '../endless_disp_v5_qpt/endless_disp_v5_qpt' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='endless_disp_v5_qpt_folded'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --fold --nodes '14,12,8' \
  --target 'qpt' \
  --base-model '../endless_disp_v5_qpt_quant/endless_disp_v5_qpt_quant' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='endless_disp_v5_qpt_quant'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train --fold --quantize --sample tune --nodes '14,12,8' \
  --target 'qpt' --lr 0.0001 \
  --base-model '../endless_disp_v5_qpt_folded/endless_disp_v5_qpt_folded' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='endless_disp_v5_d0'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train --nodes '14,12,8' \
  --target 'd0' --lr 0.001 \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='endless_disp_v5_d0_quant'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train --quantize --sample quant --nodes '14,12,8' \
  --target 'd0' --lr 0.0001 \
  --base-model '../endless_disp_v5_d0/endless_disp_v5_d0' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='endless_disp_v5_d0_folded'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --fold --nodes '14,12,8' \
  --target 'd0' \
  --base-model '../endless_disp_v5_d0_quant/endless_disp_v5_d0_quant' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='endless_disp_v5_d0_quant'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train --fold --quantize --sample tune --nodes '14,12,8' \
  --target 'd0' --lr 0.0001 \
  --base-model '../endless_disp_v5_d0_folded/endless_disp_v5_d0_folded' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='endless_disp_v5'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --merge  \
  --qpt-model '../endless_disp_v5_qpt_quant/endless_disp_v5_qpt_quant' \
  --d0-model '../endless_disp_v5_d0_quant/endless_disp_v5_d0_quant' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='endless_disp_v5_no_act'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --no-act --fold --nodes '28,24,16' \
  --base-model '../endless_disp_v5/endless_disp_v5' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"


# hls disp analysis - Run it in a separate folder (need to copy .h5 files over)
# folder_name='hls_endless_disp_v5'
# model_name='endless_disp_v5'
# mkdir -p "$folder_name" 
# cp "$wd/$model_name/$model_name.h5" "$wd/$folder_name"
# cp "$wd/$model_name/${model_name}_weights.h5" "$wd/$folder_name"
# cd "$wd/$folder_name"
# $run_command --build-hls-model \
#   "$model_name" "$posd0file" "$negd0file"
# cd "$wd"