wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Endless v4
inputfile="../../in/endless_v4/sig_features.npz"
model_pb="../../in/endless_v4/model"

run_name='vld_endless_v4'
mkdir -p "$run_name" && cd "$wd/$run_name"
$run_command check-emulator $model_pb $inputfile --use-protobuf