wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags unconverted-v4"
n4_flatinvdxy_inputfile="../../in/unconverted_v4/neg_disp_flatinvdxy_sample.npz"
p4_flatinvdxy_inputfile="../../in/unconverted_v4/pos_disp_flatinvdxy_sample.npz"
n4_flatvtx_inputfile="../../in/unconverted_v4/neg_disp_flatvtx_sample.npz"
p4_flatvtx_inputfile="../../in/unconverted_v4/pos_disp_flatvtx_sample.npz"
zerobiasfile="../../in/unconverted_v4/run369870_zero_bias_sample.npz"

# ucv4_pt
# model_name='ucv4_pt'
# mkdir -p "$model_name" && cd "$wd/$model_name"
# $run_command --train --nodes '10,8' \
#   --target 'pt' \
#   --use-unsigned-dxy \
#   "$model_name" "$n4_flatvtx_inputfile" "$p4_flatvtx_inputfile"
# cd "$wd"

# # ucv4_dxy
# model_name='ucv4_dxy'
# mkdir -p "$model_name" && cd "$wd/$model_name"
# $run_command --train --nodes '10,8' \
#   --target 'dxy' \
#   --partner-model '../ucv4_pt/ucv4_pt' \
#   --use-unsigned-dxy \
#   "$model_name" "$n4_flatvtx_inputfile" "$p4_flatvtx_inputfile"
# cd "$wd"

# # ucv4
# model_name='ucv4'
# mkdir -p "$model_name" && cd "$wd/$model_name"
# $run_command --merge \
#   --pt-model '../ucv4_pt/ucv4_pt' \
#   --dxy-model '../ucv4_dxy/ucv4_dxy' \
#   --use-unsigned-dxy \
#   "$model_name" "$n4_flatvtx_inputfile" "$p4_flatvtx_inputfile"
# cd "$wd"


# Analysis only
model_name='ucv4'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --merge \
  --use-unsigned-dxy \
  "$model_name" "$n4_flatvtx_inputfile" "$p4_flatvtx_inputfile"
cd "$wd"


# Rate test for pre-trained model
model_name='ucv4'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --test-rate --\
  "$model_name" "$zerobiasfile" "$zerobiasfile"
cd "$wd"

# hls analysis - Run it in a separate folder (need to copy .h5 files over)
# folder_name='hls_ucv4'
# model_name='ucv4'
# mkdir -p "$folder_name" 
# cp "$wd/$model_name/$model_name.h5" "$wd/$folder_name"
# cp "$wd/$model_name/${model_name}_weights.h5" "$wd/$folder_name"
# cd "$wd/$folder_name"
# $run_command --build-hls-model \
#   "$model_name" "$posd0file" "$negd0file"
# cd "$wd"
