wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags endless-v5 --dataset phase1"
posfile="../../in/unconverted_v4/neg_prompt_sample.npz"
negfile="../../in/unconverted_v4/pos_prompt_sample.npz"
posd0file="../../in/unconverted_v4/neg_disp_sample.npz"
negd0file="../../in/unconverted_v4/pos_disp_sample.npz"
zerobiasfile="../../in/unconverted_v4/run369870_zero_bias_sample.npz"
clean_zerobiasfile="../../in/unconverted_v4/clean_modes_zerobias_sample.npz" # Zero bias in same format as regular samples - only used for hls activation ranges

## Prompt
#model_name='uc_v5_qpt'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command --train --nodes '24,20,16' \
#  --target 'qpt' \
#  "$model_name" "$posfile" "$negfile"
#cd "$wd"

# Displaced
model_name='uc_disp_v5_qpt'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train --nodes '12,10,8' \
  --target 'qpt' --lr 0.01 \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='uc_disp_v5_qpt_quant'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train --quantize --sample quant --nodes '12,10,8' \
  --target 'qpt' --lr 0.0001 \
  --base-model '../uc_disp_v5_qpt/uc_disp_v5_qpt' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='uc_disp_v5_qpt_folded'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --fold --nodes '12,10,8' \
  --target 'qpt' \
  --base-model '../uc_disp_v5_qpt_quant/uc_disp_v5_qpt_quant' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='uc_disp_v5_qpt_quant'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train --fold --quantize --sample tune --nodes '12,10,8' \
  --target 'qpt' --lr 0.0001 \
  --base-model '../uc_disp_v5_qpt_folded/uc_disp_v5_qpt_folded' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='uc_disp_v5_d0'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train --nodes '12,10,8' \
  --target 'd0' --lr 0.01 \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='uc_disp_v5_d0_quant'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train --quantize --sample quant --nodes '12,10,8' \
  --target 'd0' --lr 0.0001 \
  --base-model '../uc_disp_v5_d0/uc_disp_v5_d0' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='uc_disp_v5_d0_folded'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --fold --nodes '12,10,8' \
  --target 'd0' \
  --base-model '../uc_disp_v5_d0_quant/uc_disp_v5_d0_quant' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='uc_disp_v5_d0_quant'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train --fold --quantize --sample tune --nodes '12,10,8' \
  --target 'd0' --lr 0.0001 \
  --base-model '../uc_disp_v5_d0_folded/uc_disp_v5_d0_folded' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='uc_disp_v5'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --merge  \
  --qpt-model '../uc_disp_v5_qpt_quant/uc_disp_v5_qpt_quant' \
  --d0-model '../uc_disp_v5_d0_quant/uc_disp_v5_d0_quant' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"

model_name='uc_disp_v5_no_act'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --fold --no-act --nodes '24,20,16' \
  --base-model '../uc_disp_v5/uc_disp_v5' \
  "$model_name" "$posd0file" "$negd0file"
cd "$wd"


# Rate test for pre-trained model
# model_name='uc_disp_v5'
# mkdir -p "$model_name" && cd "$wd/$model_name"
# $run_command --test-rate --\
#   "$model_name" "$zerobiasfile" "$zerobiasfile"
# cd "$wd"

# Run model Analysis only
# model_name='uc_disp_v5'
# mkdir -p "$model_name" && cd "$wd/$model_name"
# $run_command \
#   "$model_name" "$posd0file" "$negd0file"
# cd "$wd"


# hls analysis - Run it in a separate folder (need to copy .h5 files over)
# folder_name='hls_uc_disp_v5'
# model_name='uc_disp_v5'
# mkdir -p "$folder_name" 
# cp "$wd/$model_name/$model_name.h5" "$wd/$folder_name"
# cp "$wd/$model_name/${model_name}_weights.h5" "$wd/$folder_name"
# cd "$wd/$folder_name"
# $run_command --build-hls-model \
#   "$model_name" "$posd0file" "$negd0file"
# cd "$wd"

# Check the activation ranges for a zero-bias file
# folder_name='hls_uc_disp_v5_zerobias_check'
# model_name='uc_disp_v5'
# mkdir -p "$folder_name" 
# cp "$wd/$model_name/$model_name.h5" "$wd/$folder_name"
# cp "$wd/$model_name/${model_name}_weights.h5" "$wd/$folder_name"
# cd "$wd/$folder_name"
# $run_command --build-hls-model \
#   "$model_name" "$clean_zerobiasfile" "$clean_zerobiasfile"
# cd "$wd"
