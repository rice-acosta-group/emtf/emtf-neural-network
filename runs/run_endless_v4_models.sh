wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags endless-v4"
posfile="../../in/endless_v4/pos_prompt_sample.npz"
negfile="../../in/endless_v4/neg_prompt_sample.npz"
bkgfile="../../in/endless_v4/bkg_prompt_sample.npz"
posd0file="../../in/endless_v4/pos_disp_sample.npz"
negd0file="../../in/endless_v4/neg_disp_sample.npz"
bkgd0file="../../in/endless_v4/bkg_disp_sample.npz"

# endless_v4_pt
model_name='endless_v4_pt'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train \
  --target 'qinvpt' \
  "$model_name" "$posfile" "$negfile" "$bkgfile"
cd "$wd"

# endless_disp_v4_pt
model_name='endless_disp_v4_pt'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train \
  --target 'qinvpt' \
  "$model_name" "$posd0file" "$negd0file" "$bkgd0file"
cd "$wd"

# endless_disp_v4_d0
model_name='endless_disp_v4_d0'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --train \
  --target 'd0' \
  "$model_name" "$posd0file" "$negd0file" "$bkgd0file"
cd "$wd"
