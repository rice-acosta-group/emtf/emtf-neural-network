gflags=""
run_command="../bin/run $gflags"
c_inputfile="../in/converted_v1/NN_input_params_FlatXYZ.npz"

# Unconverted v1 Input
n_inputfile="../in/unconverted_v1/NN_input_params_unconverted_negEndcapFlatXYZ_noGEM.npz"
p_inputfile="../in/unconverted_v1/NN_input_params_unconverted_posEndcapFlatXYZ_noGEM.npz"

$run_command compare-v1-inputs \
  --conv-dataset "$c_inputfile" \
  --neg-uconv-dataset "$n_inputfile" \
  --pos-uconv-dataset "$p_inputfile"

# Unconverted v2 Input
n_inputfile="../in/unconverted_v2/NN_input_params_unconverted_negEndcapFlatXYZ_noGEM_noFR_noME11Ring_addMode.npz"
p_inputfile="../in/unconverted_v2/NN_input_params_unconverted_posEndcapFlatXYZ_noGEM_noFR_noME11Ring_addMode.npz"

$run_command compare-v2-inputs \
  --conv-dataset "$c_inputfile" \
  --neg-uconv-dataset "$n_inputfile" \
  --pos-uconv-dataset "$p_inputfile"