import os

project_root = None
project_name = None
debug_en = False


def resource(path):
    return os.path.join(project_root, 'rsc', path)


# PROJECT
logger = None


def get_logger():
    global logger

    # SHORT-CIRCUIT
    if logger is not None:
        return logger

    # INIT
    import logging

    # Copied from https://docs.python.org/2/howto/logging.html
    # create logger
    logger = logging.getLogger('run')
    logger.setLevel(logging.DEBUG)

    # create file handler which logs even debug messages
    fh = logging.FileHandler('run.log')
    fh.setLevel(logging.DEBUG)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)

    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s [%(levelname)-8s] %(message)s')
    fh.setFormatter(formatter)

    formatter = logging.Formatter('[%(levelname)-8s] %(message)s')
    ch.setFormatter(formatter)

    # add the handlers to the logger
    if not len(logger.handlers):
        logger.addHandler(fh)
        logger.addHandler(ch)

    return logger
