from ROOT import TLatex
from ROOT import kWhite

default_text = '#scale[1.4]{CMS #bf{#it{Preliminary}}}'


def draw_fancy_label(x, y, text=None, use_ndc=True):
    if text is None:
        text = default_text

    shadow_label = TLatex()
    shadow_label.SetTextSize(0.035)
    shadow_label.SetTextColorAlpha(kWhite, 0.65)

    if use_ndc:
        shadow_label.DrawLatexNDC(x + 0.003, y - 0.003, text)
    else:
        shadow_label.DrawLatex(x + 0.003, y - 0.003, text)

    real_label = TLatex()
    real_label.SetTextSize(0.035)

    if use_ndc:
        real_label.DrawLatexNDC(x, y, text)
    else:
        real_label.DrawLatex(x, y, text)
