import array
import math

from ROOT import TCanvas
from ROOT import TEfficiency
from ROOT import TF1
from ROOT import TGraph
from ROOT import TH1D
from ROOT import TH2D
from ROOT import TLatex
from ROOT import TLine
from ROOT import TPad
from ROOT import TProfile
from ROOT import gPad
from ROOT import gStyle
from ROOT import kAzure
from ROOT import kBlack
from ROOT import kBlue
from ROOT import kFullDotLarge
from ROOT import kRed
from ROOT import kSpring

from emtfnn.analysis.analyzers import AbstractPlotter
from emtfnn.analysis.labels import draw_fancy_label


class IdentityWrapper(object):

    def __init__(self):
        pass

    def __call__(self, x, par):
        return x[0]


class ROCPlotter(AbstractPlotter):

    def __init__(
            self, name, title, x_title, y_title,
            logx=False, logy=False
    ):
        super().__init__()

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.n = 0
        self.x_arr = list()
        self.y_arr = list()
        self.logx = logx
        self.logy = logy

        self.plot = None
        self.identity_function = None
        self.identity_draw_function = None

        self.consolidated = False

    def add(self, other):
        self.n += other.n
        self.x_arr += other.x_arr
        self.y_arr += other.y_arr

    def fill(self, x, y):
        self.n += 1
        self.x_arr.append(x)
        self.y_arr.append(y)

    def consolidate(self):
        # Short-Circuit: Already consolidated
        if self.consolidated:
            return

        # Generate plot
        self.plot = TGraph(self.n, array.array('d', self.x_arr), array.array('d', self.y_arr))
        self.plot.SetTitle(self.title)

        # Identity
        self.identity_function = IdentityWrapper()

        self.identity_draw_function = TF1(
            self.name + '_identity', self.identity_function,
            -0.15, 1.15, 1)

        # Block
        self.consolidated = True

    def write(self):
        # Consolidate
        self.consolidate()

        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetTopMargin(0.125)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.cd(0)

        frame = plot_canvas.DrawFrame(
            0., 0., 1., 1.,
            self.title
        )
        frame.GetXaxis().SetTitle(self.x_title)
        frame.GetXaxis().SetTitleOffset(1.350)
        frame.GetXaxis().SetRangeUser(0., 1.)
        frame.GetYaxis().SetTitle(self.y_title)
        frame.GetYaxis().SetTitleOffset(1.650)
        frame.GetYaxis().SetMaxDigits(3)
        frame.GetYaxis().SetRangeUser(0., 1.)

        # Draw Frame
        frame.Draw('SAME AXIS')
        frame.Draw('SAME AXIG')

        # Draw 90% Line
        perc90x_line = TLine(0.1, 0., 0.1, 1.)
        perc90x_line.SetLineStyle(10)
        perc90x_line.SetLineWidth(5)
        perc90x_line.SetLineColor(kSpring + 4)
        perc90x_line.Draw('SAME')

        # Draw 90% Line
        perc90y_line = TLine(0., 0.9, 1., 0.9)
        perc90y_line.SetLineStyle(10)
        perc90y_line.SetLineWidth(5)
        perc90y_line.SetLineColor(kSpring + 4)
        perc90y_line.Draw('SAME')

        # Draw Identity
        self.identity_draw_function.SetLineColor(kAzure + 2)
        self.identity_draw_function.SetLineWidth(2)
        self.identity_draw_function.Draw('SAME')

        # Draw Graphs
        self.plot.SetMarkerSize(1.0)
        self.plot.SetMarkerStyle(kFullDotLarge)
        self.plot.Draw('SAME P')

        # GPad
        gPad.Modified()
        gPad.Update()

        # Save
        plot_canvas.SaveAs(self.name + '.png')


class DCSPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 y_low=None, y_up=None,
                 logx=False, logy=False):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.xbins = xbins
        self.logx = logx
        self.logy = logy

        self.n_datasets = 0
        self.datasets = dict()

        self.plot = self.create_plot()

    def create_plot(self, ext=''):
        if all(v is not None for v in [self.nxbins, self.x_low, self.x_up]):
            return TH1D(self.name + ext, self.title, self.nxbins, self.x_low, self.x_up)
        elif all(v is not None for v in [self.xbins]):
            return TH1D(self.name + ext, self.title, self.nxbins, self.xbins)

    def checkout_partial_stats(self, dataset_id, xsec, xsec_error):
        # INIT AND GET DATASET
        dataset = self.datasets.get(dataset_id)

        if dataset is not None:
            return dataset

        self.n_datasets += 1

        dataset = {
            'xsec': xsec,
            'xsec_error': xsec_error,
            'plot': self.create_plot(ext=('_temp%d' % self.n_datasets))
        }

        self.datasets[dataset_id] = dataset

        return dataset

    def add(self, other):
        for other_dataset_id, other_dataset in other.datasets.items():
            dataset = self.datasets.get(other_dataset_id, None)

            if dataset is None:
                dataset = self.checkout_partial_stats(other_dataset_id, other_dataset['xsec'],
                                                      other_dataset['xsec_error'])

            dataset['plot'].Add(other_dataset['plot'])

    def fill(self, x, w, dataset, xsec, xsec_error):
        partial_plot = self.checkout_partial_stats(dataset, xsec, xsec_error)['plot']
        partial_plot.Fill(x, w)

    def consolidate(self):
        total_n = 0
        total_xsec = 0
        total_xsec_error = 0

        for bin_id in range(self.plot.GetNcells()):
            contributions = 0

            sum_n = 0
            sum_x = 0
            sum_sigma2 = 0

            for dataset_id, dataset in self.datasets.items():
                n = dataset['plot'].GetBinContent(bin_id)
                n_error = dataset['plot'].GetBinError(bin_id)

                if n != 0:
                    x = dataset['xsec'] * n
                    sigma2 = (dataset['xsec_error'] * n) ** 2 + (dataset['xsec'] * n_error) ** 2

                    sum_n += n
                    sum_x += x
                    sum_sigma2 += sigma2

                    contributions += 1

            if sum_n != 0:
                # CALCULATE VALUES
                xsec = sum_x
                xsec_error = math.sqrt(sigma2)

                # SUM
                total_n += sum_n
                total_xsec += xsec
                total_xsec_error += xsec_error ** 2

                # NORMALIZE
                width = self.plot.GetXaxis().GetBinWidth(bin_id)
                xsec = xsec / width
                xsec_error = xsec_error / width

                # FILL
                self.plot.SetBinContent(bin_id, xsec)
                self.plot.SetBinError(bin_id, xsec_error)

        print('name: ' + self.name + ' N: ' + str(total_n)
              + ' Sigma: ' + '{:e}'.format(total_xsec)
              + ' Sigma Error: ' + '{:e}'.format(math.sqrt(total_xsec_error)))

    def write(self):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetXaxis().SetTitleOffset(1.350)

        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetYaxis().SetTitleOffset(1.600)
        self.plot.GetYaxis().SetMaxDigits(3)

        if all(v is not None for v in [self.y_low, self.y_up]):
            self.plot.GetYaxis().SetRangeUser(self.y_low, self.y_up)

        self.plot.SetLineColor(kRed)
        self.plot.SetMarkerSize(0.5)
        self.plot.SetMarkerColor(kBlack)
        self.plot.SetMarkerStyle(kFullDotLarge)

        self.plot.Draw("P E1 E0 X0")

        plot_canvas.SaveAs(self.name + '.png')


class DDCSPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title, z_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 nybins=None, y_low=None, y_up=None, ybins=None,
                 z_low=None, z_up=None,
                 logx=False, logy=False, logz=False):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        if ybins is not None:
            nybins = len(ybins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.z_title = z_title
        self.nxbins = nxbins
        self.nybins = nybins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.z_low = z_low
        self.z_up = z_up
        self.xbins = xbins
        self.ybins = ybins
        self.logx = logx
        self.logy = logy
        self.logz = logz

        self.n_datasets = 0
        self.datasets = dict()

        self.plot = self.create_plot()

    def create_plot(self, ext=''):
        if all(v is not None for v in [self.nxbins, self.x_low, self.x_up, self.nybins, self.y_low, self.y_up]):
            return TH2D(self.name + ext, self.title, self.nxbins, self.x_low, self.x_up, self.nybins, self.y_low,
                        self.y_up)
        elif all(v is not None for v in [self.xbins, self.ybins]):
            return TH2D(self.name + ext, self.title, self.nxbins, self.xbins, self.nybins, self.ybins)

    def checkout_partial_stats(self, dataset_id, xsec, xsec_error):
        # INIT AND GET DATASET
        dataset = self.datasets.get(dataset_id)

        if dataset is not None:
            return dataset

        self.n_datasets += 1

        dataset = {
            'xsec': xsec,
            'xsec_error': xsec_error,
            'plot': self.create_plot(ext=('_temp%d' % self.n_datasets))
        }

        self.datasets[dataset_id] = dataset

        return dataset

    def add(self, other):
        for other_dataset_id, other_dataset in other.datasets.items():
            dataset = self.datasets.get(other_dataset_id, None)

            if dataset is None:
                dataset = self.checkout_partial_stats(other_dataset_id, other_dataset['xsec'],
                                                      other_dataset['xsec_error'])

            dataset['plot'].Add(other_dataset['plot'])

    def fill(self, x, y, w, dataset, xsec, xsec_error):
        partial_plot = self.checkout_partial_stats(dataset, xsec, xsec_error)['plot']
        partial_plot.Fill(x, y, w)

    def consolidate(self):
        total_entries = 0
        total_xsec = 0
        total_xsec_error = 0

        for bin_id in range(self.plot.GetNcells()):
            contributions = 0

            sum_n = 0
            sum_x = 0
            sum_sigma2 = 0

            for dataset_id, dataset in self.datasets.items():
                n = dataset['plot'].GetBinContent(bin_id)
                n_error = dataset['plot'].GetBinError(bin_id)

                if n != 0:
                    x = dataset['xsec'] * n
                    sigma2 = (dataset['xsec_error'] * n) ** 2 + (dataset['xsec'] * n_error) ** 2

                    sum_n += n
                    sum_x += x
                    sum_sigma2 += sigma2

                    contributions += 1

            if sum_n != 0:
                # CALCULATE VALUES
                xsec = sum_x
                xsec_error = math.sqrt(sum_sigma2)

                # SUM
                total_entries += sum_n
                total_xsec += xsec
                total_xsec_error += xsec_error ** 2

                # NORMALIZE
                x_width = self.plot.GetXaxis().GetBinWidth(bin_id)
                y_width = self.plot.GetYaxis().GetBinWidth(bin_id)
                xsec = xsec / (x_width * y_width)
                xsec_error = xsec_error / (x_width * y_width)

                # FILL
                self.plot.SetBinContent(bin_id, xsec)
                self.plot.SetBinError(bin_id, xsec_error)

        print('name: ' + self.name + ' N: ' + str(total_entries)
              + ' Sigma: ' + '{:e}'.format(total_xsec)
              + ' Sigma Error: ' + '{:e}'.format(math.sqrt(total_xsec_error)))

    def write(self):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetRightMargin(0.275)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.SetLogz(1 if self.logz else 0)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetXaxis().SetTitleOffset(1.350)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetYaxis().SetTitleOffset(1.450)
        self.plot.GetYaxis().SetMaxDigits(3)
        self.plot.GetZaxis().SetTitle(self.z_title)
        self.plot.GetZaxis().SetTitleOffset(1.650)
        self.plot.GetZaxis().SetMaxDigits(3)

        if self.z_low is not None and self.z_up is not None:
            self.plot.GetZaxis().SetRangeUser(self.z_low, self.z_up)

        self.plot.Draw('COLZ')

        plot_canvas.SaveAs(self.name + '.png')


class RMS2DPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 nybins=None, y_low=None, y_up=None, ybins=None):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        if ybins is not None:
            nybins = len(ybins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.nybins = nybins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.xbins = xbins
        self.ybins = ybins

        if all(v is not None for v in [nxbins, x_low, x_up, nybins, y_low, y_up]):
            self.counts = TH2D(name + '_tempN', title, nxbins, x_low, x_up, nybins, y_low, y_up)
            self.plot = TH2D(name, title, nxbins, x_low, x_up, nybins, y_low, y_up)
        elif all(v is not None for v in [xbins, ybins]):
            self.counts = TH2D(name + '_tempN', title, nxbins, xbins, nybins, ybins)
            self.plot = TH2D(name, title, nxbins, xbins, nybins, ybins)

    def add(self, other):
        self.counts.Add(other.counts)
        self.plot.Add(other.plot)

    def fill(self, x, y, z, w):
        self.counts.Fill(x, y, w)
        self.plot.Fill(x, y, w * z ** 2)

    def consolidate(self):
        for bin_id in range(self.plot.GetNcells()):
            n = self.counts.GetBinContent(bin_id)
            v = self.plot.GetBinContent(bin_id)

            if n != 0:
                self.plot.SetBinContent(bin_id, math.sqrt(v / n))

    def write(self):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetRightMargin(0.125)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1)
        plot_canvas.SetLogy(1)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetXaxis().SetTitleOffset(1.350)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetYaxis().SetTitleOffset(1.45)
        self.plot.GetYaxis().SetMaxDigits(3)
        self.plot.GetZaxis().SetRangeUser(0, 1)
        self.plot.GetZaxis().SetMaxDigits(3)
        self.plot.Draw('COLZ')

        plot_canvas.SaveAs(self.name + '.png')


class RMS1DPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 y_low=None, y_up=None):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.xbins = xbins

        if all(v is not None for v in [x_low, x_up]):
            self.counts = TH1D(name + '_tempN', title, nxbins, x_low, x_up)
            self.plot = TH1D(name, title, nxbins, x_low, x_up)
        elif all(v is not None for v in [xbins]):
            self.counts = TH1D(name + '_tempN', title, nxbins, xbins)
            self.plot = TH1D(name, title, nxbins, xbins)

    def add(self, other):
        self.counts.Add(other.counts)
        self.plot.Add(other.plot)

    def fill(self, x, y, w):
        self.counts.Fill(x, w)
        self.plot.Fill(x, w * y ** 2)

    def consolidate(self):
        for bin_id in range(self.plot.GetNcells()):
            n = self.counts.GetBinContent(bin_id)
            v = self.plot.GetBinContent(bin_id)

            if n != 0:
                self.plot.SetBinContent(bin_id, math.sqrt(v / n))

    def write(self):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetXaxis().SetTitleOffset(1.350)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetYaxis().SetTitleOffset(1.650)
        self.plot.GetYaxis().SetMaxDigits(3)

        if all(v is not None for v in [self.y_low, self.y_up]):
            self.plot.GetYaxis().SetRangeUser(self.y_low, self.y_up)

        self.plot.SetLineColor(kRed)
        self.plot.SetMarkerSize(0.5)
        self.plot.SetMarkerColor(kBlack)
        self.plot.SetMarkerStyle(kFullDotLarge)

        self.plot.Draw("HIST P")

        plot_canvas.SaveAs(self.name + '.png')


class ProfilePlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 y_low=None, y_up=None,
                 logx=False, logy=False):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.xbins = xbins
        self.logx = logx
        self.logy = logy

        if all(v is not None for v in [nxbins, x_low, x_up, y_low, y_up]):
            self.plot = TProfile(name, title, nxbins, x_low, x_up, y_low, y_up)
        elif all(v is not None for v in [xbins, y_low, y_up]):
            self.plot = TProfile(name, title, nxbins, xbins, y_low, y_up)

    def add(self, other):
        self.plot.Add(other.plot)

    def fill(self, x, y, w):
        self.plot.Fill(x, y, w)

    def consolidate(self):
        pass

    def write(self):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetXaxis().SetTitleOffset(1.350)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetYaxis().SetTitleOffset(1.650)
        self.plot.GetYaxis().SetMaxDigits(3)

        if all(v is not None for v in [self.y_low, self.y_up]):
            self.plot.GetYaxis().SetRangeUser(self.y_low, self.y_up)

        self.plot.SetLineColor(kRed)
        self.plot.SetMarkerSize(0.5)
        self.plot.SetMarkerColor(kBlack)
        self.plot.SetMarkerStyle(kFullDotLarge)

        self.plot.Draw('P E')

        plot_canvas.SaveAs(self.name + '.png')


class CorrelationPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 nybins=None, y_low=None, y_up=None, ybins=None,
                 log_scale=True, floor_min=True,
                 density=True,
                 max_val=None, min_val=None):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        if ybins is not None:
            nybins = len(ybins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.nybins = nybins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.xbins = xbins
        self.ybins = ybins
        self.log_scale = (1 if log_scale else 0)
        self.floor_min = floor_min
        self.density = density

        if all(v is not None for v in [nxbins, x_low, x_up, nybins, y_low, y_up]):
            self.plot = TH2D(name, '', nxbins, x_low, x_up, nybins, y_low, y_up)
        elif all(v is not None for v in [xbins, ybins]):
            self.plot = TH2D(name, '', nxbins, xbins, nybins, ybins)

        # CONSTANTS
        self.max_val = max_val
        self.min_val = min_val

    def add(self, other):
        self.plot.Add(other.plot)

    def fill(self, x, y, w=1):
        self.plot.Fill(x, y, w)

    def consolidate(self):
        pass

    def write(self):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        # Get Projections
        top_plot = self.plot.ProjectionX()
        right_plot = self.plot.ProjectionY()

        # Normalize plots
        if self.density:
            if self.plot.GetSumOfWeights() > 0:
                self.plot.Scale(1 / self.plot.GetSumOfWeights())

            if top_plot.GetSumOfWeights() > 0:
                top_plot.Scale(1 / top_plot.GetSumOfWeights())

            if right_plot.GetSumOfWeights() > 0:
                right_plot.Scale(1 / right_plot.GetSumOfWeights())

        # min/max
        if self.max_val is None:
            if self.density:
                max_val = 1
            else:
                max_val = None
        else:
            max_val = self.max_val

        if self.min_val is None:
            min_val = None
        else:
            min_val = self.min_val

        # CREATE CANVAS
        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.200)
        plot_canvas.SetRightMargin(0.200)
        plot_canvas.SetBottomMargin(0.200)

        # Draw Top
        plot_canvas.cd(0)
        top_pad = TPad("top", "top", 0.0, 0.0, 1.0, 1.0)
        top_pad.SetTopMargin(0.150)
        top_pad.SetLeftMargin(0.175)
        top_pad.SetRightMargin(0.350)
        top_pad.SetBottomMargin(0.675)
        top_pad.SetLogy(self.log_scale)
        top_pad.Draw()
        top_pad.cd()

        top_plot.SetLineColor(kRed)
        top_plot.SetFillColor(kRed - 7)
        top_plot.GetXaxis().SetTitle('')
        top_plot.GetXaxis().SetLabelSize(0)
        top_plot.GetXaxis().SetLabelOffset(999)
        top_plot.GetYaxis().SetTitle('')
        top_plot.GetYaxis().SetNdivisions(203)
        top_plot.GetYaxis().SetLabelSize(0.03)

        if max_val is not None:
            top_plot.SetMaximum(max_val)

        if min_val is not None:
            top_plot.SetMinimum(min_val)

        top_plot.Draw('BAR')
        top_plot.Draw('SAME AXIG')

        # Draw Right
        plot_canvas.cd(0)
        right_pad = TPad("right", "right", 0.0, 0.0, 1.0, 1.0)
        right_pad.SetFillStyle(4000)
        right_pad.SetTopMargin(0.365)
        right_pad.SetLeftMargin(0.800)
        right_pad.SetRightMargin(0.025)
        right_pad.SetBottomMargin(0.160)
        right_pad.SetLogx(self.log_scale)
        right_pad.Draw('')
        right_pad.cd()

        right_plot.SetLineColor(kBlue)
        right_plot.SetFillColor(kBlue - 7)
        right_plot.GetXaxis().SetTitle('')
        right_plot.GetXaxis().SetLabelSize(0)
        right_plot.GetXaxis().SetLabelOffset(999)
        right_plot.GetYaxis().SetTitle('')
        right_plot.GetYaxis().SetNdivisions(203)
        right_plot.GetYaxis().SetLabelSize(0.03)

        if max_val is not None:
            right_plot.SetMaximum(max_val)

        if min_val is not None:
            right_plot.SetMinimum(min_val)

        right_plot.Draw('HBAR')
        right_plot.Draw('SAME AXIG')

        # Draw Center
        plot_canvas.cd(0)
        center_pad = TPad("center", "center", 0.0, 0.0, 1.0, 1.0)
        center_pad.SetFillStyle(4000)
        center_pad.SetTopMargin(0.365)
        center_pad.SetLeftMargin(0.175)
        center_pad.SetRightMargin(0.350)
        center_pad.SetBottomMargin(0.160)
        center_pad.SetLogz(self.log_scale)
        center_pad.Draw()
        center_pad.cd()

        if self.floor_min and min_val is not None:
            for bin_id in range(self.plot.GetNcells()):
                val = self.plot.GetBinContent(bin_id)

                if val < min_val:
                    self.plot.SetBinContent(bin_id, min_val)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetXaxis().SetTitleOffset(1.45)
        self.plot.GetYaxis().SetTitleOffset(1.45)
        self.plot.GetYaxis().SetMaxDigits(3)

        if max_val is not None:
            self.plot.SetMaximum(max_val)

        if min_val is not None:
            self.plot.SetMinimum(min_val)

        self.plot.Draw('COLZ')

        # Draw Titles
        plot_canvas.cd(0)

        top_title = TLatex(0.75 - 0.150, 0.8375, 'Proj. X')
        top_title.SetTextAlign(23)
        top_title.SetTextSize(0.030)
        top_title.Draw()

        right_title = TLatex(0.9625, 0.625 - 0.050, 'Proj. Y')
        right_title.SetTextAlign(23)
        right_title.SetTextAngle(-90)
        right_title.SetTextSize(0.030)
        right_title.Draw()

        main_title = TLatex(0.045, 0.95, self.title)
        main_title.SetTextAlign(12)
        main_title.Draw()

        draw_fancy_label(0.045, 0.875)

        plot_canvas.SaveAs(self.name + '.png')


class EfficiencyPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 logx=False, logy=False):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title

        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.xbins = xbins
        self.logx = logx
        self.logy = logy

        if all(v is not None for v in [x_low, x_up]):
            self.plot = TEfficiency(name, title, nxbins, x_low, x_up)
        elif all(v is not None for v in [xbins]):
            self.plot = TEfficiency(name, title, nxbins, xbins)

    def add(self, other):
        self.plot.Add(other.plot)

    def fill(self, passed_cut, value):
        self.plot.Fill(passed_cut, value)

    def consolidate(self):
        pass

    def write(self):
        # Write
        self.plot.Write()

        # Image
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetTopMargin(0.110)
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.cd(0)

        # Paint
        self.plot.Paint("")
        gPad.Update()

        # Configure
        self.plot.GetPaintedGraph().GetXaxis().SetTitle(self.x_title)
        self.plot.GetPaintedGraph().GetXaxis().SetTitleOffset(1.350)
        self.plot.GetPaintedGraph().GetYaxis().SetMaxDigits(3)
        self.plot.GetPaintedGraph().GetYaxis().SetTitle(self.y_title)
        self.plot.GetPaintedGraph().GetYaxis().SetTitleOffset(1.450)

        self.plot.SetLineColor(kRed)
        self.plot.SetMarkerSize(0.5)
        self.plot.SetMarkerColor(kBlack)
        self.plot.SetMarkerStyle(kFullDotLarge)

        self.plot.Draw('C P')
        self.plot.Draw('SAME AXIG')

        # Draw 90% Line
        line = TLine(self.plot.GetXaxis().GetXmin(), 0.9, self.plot.GetXaxis().GetXmax(), 0.9)
        line.SetLineStyle(10)
        line.SetLineWidth(5)
        line.SetLineColor(kBlack)
        line.Draw('SAME')

        plot_canvas.SaveAs(self.name + '.png')


class Rate1DPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 min_val=None, max_val=None, target_val=None,
                 logx=False, logy=False, density=False):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.xbins = xbins
        self.logx = logx
        self.logy = logy
        self.density = density
        self.max_val = max_val
        self.min_val = min_val
        self.target_val = target_val
        self.comment = None

        if all(v is not None for v in [x_low, x_up]):
            self.plot = TH1D(name, title, nxbins, x_low, x_up)
        elif all(v is not None for v in [xbins]):
            self.plot = TH1D(name, title, nxbins, xbins)

    def add(self, other):
        self.plot.Add(other.plot)

    def fill(self, value, w=1):
        self.plot.Fill(value, w)

    def consolidate(self):
        pass

    def write(self):
        # WRITE
        self.plot.Write()

        # Scale
        if self.density and self.plot.GetSumOfWeights() > 0:
            self.plot.Scale(1 / self.plot.GetSumOfWeights())

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetTopMargin(0.110)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.cd(0)

        self.plot.SetLineWidth(3)
        self.plot.SetLineColor(kBlue)
        self.plot.SetFillColorAlpha(kBlue, 0)
        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetXaxis().SetTitleOffset(1.350)
        self.plot.GetYaxis().SetMaxDigits(3)

        # min/max
        if self.density:
            self.plot.SetMaximum(1.1)
            self.plot.SetMinimum(1e-5)

        if self.max_val is not None:
            self.plot.SetMaximum(self.max_val)

        if self.min_val is not None:
            self.plot.SetMinimum(self.min_val)

        # Draw Hist
        self.plot.Draw('HIST')

        # Draw Error
        err_plot = self.plot.Clone()
        err_plot.SetFillColorAlpha(kBlue, 0.5)
        err_plot.Draw('SAME E2')

        if self.y_title is not None:
            plot_canvas.SetLeftMargin(0.175)
            self.plot.GetYaxis().SetTitle(self.y_title)
            self.plot.GetYaxis().SetTitleOffset(1.450)

        self.plot.Draw('SAME AXIG')

        # Draw 90% Line
        line = None

        if self.target_val is not None:
            line = TLine(self.plot.GetXaxis().GetXmin(), self.target_val, self.plot.GetXaxis().GetXmax(),
                         self.target_val)
            line.SetLineStyle(10)
            line.SetLineWidth(2)
            line.SetLineColor(kSpring + 4)
            line.Draw('SAME')

        # Comment
        if self.comment is not None:
            draw_fancy_label(0.675, 0.835, text=self.comment)

        plot_canvas.SaveAs(self.name + '.png')


class Hist2DPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title, z_title=None,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 nybins=None, y_low=None, y_up=None, ybins=None,
                 logx=False, logy=False, logz=False,
                 xlabels=None, ylabels=None,
                 density=False, normalize_by=None,
                 floor_min=True, min_val=None, max_val=None):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        if ybins is not None:
            nybins = len(ybins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.z_title = z_title
        self.nxbins = nxbins
        self.nybins = nybins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.xbins = xbins
        self.ybins = ybins
        self.logx = logx
        self.logy = logy
        self.logz = logz
        self.density = density
        self.normalize_by = normalize_by
        self.xlabels = xlabels
        self.ylabels = ylabels

        if all(v is not None for v in [nxbins, x_low, x_up, nybins, y_low, y_up]):
            self.plot = TH2D(name, title, nxbins, x_low, x_up, nybins, y_low, y_up)
        elif all(v is not None for v in [xbins, ybins]):
            self.plot = TH2D(name, title, nxbins, xbins, nybins, ybins)

        self.floor_min = floor_min
        self.min_val = min_val
        self.max_val = max_val

    def add(self, other):
        self.plot.Add(other.plot)

    def fill(self, x, y, w=1):
        self.plot.Fill(x, y, w)

    def consolidate(self):
        pass

    def write(self):
        # WRITE
        self.plot.Write()

        # Normalize
        if self.normalize_by is None:
            pass
        elif self.normalize_by == 'row':
            for row_id in range(self.nybins):
                norm = 0

                for col_id in range(self.nxbins):
                    bin_id = self.plot.GetBin(col_id + 1, row_id + 1, 0)
                    bin_val = self.plot.GetBinContent(bin_id)
                    norm += bin_val

                for col_id in range(self.nxbins):
                    bin_id = self.plot.GetBin(col_id + 1, row_id + 1, 0)
                    bin_val = self.plot.GetBinContent(bin_id)
                    bin_val = bin_val / max(norm, 1)
                    self.plot.SetBinContent(bin_id, bin_val)
        elif self.normalize_by == 'column':
            for col_id in range(self.nxbins):
                norm = 0

                for row_id in range(self.nybins):
                    bin_id = self.plot.GetBin(col_id + 1, row_id + 1, 0)
                    bin_val = self.plot.GetBinContent(bin_id)
                    norm += bin_val

                for row_id in range(self.nybins):
                    bin_id = self.plot.GetBin(col_id + 1, row_id + 1, 0)
                    bin_val = self.plot.GetBinContent(bin_id)
                    bin_val = bin_val / max(norm, 1)
                    self.plot.SetBinContent(bin_id, bin_val)

        # Density
        if self.density and self.plot.GetSumOfWeights() > 0:
            self.plot.Scale(1 / self.plot.GetSumOfWeights())

        # min/max
        if self.max_val is None:
            if self.density:
                max_val = 1
            else:
                max_val = None
        else:
            max_val = self.max_val

        if self.min_val is None:
            min_val = None
        else:
            min_val = self.min_val

        # Floor
        if self.min_val is None:
            floor_val = self.plot.GetMinimum() * 0.1
        else:
            floor_val = self.min_val

        if self.floor_min:
            for bin_id in range(self.plot.GetNcells()):
                val = self.plot.GetBinContent(bin_id)

                if val < floor_val:
                    self.plot.SetBinContent(bin_id, floor_val)

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetRightMargin(0.150)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.SetLogz(1 if self.logz else 0)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetYaxis().SetTitleOffset(1.65)
        self.plot.GetYaxis().SetMaxDigits(3)

        # Set Z-Axis
        if self.z_title is not None:
            self.plot.GetZaxis().SetTitle(self.z_title)
            self.plot.GetZaxis().SetTitleOffset(1.25)
            plot_canvas.SetRightMargin(0.200)

        # Set Labels
        if self.xlabels is not None:
            for col_id in range(self.nxbins):
                bin_id = self.plot.GetXaxis().FindBin(col_id)
                self.plot.GetXaxis().SetBinLabel(bin_id, self.xlabels[col_id])

        if self.ylabels is not None:
            for row_id in range(self.nybins):
                bin_id = self.plot.GetYaxis().FindBin(row_id)
                self.plot.GetYaxis().SetBinLabel(bin_id, self.ylabels[row_id])

        # min/max
        if max_val is not None:
            self.plot.SetMaximum(max_val)

        if min_val is not None:
            self.plot.SetMinimum(min_val)

        self.plot.Draw('COLZ')

        plot_canvas.SaveAs(self.name + '.png')


class Hist1DPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 min_val=None, max_val=None,
                 logx=False, logy=False,
                 xlabels=None,
                 density=False):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.xbins = xbins
        self.logx = logx
        self.logy = logy
        self.density = density
        self.xlabels = xlabels

        if all(v is not None for v in [x_low, x_up]):
            self.plot = TH1D(name, title, nxbins, x_low, x_up)
        elif all(v is not None for v in [xbins]):
            self.plot = TH1D(name, title, nxbins, xbins)

        self.min_val = min_val
        self.max_val = max_val

    def add(self, other):
        self.plot.Add(other.plot)

    def fill(self, value, w=1):
        self.plot.Fill(value, w)

    def consolidate(self):
        pass

    def write(self):
        # WRITE
        self.plot.Write()

        # Scale
        if self.density and self.plot.GetSumOfWeights() > 0:
            self.plot.Scale(1 / self.plot.GetSumOfWeights())

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetTopMargin(0.110)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.cd(0)

        self.plot.SetLineColor(kBlue)
        self.plot.SetFillColorAlpha(kBlue, 0.5)
        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetXaxis().SetTitleOffset(1.350)
        self.plot.GetYaxis().SetMaxDigits(3)

        # Set Labels
        if self.xlabels is not None:
            for col_id in range(self.nxbins):
                bin_id = self.plot.GetXaxis().FindBin(col_id)
                self.plot.GetXaxis().SetBinLabel(bin_id, self.xlabels[col_id])

        # min/max
        if self.density:
            self.plot.SetMaximum(1.1)
            self.plot.SetMinimum(1e-5)
        else:
            if self.max_val is not None:
                self.plot.SetMaximum(self.max_val)

            if self.min_val is not None:
                self.plot.SetMinimum(self.min_val)

        self.plot.Draw('HIST')

        if self.y_title is not None:
            plot_canvas.SetLeftMargin(0.175)
            self.plot.GetYaxis().SetTitle(self.y_title)
            self.plot.GetYaxis().SetTitleOffset(1.450)

        self.plot.Draw('SAME AXIG')

        plot_canvas.SaveAs(self.name + '.png')


class GraphPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title,
                 logx=False, logy=False):
        super().__init__()

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.n = 0
        self.x_arr = list()
        self.y_arr = list()
        self.logx = logx
        self.logy = logy

        self.plot = None

    def add(self, other):
        self.n += other.n
        self.x_arr += other.x_arr
        self.y_arr += other.y_arr

    def fill(self, x, y):
        self.n += 1
        self.x_arr.append(x)
        self.y_arr.append(y)

    def consolidate(self):
        self.plot = TGraph(self.n, array.array('d', self.x_arr), array.array('d', self.y_arr))
        self.plot.SetTitle(self.title)

    def write(self):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetTopMargin(0.125)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetXaxis().SetTitleOffset(1.350)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetYaxis().SetTitleOffset(1.650)
        self.plot.Draw('A*')

        plot_canvas.SaveAs(self.name + '.png')
