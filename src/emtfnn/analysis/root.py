from array import array

from ROOT import TColor
from ROOT import TH1
from ROOT import gROOT
from ROOT import gStyle
from ROOT import kFALSE
from ROOT import kTRUE
from matplotlib import pyplot as plt

from emtfnn.analysis import cms

cm_tab10 = list()
_cm_tab10_root = list()

cm_tab20 = list()
_cm_tab20_root = list()


def configure():
    # Stupid Root Stuff
    gROOT.SetBatch(kTRUE)
    TH1.AddDirectory(kFALSE)
    gROOT.ProcessLine("gErrorIgnoreLevel = kFatal;")

    # Stupid Style Stuff
    cms.apply_tdr_style()
    gStyle.SetPalette(1)
    gStyle.SetCanvasDefH(1200)
    gStyle.SetCanvasDefW(1200)
    gStyle.SetOptTitle(1)
    gStyle.SetPadTopMargin(0.100)
    gStyle.SetPadBottomMargin(0.100)
    gStyle.SetPadGridX(1)
    gStyle.SetPadGridY(1)
    set_palette('osvaldo')
    load_tab10()
    load_tab20()


def load_tab10():
    color_id = 9000

    for i in range(10):
        mpl_color = plt.cm.tab10(i)

        r = round(mpl_color[0] * 100) / 100.
        g = round(mpl_color[1] * 100) / 100.
        b = round(mpl_color[2] * 100) / 100.
        root_color = TColor(color_id, r, g, b, 'tab10_%d' % i, 1.)

        cm_tab10.append(color_id)
        _cm_tab10_root.append(root_color)

        color_id += 1


def load_tab20():
    color_id = 9100

    for i in range(20):
        mpl_color = plt.cm.tab20(i)

        r = round(mpl_color[0] * 100) / 100.
        g = round(mpl_color[1] * 100) / 100.
        b = round(mpl_color[2] * 100) / 100.
        root_color = TColor(color_id, r, g, b, 'tab20_%d' % i, 1.)

        cm_tab20.append(color_id)
        _cm_tab20_root.append(root_color)

        color_id += 1


def set_palette(name='default', ncontours=999):
    """Set a color palette from a given RGB list
    stops, red, green and blue should all be lists of the same length
    see set_decent_colors for an example"""

    if name == "gray" or name == "grayscale":
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red = [1.00, 0.84, 0.61, 0.34, 0.00]
        green = [1.00, 0.84, 0.61, 0.34, 0.00]
        blue = [1.00, 0.84, 0.61, 0.34, 0.00]
    elif name == "osvaldo":
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red = [0.26, 0.00, 0.87, 1.00, 0.51]
        green = [0.00, 0.81, 1.00, 0.20, 0.00]
        blue = [0.33, 1.00, 0.12, 0.00, 0.00]
    else:
        # default palette, looks cool
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red = [0.00, 0.00, 0.87, 1.00, 0.51]
        green = [0.00, 0.81, 1.00, 0.20, 0.00]
        blue = [0.51, 1.00, 0.12, 0.00, 0.00]

    s = array('d', stops)
    r = array('d', red)
    g = array('d', green)
    b = array('d', blue)

    npoints = len(s)
    TColor.CreateGradientColorTable(npoints, s, r, g, b, ncontours)
    gStyle.SetNumberContours(ncontours)
