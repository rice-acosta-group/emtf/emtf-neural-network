import numpy as np
from matplotlib import pyplot as plt

from emtfnn.commands.compare_perf.comparison_data import rmse_datasets
from emtfnn.commands.compare_perf.comparison_data import rmse_pt_bins
from emtfnn.commands.compare_perf.comparison_data import rmse_values


def configure_parser(parser):
    pass


def run(args):
    # Init
    arr_dataset_bin_id = np.arange(0, len(rmse_datasets))
    arr_pt_bin_id = np.arange(0, len(rmse_pt_bins))

    arr_x = np.repeat(arr_dataset_bin_id, len(rmse_pt_bins))
    arr_y = np.tile(arr_pt_bin_id, len(rmse_datasets))

    bin_x_edges = np.arange(-0.5, len(rmse_datasets) + 0.5, step=1)
    bin_y_edges = np.arange(-0.5, len(rmse_pt_bins) + 0.5, step=1)

    print(bin_x_edges, bin_y_edges)

    # Aggregate Plot 1/pT
    fig, ax = plt.subplots()

    arr_rmse_val_dataset = list()

    for label, values in rmse_values['invpt'].items():
        arr_rmse_val_dataset += values

    h2d = ax.hist2d(arr_x, arr_y,
                    bins=[bin_x_edges, bin_y_edges],
                    weights=arr_rmse_val_dataset)
    plt.colorbar(h2d[3], ax=ax, label='RMSE [1/GeV]')

    ax.set_xticks(arr_dataset_bin_id, rmse_datasets, rotation=-20)
    ax.set_yticks(arr_pt_bin_id, rmse_pt_bins, rotation=0)

    fig.set_figheight(4)
    fig.set_figwidth(10)
    fig.tight_layout()
    fig.tight_layout()
    fig.tight_layout()
    fig.tight_layout()
    plt.savefig('invpt_rmse.png')

    # Aggregate Plot pT
    fig, ax = plt.subplots()

    arr_rmse_val_dataset = list()

    for label, values in rmse_values['pt'].items():
        arr_rmse_val_dataset += values

    h2d = ax.hist2d(arr_x, arr_y,
                    bins=[bin_x_edges, bin_y_edges],
                    weights=arr_rmse_val_dataset)
    plt.colorbar(h2d[3], ax=ax, label='RMSE [GeV]')

    ax.set_xticks(arr_dataset_bin_id, rmse_datasets, rotation=-20)
    ax.set_yticks(arr_pt_bin_id, rmse_pt_bins, rotation=0)

    fig.set_figheight(4)
    fig.set_figwidth(10)
    fig.tight_layout()
    fig.tight_layout()
    fig.tight_layout()
    fig.tight_layout()
    plt.savefig('pt_rmse.png')

    # Aggregate Plot dxy
    fig, ax = plt.subplots()

    arr_rmse_val_dataset = list()

    for label, values in rmse_values['dxy'].items():
        arr_rmse_val_dataset += values

    h2d = ax.hist2d(arr_x, arr_y,
                    bins=[bin_x_edges, bin_y_edges],
                    weights=arr_rmse_val_dataset)
    plt.colorbar(h2d[3], ax=ax, label='RMSE [cm]')

    ax.set_xticks(arr_dataset_bin_id, rmse_datasets, rotation=-20)
    ax.set_yticks(arr_pt_bin_id, rmse_pt_bins, rotation=0)

    fig.subplots_adjust(bottom=4 * 0.05)
    fig.set_figheight(4)
    fig.set_figwidth(10)
    fig.tight_layout()
    fig.tight_layout()
    fig.tight_layout()
    fig.tight_layout()
    plt.savefig('dxy_rmse.png')

    # Aggregate Plot Rel pT
    fig, ax = plt.subplots()

    arr_rmse_val_dataset = list()

    for label, values in rmse_values['rel_pt'].items():
        arr_rmse_val_dataset += values

    h2d = ax.hist2d(arr_x, arr_y,
                    bins=[bin_x_edges, bin_y_edges],
                    weights=arr_rmse_val_dataset)
    plt.colorbar(h2d[3], ax=ax, label='RMSRE')

    ax.set_xticks(arr_dataset_bin_id, rmse_datasets, rotation=-20)
    ax.set_yticks(arr_pt_bin_id, rmse_pt_bins, rotation=0)

    fig.set_figheight(4)
    fig.set_figwidth(10)
    fig.tight_layout()
    fig.tight_layout()
    fig.tight_layout()
    fig.tight_layout()
    plt.savefig('pt_rmsre.png')
