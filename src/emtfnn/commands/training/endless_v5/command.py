import numpy as np
from keras.callbacks import ModelCheckpoint
from keras.layers import BatchNormalization

from emtfnn.commands.training.endless_v5 import constants
from emtfnn.commands.training.endless_v5.custom.d0_activation import D0Activation
from emtfnn.commands.training.endless_v5.custom.hybrid_activation import HybridActivation
from emtfnn.commands.training.endless_v5.custom.qpt_activation import QPtActivation
from emtfnn.commands.training.endless_v5.custom.quantized_relu import QuantizedReLu
from emtfnn.commands.training.endless_v5.custom.weight_constraint import QuantizedWeight
from emtfnn.commands.training.endless_v5.models import create_model, create_lr_schedule, \
    create_optimizer, d0_loss, qpt_loss
from emtfnn.commands.training.endless_v5.operations.bnfold import fold_first_batch_norm
from emtfnn.commands.training.endless_v5.operations.stitching import stitch
from emtfnn.commands.training.endless_v5.phase1_datasets import Phase1Dataset
from emtfnn.commands.training.endless_v5.phase1_zerobias_datasets import Phase1_ZeroBias_Dataset
from emtfnn.commands.training.endless_v5.phase2_datasets import Phase2Dataset
from emtfnn.commons.analyzers.dxy_training import DxyTrainingAnalyzer
from emtfnn.commons.analyzers.qpt_training import QPtTrainingAnalyzer
from emtfnn.commons.analyzers.disp_nn_rates import DxyRateAnalyzer
from emtfnn.commons.plotting import plot_variables
from emtfnn.commons.trainers import ModelTrainer
from emtfnn.commons.workbench import dump_vars_params
from emtfnn.commons.workbench import load_model
from emtfnn.commons.workbench import save_model
from emtfnn.execution.runtime import get_logger
from emtfnn.keras.loggers import LearningRateLogger
from emtfnn.keras.optimizers import Adamu, WarmupCosineDecay

from emtfnn.hls.phase1_v5.build_hls_model import build_and_test_hls_model as phase1_build_and_test_hls_model
from emtfnn.hls.phase2_v5.build_hls_model import build_and_test_hls_model as phase2_build_and_test_hls_model
from emtfnn.commons.resolution_plotter import make_resolution_pdf


def configure_parser(parser):
    parser.add_argument(dest='model_name', metavar='MODEL_NAME', type=str, help='Model Name')
    parser.add_argument(dest='pos_file', metavar='POS_FILE', type=str, help='Positive Endcap File')
    parser.add_argument(dest='neg_file', metavar='NEG_FILE', type=str, help='Negative Endcap File')

    parser.add_argument('--dataset', dest='dataset', metavar='DATASET',
                        type=str, default='phase2',
                        help='dataset')

    parser.add_argument('--sample', dest='sample', metavar='SAMPLE',
                        type=str, default='train',
                        help='sample')

    parser.add_argument('--lr', dest='lr', metavar='LEARNING_RATE',
                        type=float, default=None,
                        help='Learning Rate')

    parser.add_argument('--epochs', dest='epochs', metavar='EPOCHS',
                        type=int, default=None,
                        help='Number of epochs')

    parser.add_argument('--nodes', dest='nodes', metavar='NODES',
                        type=str, default=None,
                        help='List of number of nodes')

    parser.add_argument('--target', dest='target', metavar='TARGET',
                        type=str, default=None,
                        help='target parameter')

    parser.add_argument('--base-model', dest='base_model', metavar='BASE_MODEL',
                        type=str, default=None,
                        help='base model')

    parser.add_argument('--qpt-model', dest='qpt_model', metavar='QPT_MODEL',
                        type=str, default=None,
                        help='qpt model')

    parser.add_argument('--d0-model', dest='d0_model', metavar='D0_MODEL',
                        type=str, default=None,
                        help='d0 model')

    parser.add_argument('--train', dest='train_en', action='store_const',
                        const=True, default=False,
                        help='Train')

    parser.add_argument('--fold', dest='fold_en', action='store_const',
                        const=True, default=False,
                        help='Fold first batch norm')

    parser.add_argument('--quantize', dest='quantize_en', action='store_const',
                        const=True, default=False,
                        help='Quantize Weights')

    parser.add_argument('--merge', dest='merge_en', action='store_const',
                        const=True, default=False,
                        help='Merge')

    parser.add_argument('--no-act', dest='no_act_en', action='store_const',
                        const=True, default=False,
                        help='Remove activation function')
    
    parser.add_argument('--build-hls-model', dest='build_hls_model', action='store_const',
                    const=True, default=False,
                    help='Run hls4ml model conversion')
    
    parser.add_argument('--test-rate', dest='test_rate', action='store_const',
                    const=True, default=False,
                    help='Run Rate Test - must input zero bias sample')


def run(args):
    # Unpack Args
    model_name = args.model_name
    pos_file = args.pos_file
    neg_file = args.neg_file

    dataset = args.dataset
    sample = args.sample

    lr = args.lr
    epochs = args.epochs

    base_model = args.base_model
    qpt_model = args.qpt_model
    d0_model = args.d0_model

    nodes = args.nodes
    target = args.target
    train_en = args.train_en
    fold_en = args.fold_en
    quantize_en = args.quantize_en
    merge_en = args.merge_en
    no_act_en = args.no_act_en

    build_hls_en = args.build_hls_model
    test_rate_en = args.test_rate

    # Update Constants
    if lr is not None:
        constants.learning_rate = lr
        constants.final_learning_rate = lr * 0.01

    if epochs is not None:
        constants.epochs = epochs

    # Check inputs
    inputs_consistent = (pos_file is not None and neg_file is not None)

    assert inputs_consistent, "Inputs are invalid. Check you\'re not missing an inputfile or that they are not empty"

    # Sanitize Args
    if nodes is not None:
        nodes = nodes.split(',')
        nodes = [int(n) for n in nodes]

    # Load Dataset
    if dataset == 'phase2':
        dataset = Phase2Dataset(pos_file, neg_file, sample=sample)
    elif dataset == 'phase1':
        if not test_rate_en:
            dataset = Phase1Dataset(pos_file, neg_file, sample=sample)
        else:
            dataset = Phase1_ZeroBias_Dataset(pos_file, sample=sample)
    else:
        raise ValueError('Invalid dataset')

    x_train, y_train, \
    x_test, y_test = dataset.get_samples()

    # Unpack parameters
    original_x_test = np.copy(x_test)
    original_y_test = np.copy(y_test)

    qpt_train, d0_train = y_train[:, 0], y_train[:, 1]
    qpt_test, d0_test = y_test[:, 0], y_test[:, 1]

    # Stack Parameters
    y_train = np.column_stack((qpt_train, d0_train))
    y_test = np.column_stack((qpt_test, d0_test))

    # Mask Parameters
    if target is None:
        y_mask = [True, True]
    elif target in ('qpt',):
        y_mask = [True, False]
    elif target in ('d0',):
        y_mask = [False, True]

    y_train = y_train[:, y_mask]
    y_test = y_test[:, y_mask]

    # Plot Variables
    plot_variables(x_train, 'variables.png')
    plot_variables(y_train, 'parameters.png')

    # Train
    if train_en:
        train_submodel(model_name,
                       x_train, y_train,
                       x_test, y_test,
                       nodes=nodes,
                       target=target,
                       fold_en=fold_en,
                       quantize_en=quantize_en,
                       base_model=base_model)
    elif no_act_en:
        noactivation_submodel(model_name,
                              x_train, y_train,
                              nodes=nodes,
                              target=target,
                              fold_en=fold_en,
                              base_model=base_model)
    elif fold_en:
        fold_models(model_name,
                    base_model,
                    target=target)
    elif merge_en:
        merge_models(model_name,
                     qpt_model,
                     d0_model)


    if build_hls_en:
        build_and_analyze_hls(model_name, dataset, x_train, x_test, original_x_test, original_y_test, y_mask)
    elif test_rate_en:
        test_model_rate(model_name, x_test, original_y_test)
    else:
        # Analyze Performance
        analyze_performance(model_name, original_x_test, original_y_test, y_mask)


def fold_models(
        model_name,
        base_model_name,
        target):
    base_model = _load_static_model(base_model_name)

    fold_first_batch_norm(model_name, base_model, target)


def merge_models(
        model_name,
        qpt_model_name,
        d0_model_name):
    qpt_model = _load_static_model(qpt_model_name)
    d0_model = _load_static_model(d0_model_name)

    stitch(model_name, qpt_model, d0_model)


def train_submodel(
        model_name,
        x_train, y_train,
        x_test, y_test,
        nodes=[28, 24, 16],
        target='qpt',
        fold_en=False,
        quantize_en=False,
        base_model=None):
    lr_schedule = create_lr_schedule(
        x_train.shape[0],
        epochs=constants.epochs,
        warmup_epochs=constants.warmup_epochs,
        batch_size=constants.batch_size,
        learning_rate=constants.learning_rate,
        final_learning_rate=constants.final_learning_rate)

    optimizer = create_optimizer(
        lr_schedule,
        gradient_clipnorm=constants.gradient_clipnorm)

    model = create_model(
        optimizer,
        in_nodes=x_train.shape[1],
        out_nodes=y_train.shape[1],
        hidden_layer_nodes=nodes,
        fold_en=fold_en,
        quantized_en=quantize_en,
        target_parameter=target)

    if base_model is not None:
        model.load_weights(base_model + '_weights.h5')

    lr_logger = LearningRateLogger()
    model_best_check = ModelCheckpoint(filepath='model_bchk.h5', monitor='val_loss', verbose=1, save_best_only=True)
    model_best_check_weights = ModelCheckpoint(filepath='model_bchk_weights.h5', monitor='val_loss', verbose=1,
                                               save_best_only=True, save_weights_only=True)

    # Train Model
    get_logger().info('Training model')
    get_logger().info('learning_rate: {} final_learning_rate: {} epochs: {} batch_size: {}'.format(
        constants.learning_rate, constants.final_learning_rate, constants.epochs, constants.batch_size))

    history = ModelTrainer(model).fit(
        x_train, y_train,
        epochs=constants.epochs,
        batch_size=constants.batch_size,
        callbacks=[lr_logger, model_best_check, model_best_check_weights],
        validation_data=(x_test, y_test), shuffle=True,
        verbose=2)

    metrics = [len(history.history['loss']), history.history['loss'][-1], history.history['val_loss'][-1]]
    get_logger().info('Epoch {0}/{0} - loss: {1} - val_loss: {2}'.format(*metrics))

    # Save Model
    save_model(model, name=model_name)


def noactivation_submodel(
        model_name,
        x_train, y_train,
        nodes=[28, 24, 16],
        target='qpt',
        fold_en=False,
        base_model=None):
    lr_schedule = create_lr_schedule(
        x_train.shape[0],
        epochs=constants.epochs,
        warmup_epochs=constants.warmup_epochs,
        batch_size=constants.batch_size,
        learning_rate=constants.learning_rate,
        final_learning_rate=constants.final_learning_rate)

    optimizer = create_optimizer(
        lr_schedule,
        gradient_clipnorm=constants.gradient_clipnorm)

    model = create_model(
        optimizer,
        in_nodes=x_train.shape[1],
        out_nodes=y_train.shape[1],
        hidden_layer_nodes=nodes,
        target_parameter=target,
        fold_en=fold_en,
        no_activation_en=True)

    if base_model is not None:
        model.load_weights(base_model + '_weights.h5')

    # Save Model
    save_model(model, name=model_name)


def build_and_analyze_hls(
        model_name, dataset, x_train, x_test, original_x_test, original_y_test, y_mask):
    
    kmodel = _load_static_model(model_name) # Must be hls4ml compatible
    if dataset == 'phase1':
        hls_model = phase1_build_and_test_hls_model(kmodel, x_train, x_test)
    else:
        hls_model = phase2_build_and_test_hls_model(kmodel, x_train, x_test)

    if not np.all(original_y_test == 0):
        predictions = hls_model.predict(np.ascontiguousarray(original_x_test))
        analyze_performance_from_predictions(predictions, original_x_test, original_y_test, y_mask)
    else:
        print('All particle gen info is 0, skipping hls performance analysis')


def test_model_rate(
        model_name, x_test, original_y_test):
    
    # Must use a specific min-bias dataset
    evt_nums = original_y_test[:,7]
    modes = original_y_test[:,8]
    bxs = original_y_test[:,9]

    # Run all tracks, then zero out invalid modes and bxs
    loaded_model = _load_static_model(model_name)
    predictions = loaded_model.predict(x_test, batch_size=16384)
    valid_tracks_all = (bxs == 0) & ((modes == 11) | (modes == 13) | (modes == 14) | (modes == 15))

    for mode in [14,15,'highqual']:
        if mode == 'highqual':
            valid_tracks = valid_tracks_all
        else:
            valid_tracks = valid_tracks_all & (modes == mode)

        predictions_clean = np.where(valid_tracks.reshape((-1,1)), np.abs(predictions), 0)
        data = (predictions_clean[:,0], predictions_clean[:,1], evt_nums)
        rate_analyzer = DxyRateAnalyzer(True)
        rate_analyzer.process(data, mode=str(mode))
                     


def analyze_performance(
        model_name, x_test, y_test, y_mask):
    # Load model and predict outputs
    loaded_model = _load_static_model(model_name)

    # Make predictions and unscale
    predictions = loaded_model.predict(x_test, batch_size=16384)

    analyze_performance_from_predictions(predictions, x_test, y_test, y_mask)


def analyze_performance_from_predictions(
        predictions, x_test, y_test, y_mask):
    
    # Unpack parameters
    param_id = 0

    if y_mask[0]:
        qpt_pred = predictions[:, param_id]
        param_id += 1
    else:
        qpt_pred = np.zeros((y_test.shape[0], 1))

    if y_mask[1]:
        d0_pred = predictions[:, param_id]
        param_id += 1
    else:
        d0_pred = np.zeros((y_test.shape[0], 1))

    # Analyze
    qpt_test = y_test[:, 0]
    d0_test = y_test[:, 1]
    eta_star_test = y_test[:, 2]
    dz_test = y_test[:, 6]

    test_param = np.column_stack((qpt_test, d0_test, eta_star_test, dz_test))
    pred_param = np.column_stack((qpt_pred, d0_pred))

    analyzers = list()

    if y_mask[0]:
        analyzers.append(QPtTrainingAnalyzer())

    if y_mask[1]:
        analyzers.append(DxyTrainingAnalyzer())

    for analyzer in analyzers:
        analyzer.process((test_param, pred_param))

    for analyzer in analyzers:
        analyzer.write()

    # Dump Calibration Inputs
    calibration_data = np.column_stack((qpt_test, d0_test, qpt_pred, d0_pred))
    np.savez_compressed('calibration_data.npz', calibration_data=calibration_data)

    # Dump Predictions
    dump_vars_params(x_test[:1000, :], pred_param[:1000, :], file_path='vld_prediction_dump.tsv')


    # Make BDT Style Resolution Plots
    if y_mask[0]:
        make_resolution_pdf(qpt_pred, qpt_test, eta_star_test)


def _load_static_model(
        model_name):
    loaded_model = load_model(
        name=model_name,
        custom_objects={
            'Adamu': Adamu,
            'WarmupCosineDecay': WarmupCosineDecay,
            'qpt_loss': qpt_loss,
            'd0_loss': d0_loss,
            'QuantizedWeight': QuantizedWeight,
            'QuantizedReLu': QuantizedReLu,
            'BatchNormalization': BatchNormalization,
            'D0Activation': D0Activation,
            'QPtActivation': QPtActivation,
            'HybridActivation': HybridActivation,
        }
    )
    loaded_model.trainable = False

    assert not loaded_model.updates

    print(loaded_model.summary())

    return loaded_model
