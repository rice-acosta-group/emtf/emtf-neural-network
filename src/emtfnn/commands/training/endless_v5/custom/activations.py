import tensorflow as tf

from emtfnn.commands.training.endless_v5.constants import qpt_dense_eps, d0_dense_eps
from emtfnn.keras.math_utils import clip_by_value_preserve_gradient, floor_preserve_gradient


def qpt_activation_output(x, a=1., b=1., bias=0.):
    # Assume output from last dense layer is quantized in units of eps
    eps = tf.constant(qpt_dense_eps, x.dtype)

    x = floor_preserve_gradient(x)

    # Unpack Address
    in_charge = tf.where(x < 0, -tf.ones_like(x), tf.ones_like(x))
    in_invpt = tf.math.abs(x * eps)
    in_pt = tf.math.reciprocal(in_invpt + 1e-6)

    # Calculate Activation
    out_pt = a * in_pt + b * in_invpt + bias
    out_pt = clip_by_value_preserve_gradient(out_pt, 1, 1e3)
    out_qpt = in_charge * out_pt

    # Return
    return out_qpt


def d0_activation_output(x, a=1., bias=0.):
    # Assume output from last dense layer is quantized in units of eps
    eps = tf.constant(d0_dense_eps, x.dtype)

    x = floor_preserve_gradient(x)

    # Unpack Address
    in_sign = tf.where(x < 0, -tf.ones_like(x), tf.ones_like(x))
    in_dxy = tf.math.abs(x * eps)

    # Calculate Activation
    out_dxy = a * in_dxy + bias
    out_dxy = clip_by_value_preserve_gradient(out_dxy, 0, 1e3)
    out_d0 = in_sign * out_dxy

    # Return
    return out_d0