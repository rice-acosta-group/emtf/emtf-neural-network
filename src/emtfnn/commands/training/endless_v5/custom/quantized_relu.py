import keras.activations
import tensorflow as tf
from keras.engine.base_layer import Layer

from emtfnn.keras.math_utils import clip_by_value_preserve_gradient, floor_preserve_gradient


class QuantizedReLu(Layer):

    def __init__(self, bits, integer, signed, **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True

        self.bits = bits
        self.integer = integer
        self.signed = signed

        if signed:
            if bits > integer:
                bits = bits - 1
            else:
                integer = integer - 1

        self.po2 = pow(2, integer)
        self.eps_val = pow(2, -(bits - integer))

    def call(self, inputs, mask=None):
        out = keras.activations.relu(inputs)
        out = self.quantize(out)
        return out

    def quantize(self, raw_value):
        if not self.signed:
            raw_value = tf.abs(raw_value)

        quantized_value = floor_preserve_gradient(raw_value / self.eps_val) * self.eps_val

        if self.signed:
            quantized_value = clip_by_value_preserve_gradient(quantized_value, -self.po2, self.po2 - 1)
        else:
            quantized_value = clip_by_value_preserve_gradient(quantized_value, 0, self.po2 - 1)

        return quantized_value

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        config = super().get_config()

        config.update({
            'bits': self.bits,
            'integer': self.integer,
            'signed': self.signed
        })

        return config
