import tensorflow
from keras.engine.base_layer import Layer

from emtfnn.commands.training.endless_v5.custom.activations import qpt_activation_output


class QPtActivation(Layer):
    def __init__(self, quantize_en, no_activation_en, **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True

        self.quantize_en = quantize_en
        self.no_activation_en = no_activation_en

        a_constraint = None

        self.a = self.add_weight(
            name='a',
            shape=(1,),
            dtype=self.dtype,
            initializer=tensorflow.keras.initializers.constant(1.0),
            constraint=a_constraint,
            trainable=False)

        b_constraint = None

        self.b = self.add_weight(
            name='b',
            shape=(1,),
            dtype=self.dtype,
            initializer=tensorflow.keras.initializers.constant(0),
            constraint=b_constraint,
            trainable=False)

        bias_constraint = None

        self.bias = self.add_weight(
            name='bias',
            shape=(1,),
            dtype=self.dtype,
            initializer=tensorflow.keras.initializers.constant(-16.),
            constraint=bias_constraint,
            trainable=False)

    def call(self, inputs, mask=None):
        if self.no_activation_en:
            return inputs

        return qpt_activation_output(inputs, a=self.a, b=self.b, bias=self.bias)

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        config = super().get_config()

        config.update({
            'quantize_en': self.quantize_en,
            'no_activation_en': self.no_activation_en,
        })

        return config
