import tensorflow as tf
from keras.constraints import Constraint


class QuantizedWeight(Constraint):

    def __init__(self, bits, integer, signed):
        self.bits = bits
        self.integer = integer
        self.signed = signed

        if signed:
            if bits > integer:
                bits = bits - 1
            else:
                integer = integer - 1

        self.po2 = pow(2, integer)
        self.eps_val = pow(2, -(bits - integer))

    def __call__(self, w):
        if not self.signed:
            w = tf.abs(w)

        qw = tf.round(w / self.eps_val) * self.eps_val

        if self.signed:
            qw = tf.clip_by_value(qw, -self.po2, self.po2 - 1)
        else:
            qw = tf.clip_by_value(qw, 0, self.po2 - 1)

        return qw

    def get_config(self):
        config = super().get_config()

        config.update({
            'bits': self.bits,
            'integer': self.integer,
            'signed': self.signed
        })

        return config
