import tensorflow
import tensorflow as tf
from keras.engine.base_layer import Layer

from emtfnn.commands.training.endless_v5.custom.activations import qpt_activation_output, d0_activation_output


class HybridActivation(Layer):
    def __init__(self, no_activation_en, **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True

        self.no_activation_en = no_activation_en

        self.a = self.add_weight(
            name='a',
            shape=(2,),
            dtype=self.dtype,
            initializer=tensorflow.keras.initializers.constant(1),
            trainable=True)

        self.b = self.add_weight(
            name='b',
            shape=(1,),
            dtype=self.dtype,
            initializer=tensorflow.keras.initializers.constant(1),
            trainable=True)

        self.bias = self.add_weight(
            name='bias',
            shape=(2,),
            dtype=self.dtype,
            initializer='zeros',
            trainable=True)

    def call(self, inputs, mask=None):
        if self.no_activation_en:
            return inputs

        qpt = inputs[:, 0]
        d0 = inputs[:, 1]

        qpt = qpt_activation_output(qpt, a=self.a[0], b=self.b[0], bias=self.bias[0])
        d0 = d0_activation_output(d0, a=self.a[1], bias=self.bias[1])

        return tf.stack([qpt, d0], axis=1)

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        config = super().get_config()

        config.update({
            'no_activation_en': self.no_activation_en,
        })

        return config
