import math

import numpy as np

from emtfnn.execution.runtime import get_logger


class Phase1Dataset(object):

    def __init__(self, pos_input_file, neg_input_file, sample='train'):
        self.pos_input_file = pos_input_file
        self.neg_input_file = neg_input_file
        self.sample = sample

    def get_samples(self):
        pos_x_train, pos_y_train, \
        pos_x_test, pos_y_test = self._load_data(self.pos_input_file)
        neg_x_train, neg_y_train, \
        neg_x_test, neg_y_test = self._load_data(self.neg_input_file)

        # Trim to same size and stack
        max_sig_length = int(math.floor(min(pos_x_train.shape[0], neg_x_train.shape[0]) * 2) / 2)

        pos_x_train = pos_x_train[:max_sig_length]
        pos_y_train = pos_y_train[:max_sig_length]
        neg_x_train = neg_x_train[:max_sig_length]
        neg_y_train = neg_y_train[:max_sig_length]

        sig_x_train = np.concatenate((neg_x_train, pos_x_train))
        sig_y_train = np.concatenate((neg_y_train, pos_y_train))
        sig_x_test = np.concatenate((neg_x_test, pos_x_test))
        sig_y_test = np.concatenate((neg_y_test, pos_y_test))

        get_logger().info('Trimmed and merged signal variables to shape {0}'.format(sig_x_train.shape))
        get_logger().info('Trimmed and merged signal parameters to shape {0}'.format(sig_y_train.shape))

        # Format variables
        sig_x_train = sig_x_train.astype(np.float32)
        sig_x_test = sig_x_test.astype(np.float32)

        # Return
        get_logger().info(
            'Loaded # of training and testing events: {0}'.format((sig_x_train.shape[0], sig_x_test.shape[0]))
        )

        return sig_x_train, sig_y_train, \
               sig_x_test, sig_y_test

    def _load_data(self, input_file):
        get_logger().info('Loading data from {0} ...'.format(input_file))

        loaded_file = np.load(input_file)

        if self.sample == 'tune':
            x_train = loaded_file['variables_tune']
            y_train = loaded_file['parameters_tune']
            x_test = loaded_file['variables_ttest']
            y_test = loaded_file['parameters_ttest']
        elif self.sample == 'quant':
            x_train = loaded_file['variables_quant']
            y_train = loaded_file['parameters_quant']
            x_test = loaded_file['variables_qtest']
            y_test = loaded_file['parameters_qtest']
        else:
            x_train = loaded_file['variables_train']
            y_train = loaded_file['parameters_train']
            x_test = loaded_file['variables_test']
            y_test = loaded_file['parameters_test']

        def handle_nan_in_dphi(x, max_value):
            # dphi is 13 bits
            dphi = x[:, 0:6]
            mask = (np.isnan(dphi) | (dphi == -999.0) | (dphi == -99))
            dphi[mask] = 0  # np.random.random_integers(0, max_value, np.sum(mask))

            dphi_sign = x[:, 6:12]
            dphi_sign[mask] = 0  # np.random.random_integers(0, 1, np.sum(mask))

            return x

        def handle_nan_in_dtheta(x, max_value):
            # dtheta is 7 bits
            dtheta = x[:, 12:18]
            mask = (np.isnan(dtheta) | (dtheta == -999.0) | (dtheta == -99))
            dtheta[mask] = 0  # np.random.random_integers(0, max_value, np.sum(mask))

            dtheta_sign = x[:, 18:24]
            dtheta_sign[mask] = 0  # np.random.random_integers(0, 1, np.sum(mask))

            return x

        def handle_nan_in_pattern(x, max_value):
            pattern = x[:, 24:28]
            mask = (np.isnan(pattern) | (pattern == -999.0) | (pattern == -99))
            pattern[mask] = 0  # np.random.random_integers(0, max_value, np.sum(mask))

            return x

        def handle_nan_in_fr(x, max_value):
            fr_bit = x[:, 28:32]
            mask = (np.isnan(fr_bit) | (fr_bit == -999.0) | (fr_bit == -99) | (fr_bit == -1))
            fr_bit[mask] = 0  # np.random.random_integers(0, max_value, np.sum(mask))

            return x

        def handle_nan_in_me11(x, max_value):
            me11 = x[:, 36]
            mask = (np.isnan(me11) | (me11 == -999.0) | (me11 == -99) | (me11 == -1))
            me11[mask] = 0  # np.random.random_integers(0, max_value, np.sum(mask))

            return x

        def _refine(x_train, y_train):
            ##########################################
            # Mode Cleaning
            ##########################################
            x_mode = np.zeros((x_train.shape[0]))

            for i in range(x_train.shape[0]):
                s1_pat = x_train[i, 24]
                s2_pat = x_train[i, 25]
                s3_pat = x_train[i, 26]
                s4_pat = x_train[i, 27]

                mode = 0

                if s1_pat >= 0:
                    mode += 8

                if s2_pat >= 0:
                    mode += 4

                if s3_pat >= 0:
                    mode += 2

                if s4_pat >= 0:
                    mode += 1

                x_mode[i] = mode

            mask = (x_mode == 11) | (x_mode == 13) | \
                   (x_mode == 14) | (x_mode == 15)

            x_train = x_train[mask, :]
            y_train = y_train[mask, :]

            ##########################################
            # Handle invalid values
            ##########################################
            handle_nan_in_dphi(x_train, ((1 << 13) - 1))
            handle_nan_in_dtheta(x_train, ((1 << 7) - 1))
            handle_nan_in_pattern(x_train, 10)
            handle_nan_in_fr(x_train, 1)
            handle_nan_in_me11(x_train, 1)

            ##########################################
            # Select variables
            ##########################################
            variable_mask = [True] * x_train.shape[1]

            # Drop Dphi Signs
            # variable_mask[6:12] = [False] * 6
            #
            # Drop Dtheta Signs
            # variable_mask[18:24] = [False] * 6

            # Remove FR bits
            # variable_mask[28:32] = [False] * 4

            # Remove Chamber Ids
            variable_mask[32:36] = [False] * 4

            # Remove Me11 bit
            # variable_mask[36] = False

            # Remove Track Endcap and Sector
            variable_mask[37:39] = [False] * 2

            x_train = x_train[:, variable_mask]

            print('First entry (variables)')
            print(x_train[0, :])

            ##########################################
            # Select Parameters
            ##########################################
            qpt = y_train[:, 0]
            dxy = y_train[:, 1]
            eta_star = y_train[:, 2]
            phi = y_train[:, 3]
            vx = y_train[:, 4]
            vy = y_train[:, 5]
            vz = y_train[:, 6]

            ##########################################
            # Pack Parameters
            ##########################################
            y_train = np.column_stack((qpt, dxy, eta_star, phi, vx, vy, vz))

            print('First variables')
            print(x_train[0:5, :])

            print('First parameters')
            print(y_train[0:5, :])

            # Return
            return x_train, y_train

        x_train, y_train = _refine(x_train, y_train)
        x_test, y_test = _refine(x_test, y_test)

        get_logger().info('Loaded the train variables with shape {0}'.format(x_train.shape))
        get_logger().info('Loaded the train parameters with shape {0}'.format(y_train.shape))
        get_logger().info('Loaded the test variables with shape {0}'.format(x_test.shape))
        get_logger().info('Loaded the test parameters with shape {0}'.format(y_test.shape))

        return x_train, y_train, x_test, y_test
