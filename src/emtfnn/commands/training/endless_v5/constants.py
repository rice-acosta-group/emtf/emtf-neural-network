learning_rate = 0.01

final_learning_rate = learning_rate * 0.01

gradient_clipnorm = 10.

warmup_epochs = 30

epochs = 300

batch_size = 4096

internal_weight_bw = 22 # Phase-2
# internal_weight_bw = 18  # Run-3

internal_weight_integer = 6

qpt_dense_eps = 2 ** -13

d0_dense_eps = 1
