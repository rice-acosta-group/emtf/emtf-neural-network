import math

import numpy as np

from emtfnn.execution.runtime import get_logger


class Phase1_ZeroBias_Dataset(object):

    def __init__(self, input_file, sample='train'):
        self.input_file = input_file
        self.sample = sample

    def get_samples(self):
        x_train, y_train, \
        x_test, y_test = self._load_data(self.input_file)

        sig_x_train = x_train #unused
        sig_y_train = y_train #unused
        sig_x_test = x_test
        sig_y_test = y_test

        get_logger().info('Trimmed and merged signal variables to shape {0}'.format(sig_x_train.shape))
        get_logger().info('Trimmed and merged signal parameters to shape {0}'.format(sig_y_train.shape))

        # Format variables
        sig_x_train = sig_x_train.astype(np.float32)
        sig_x_test = sig_x_test.astype(np.float32)

        # Return
        get_logger().info(
            'Loaded # of training and testing events: {0}'.format((sig_x_train.shape[0], sig_x_test.shape[0]))
        )

        return sig_x_train, sig_y_train, \
               sig_x_test, sig_y_test

    def _load_data(self, input_file):
        get_logger().info('Loading data from {0} ...'.format(input_file))

        loaded_file = np.load(input_file)
        x_test = loaded_file['variables_test']
        y_test = loaded_file['parameters_test']

        def handle_nan_in_dphi(x, max_value):
            # dphi is 13 bits
            dphi = x[:, 0:6]
            mask = (np.isnan(dphi) | (dphi == -999.0) | (dphi == -99))
            dphi[mask] = 0  # np.random.random_integers(0, max_value, np.sum(mask))

            dphi_sign = x[:, 6:12]
            dphi_sign[mask] = 0  # np.random.random_integers(0, 1, np.sum(mask))

            return x

        def handle_nan_in_dtheta(x, max_value):
            # dtheta is 7 bits
            dtheta = x[:, 12:18]
            mask = (np.isnan(dtheta) | (dtheta == -999.0) | (dtheta == -99))
            dtheta[mask] = 0  # np.random.random_integers(0, max_value, np.sum(mask))

            dtheta_sign = x[:, 18:24]
            dtheta_sign[mask] = 0  # np.random.random_integers(0, 1, np.sum(mask))

            return x

        def handle_nan_in_pattern(x, max_value):
            pattern = x[:, 24:28]
            mask = (np.isnan(pattern) | (pattern == -999.0) | (pattern == -99))
            pattern[mask] = 0  # np.random.random_integers(0, max_value, np.sum(mask))

            return x

        def handle_nan_in_fr(x, max_value):
            fr_bit = x[:, 28:32]
            mask = (np.isnan(fr_bit) | (fr_bit == -999.0) | (fr_bit == -99) | (fr_bit == -1))
            fr_bit[mask] = 0  # np.random.random_integers(0, max_value, np.sum(mask))

            return x

        def handle_nan_in_me11(x, max_value):
            me11 = x[:, 36]
            mask = (np.isnan(me11) | (me11 == -999.0) | (me11 == -99) | (me11 == -1))
            me11[mask] = 0  # np.random.random_integers(0, max_value, np.sum(mask))

            return x

        def _refine(x_train, y_train):
            ##########################################
            # Get the mode info for zero bias
            ##########################################

            mode = np.where(x_train[:,24] >= 0, 8, 0)
            mode = np.where(x_train[:,25] >= 0, mode+4, mode)
            mode = np.where(x_train[:,26] >= 0, mode+2, mode)
            mode = np.where(x_train[:,27] >= 0, mode+1, mode)
            x_mode = mode

            ##########################################
            # Handle invalid values
            ##########################################
            handle_nan_in_dphi(x_train, ((1 << 13) - 1))
            handle_nan_in_dtheta(x_train, ((1 << 7) - 1))
            handle_nan_in_pattern(x_train, 10)
            handle_nan_in_fr(x_train, 1)
            handle_nan_in_me11(x_train, 1)

            ##########################################
            # Select variables
            ##########################################
            variable_mask = [True] * x_train.shape[1]

            # Drop Dphi Signs
            # variable_mask[6:12] = [False] * 6
            #
            # Drop Dtheta Signs
            # variable_mask[18:24] = [False] * 6

            # Remove FR bits
            # variable_mask[28:32] = [False] * 4

            # Remove Chamber Ids
            variable_mask[32:36] = [False] * 4

            # Remove Me11 bit
            # variable_mask[36] = False

            # Remove Track Endcap and Sector
            variable_mask[37:39] = [False] * 2

            x_train = x_train[:, variable_mask]

            print('First entry (variables)')
            print(x_train[0, :])

            ##########################################
            # Select Parameters
            ##########################################
            qpt = y_train[:, 0]
            dxy = y_train[:, 1]
            eta_star = y_train[:, 2]
            phi = y_train[:, 3]
            vx = y_train[:, 4]
            vy = y_train[:, 5]
            vz = y_train[:, 6]

            # Additional parameter for zero bias (the only real one actually)
            event_num = y_train[:,7]
            bx = y_train[:,8]

            ##########################################
            # Pack Parameters
            ##########################################
            y_train = np.column_stack((qpt, dxy, eta_star, phi, vx, vy, vz, event_num, x_mode, bx))

            print('First variables')
            print(x_train[0:5, :])

            print('First parameters')
            print(y_train[0:5, :])

            # Return
            return x_train, y_train

        x_test, y_test = _refine(x_test, y_test)

        get_logger().info('Loaded the test variables with shape {0}'.format(x_test.shape))
        get_logger().info('Loaded the test parameters with shape {0}'.format(y_test.shape))

        return x_test, y_test, x_test, y_test # just to keep it compatible
