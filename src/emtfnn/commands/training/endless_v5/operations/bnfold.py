import numpy as np
import tensorflow as tf

from emtfnn.commands.training.endless_v5 import constants
from emtfnn.commands.training.endless_v5.models import create_lr_schedule, create_optimizer, create_model
from emtfnn.commons.workbench import save_model


def fold_first_batch_norm(model_name,
                          base_model,
                          target):
    # Get Nodes
    model_in_nodes = base_model.get_layer(index=0).get_weights()[0].shape[-1]
    model_h1_nodes = base_model.get_layer(index=1).get_weights()[0].shape[-1]
    model_h2_nodes = base_model.get_layer(index=4).get_weights()[0].shape[-1]
    model_h3_nodes = base_model.get_layer(index=7).get_weights()[0].shape[-1]
    model_out_nodes = base_model.get_layer(index=11).get_weights()[0].shape[-1]

    model_hl_nodes = (model_h1_nodes, model_h2_nodes, model_h3_nodes)

    print('Nodes in model')
    print(model_in_nodes, model_hl_nodes, model_out_nodes)

    # Create Folded Model
    lr_schedule = create_lr_schedule(
        0,
        epochs=constants.epochs,
        warmup_epochs=constants.warmup_epochs,
        batch_size=constants.batch_size,
        learning_rate=constants.learning_rate,
        final_learning_rate=constants.final_learning_rate)

    optimizer = create_optimizer(
        lr_schedule,
        gradient_clipnorm=constants.gradient_clipnorm)

    model = create_model(
        optimizer,
        in_nodes=model_in_nodes,
        out_nodes=model_out_nodes,
        fold_en=True,
        target_parameter=target,
        hidden_layer_nodes=model_hl_nodes)

    # Fold First Batch Norm
    hl1_kernel, hl1_bias = get_prebn_folded_weights(
        base_model.layers[0],  # Batch Normalizaiton
        base_model.layers[1]  # Dense
    )

    model.get_layer(index=0).set_weights([hl1_kernel, hl1_bias])

    # Fold Second Batch Norm
    hl1_kernel, hl1_bias = get_postbn_folded_weights(
        base_model.layers[2],  # Batch Normalization
        model.layers[0]  # Dense
    )

    model.get_layer(index=0).set_weights([hl1_kernel, hl1_bias])

    # Get Weights
    hl2_kernel, hl2_bias = get_postbn_folded_weights(
        base_model.layers[5],  # Batch Normalization
        base_model.layers[4]  # Dense
    )
    hl3_kernel, hl3_bias = get_postbn_folded_weights(
        base_model.layers[8],  # Batch Normalization
        base_model.layers[7]  # Dense
    )

    # Load weights
    model.get_layer(index=2).set_weights([hl2_kernel, hl2_bias])
    model.get_layer(index=4).set_weights([hl3_kernel, hl3_bias])

    # Copy Original Weights
    direct_layers = [
        (3, 1),
        (6, 3),
        (9, 5),
        (10, 6),
        (11, 7)
    ]

    for pair in direct_layers:
        from_layer, to_layer = pair

        model.get_layer(index=to_layer).set_weights(
            base_model.get_layer(index=from_layer).get_weights()
        )

    # Save
    save_model(model, name=model_name)


def get_prebn_folded_weights(bn_layer, dense_layer):
    # Unpack Batch Norm Weights
    bn_weights = np.array(bn_layer.get_weights()).astype(np.double)
    gamma = bn_weights[0]
    beta = bn_weights[1]
    mov_mean = bn_weights[2]
    mov_var = bn_weights[3]
    epsilon = bn_layer.get_config()['epsilon']

    # Unpack Dense Layer Weights
    kernel = np.array(dense_layer.get_weights()[0]) \
        .astype(np.double)  # list of weights for each input node

    # Calculate new weights
    s0 = gamma / (tf.math.sqrt(mov_var + epsilon))  # "weights" for batch norm layer
    b0 = np.array(beta - (s0 * mov_mean))  # bias for batch-norm layer
    b0 = np.reshape(b0, (-1, 1))

    # Calculate
    new_kernel = np.reshape(s0, (-1, 1)) * kernel
    new_bias = np.sum(b0 * kernel, axis=0)

    # Return
    return new_kernel, new_bias


def get_postbn_folded_weights(bn_layer, dense_layer):
    # Unpack Batch Norm Weights
    bn_weights = np.array(bn_layer.get_weights()).astype(np.double)
    gamma = bn_weights[0]
    beta = bn_weights[1]
    mov_mean = bn_weights[2]
    mov_var = bn_weights[3]
    epsilon = bn_layer.get_config()['epsilon']

    # Unpack Dense Layer Weights
    kernel = np.array(dense_layer.get_weights()[0]) \
        .astype(np.double)  # list of weights for each input node

    # Calculate new weights
    s0 = gamma / (tf.math.sqrt(mov_var + epsilon))  # "weights" for batch norm layer
    b0 = np.array(beta - (s0 * mov_mean))  # bias for batch-norm layer

    # Calculate
    new_kernel = s0 * kernel
    new_bias = b0

    # Check for bias
    if len(dense_layer.get_weights()) > 1:
        bias = np.array(dense_layer.get_weights()[1]).astype(np.double)
        new_bias += s0 * bias

    # Return
    return new_kernel, new_bias
