import numpy as np

from emtfnn.commands.training.endless_v5 import constants
from emtfnn.commands.training.endless_v5.models import create_optimizer, create_model, create_lr_schedule
from emtfnn.commons.workbench import save_model


def stitch(model_name,
           qpt_model,
           d0_model):
    # Get weights
    def _get_weights(model):
        hl1_weights = model.get_layer(index=0).get_weights()  # First Dense
        hl2_weights = model.get_layer(index=2).get_weights()  # Second Dense
        hl3_weights = model.get_layer(index=4).get_weights()  # Third Dense
        hl4_weights = model.get_layer(index=6).get_weights()  # Fourth Dense
        out_weights = model.get_layer(index=7).get_weights()  # Output Activation
        return (hl1_weights, hl2_weights,
                hl3_weights, hl4_weights, out_weights)

    qpt_weights = _get_weights(qpt_model)
    d0_weights = _get_weights(d0_model)

    # Identify number of nodes
    def _get_dense_layer_nodes(weights):
        hl1_shape = weights[0][0].shape  # First Dense
        hl2_shape = weights[1][0].shape  # Second Dense
        hl3_shape = weights[2][0].shape  # Third Dense
        hl4_shape = weights[3][0].shape  # Last Dense
        return (hl1_shape, hl2_shape, hl3_shape, hl4_shape)

    pt_hl_nodes = _get_dense_layer_nodes(qpt_weights)
    d0_hl_nodes = _get_dense_layer_nodes(d0_weights)

    assert (pt_hl_nodes == d0_hl_nodes, 'Should be of equal nodes')

    # Determine Internal Layer Nodes
    model_in_nodes = qpt_weights[0][0].shape[0]
    model_hl1_nodes = qpt_weights[0][0].shape[-1] + d0_weights[0][0].shape[-1]
    model_hl2_nodes = qpt_weights[1][0].shape[-1] + d0_weights[1][0].shape[-1]
    model_hl3_nodes = qpt_weights[2][0].shape[-1] + d0_weights[2][0].shape[-1]
    model_out_act_nodes = qpt_weights[4][0].shape[-1] + d0_weights[4][0].shape[-1]

    model_hl_nodes = (model_hl1_nodes, model_hl2_nodes, model_hl3_nodes)

    print(model_in_nodes, (model_hl1_nodes, model_hl2_nodes, model_hl3_nodes), model_out_act_nodes)

    # Create Stitched Model
    lr_schedule = create_lr_schedule(
        0,
        epochs=constants.epochs,
        warmup_epochs=constants.warmup_epochs,
        batch_size=constants.batch_size,
        learning_rate=constants.learning_rate,
        final_learning_rate=constants.final_learning_rate)

    optimizer = create_optimizer(
        lr_schedule,
        gradient_clipnorm=constants.gradient_clipnorm)

    model = create_model(
        optimizer,
        in_nodes=model_in_nodes,
        out_nodes=model_out_act_nodes,
        hidden_layer_nodes=model_hl_nodes,
        fold_en=True)

    # Get model weights
    model_weights = _get_weights(model)

    # Create Weight Container
    # Note: Output Nodes = 2
    model_hl1_kernel = np.zeros(model_weights[0][0].shape)
    model_hl1_bias = np.zeros(model_weights[0][1].shape)
    model_hl2_kernel = np.zeros(model_weights[1][0].shape)
    model_hl2_bias = np.zeros(model_weights[1][1].shape)
    model_hl3_kernel = np.zeros(model_weights[2][0].shape)
    model_hl3_bias = np.zeros(model_weights[2][1].shape)
    model_hl4_kernel = np.zeros(model_weights[3][0].shape)

    # Copy weights
    def _copy_bn_weights(source, target, offset):
        for param_id in range(len(source)):
            source_param_weights = source[param_id]
            target_param_weights = target[param_id]

            for weight_id in range(len(source_param_weights)):
                target_param_weights[weight_id + offset] = source_param_weights[weight_id]

    def _copy_kernel_weights(source, target, offset_in, offset_out):
        for in_id in range(len(source)):
            source_row_weights = source[in_id]
            target_row_weights = target[in_id + offset_in]

            for out_id in range(source_row_weights.shape[0]):
                target_row_weights[out_id + offset_out] = source_row_weights[out_id]

    def _copy_bias_weights(source, target, offset_out):
        for out_id in range(source.shape[0]):
            target[out_id + offset_out] = source[out_id]

    # ===========================

    # Copy First Dense Kernel
    # Note: No input offset since both neural networks expect the same number of inputs
    layer_id = 0
    print('Copying first dense kernel')
    _copy_kernel_weights(qpt_weights[layer_id][0], model_hl1_kernel, 0, 0)
    _copy_kernel_weights(d0_weights[layer_id][0], model_hl1_kernel, 0, qpt_weights[layer_id][0].shape[1])

    print('Copying first dense bias')
    _copy_bias_weights(
        qpt_weights[layer_id][1],
        model_hl1_bias,
        0
    )
    _copy_bias_weights(
        d0_weights[layer_id][1],
        model_hl1_bias,
        len(qpt_weights[layer_id][1])
    )

    # ===========================

    # Copy Second Dense Kernel
    # Note: Input offset the first neural network will input half the previous layer and output half the next layer
    layer_id = 1
    print('Copying second dense kernel')
    _copy_kernel_weights(
        qpt_weights[layer_id][0],
        model_hl2_kernel,
        0, 0
    )
    _copy_kernel_weights(
        d0_weights[layer_id][0],
        model_hl2_kernel,
        len(qpt_weights[layer_id][0]),  # Offset d0 inputs
        qpt_weights[layer_id][0].shape[1]
    )

    print('Copying second dense bias')
    _copy_bias_weights(
        qpt_weights[layer_id][1],
        model_hl2_bias,
        0
    )
    _copy_bias_weights(
        d0_weights[layer_id][1],
        model_hl2_bias,
        len(qpt_weights[layer_id][1])
    )

    # ===========================

    # Copy Third Dense Kernel
    # Note: Input offset the first neural network will input half the previous layer and output half the next layer
    layer_id = 2
    print('Copying third dense kernel')
    _copy_kernel_weights(
        qpt_weights[layer_id][0],
        model_hl3_kernel,
        0, 0
    )
    _copy_kernel_weights(
        d0_weights[layer_id][0],
        model_hl3_kernel,
        len(qpt_weights[layer_id][0]),  # Offset d0 inputs
        qpt_weights[layer_id][0].shape[1]
    )

    print('Copying third dense bias')
    _copy_bias_weights(
        qpt_weights[layer_id][1],
        model_hl3_bias,
        0
    )
    _copy_bias_weights(
        d0_weights[layer_id][1],
        model_hl3_bias,
        len(qpt_weights[layer_id][1])
    )

    # ===========================

    # Copy Fourth Dense Kernel
    # Note: Input offset the first neural network will input half the previous layer and output half the next layer
    layer_id = 3
    print('Copying fourth dense kernel')
    _copy_kernel_weights(
        qpt_weights[layer_id][0],
        model_hl4_kernel,
        0, 0
    )
    _copy_kernel_weights(
        d0_weights[layer_id][0],
        model_hl4_kernel,
        len(qpt_weights[layer_id][0]),  # Offset d0 inputs
        qpt_weights[layer_id][0].shape[1]
    )

    # Copy Output Activation Weights
    layer_id = 4
    print('Copying output activation weights')

    qpt_a, qpt_b, qpt_bias = qpt_weights[layer_id][0:]
    d0_a, d0_bias = d0_weights[layer_id][0:]
    a = np.concatenate((qpt_a, d0_a))
    bias = np.concatenate((qpt_bias, d0_bias))

    # Set weights
    model.get_layer(index=0).set_weights([model_hl1_kernel, model_hl1_bias])  # First Dense
    model.get_layer(index=2).set_weights([model_hl2_kernel, model_hl2_bias])  # Second Dense
    model.get_layer(index=4).set_weights([model_hl3_kernel, model_hl3_bias])  # Third Dense
    model.get_layer(index=6).set_weights([model_hl4_kernel])  # Pre-Output
    model.get_layer(index=7).set_weights([a, qpt_b, bias])  # Output

    # Save Model
    save_model(model, name=model_name)
