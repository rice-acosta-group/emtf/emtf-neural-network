import math

import numpy as np
import tensorflow
from keras.layers import InputLayer, BatchNormalization, Dense, Activation
from keras.models import Sequential

from emtfnn.commands.training.endless_v5.constants import internal_weight_bw, internal_weight_integer
from emtfnn.commands.training.endless_v5.custom.d0_activation import D0Activation
from emtfnn.commands.training.endless_v5.custom.hybrid_activation import HybridActivation
from emtfnn.commands.training.endless_v5.custom.qpt_activation import QPtActivation
from emtfnn.commands.training.endless_v5.custom.weight_constraint import QuantizedWeight
from emtfnn.keras.losses import logcosh_error
from emtfnn.keras.optimizers import Adamu, WarmupCosineDecay, WarmupExponentialDecay


###########################################################################
# Optimizers
##########################################################################
def create_lr_schedule(num_train_samples,
                       epochs=100,
                       warmup_epochs=30,
                       batch_size=32,
                       learning_rate=0.001,
                       final_learning_rate=0.00001):
    # Create learning rate schedule with warmup and cosine decay
    steps_per_epoch = int(np.ceil(num_train_samples / float(batch_size)))
    warmup_steps = steps_per_epoch * warmup_epochs
    total_steps = steps_per_epoch * epochs
    cosine_decay_alpha = final_learning_rate / learning_rate

    assert warmup_steps <= total_steps

    return WarmupCosineDecay(
        initial_learning_rate=learning_rate, warmup_steps=warmup_steps,
        decay_steps=(total_steps - warmup_steps), alpha=cosine_decay_alpha)


def create_exponential_lr_schedule(num_train_samples,
                                   epochs=100,
                                   warmup_epochs=30,
                                   batch_size=32,
                                   learning_rate=0.001,
                                   final_learning_rate=0.00001,
                                   decay_rate=0.9,
                                   staircase=True):
    assert warmup_epochs <= epochs

    # Create learning rate schedule with warmup and cosine decay
    steps_per_epoch = int(np.ceil(num_train_samples / float(batch_size)))
    warmup_steps = steps_per_epoch * warmup_epochs

    decay_epochs = (epochs - warmup_epochs)
    decay_step = (decay_epochs * steps_per_epoch) * math.log(decay_rate) / math.log(final_learning_rate / learning_rate)

    return WarmupExponentialDecay(
        initial_learning_rate=learning_rate, warmup_steps=warmup_steps,
        decay_steps=decay_step, decay_rate=decay_rate,
        staircase=staircase)


def create_optimizer(lr_schedule, gradient_clipnorm=10000):
    # Create optimizer with lr_schedule and clipnorm
    optimizer = Adamu(learning_rate=lr_schedule, clipnorm=gradient_clipnorm)

    return optimizer


###########################################################################
# Loss Functions
###########################################################################

def qpt_loss(y_true, y_pred):
    pt_true = y_true[:, 0]
    q_true = tensorflow.where(pt_true < 0, -tensorflow.ones_like(pt_true), tensorflow.ones_like(pt_true))
    pt_true = tensorflow.math.abs(pt_true)

    pt_pred = y_pred[:, 0]
    q_pred = tensorflow.where(pt_pred < 0, -tensorflow.ones_like(pt_pred), tensorflow.ones_like(pt_pred))
    pt_pred = tensorflow.math.abs(pt_pred)

    invpt_true = tensorflow.math.reciprocal_no_nan(pt_true + 1e-12)
    invpt_pred = tensorflow.math.reciprocal_no_nan(pt_pred + 1e-12)

    invpt_true = tensorflow.math.multiply(invpt_true, q_true)
    invpt_pred = tensorflow.math.multiply(invpt_pred, q_pred)

    loss = logcosh_error(invpt_true, invpt_pred)

    return loss


def d0_loss(y_true, y_pred):
    dxy_true = y_true[:, 0]
    dxy_pred = y_pred[:, 0]

    loss = logcosh_error(dxy_true, dxy_pred)

    return loss


###########################################################################
# Models
###########################################################################

def create_model(optimizer,
                 in_nodes=40,
                 out_nodes=1,
                 fold_en=False,
                 quantized_en=False,
                 hidden_layer_nodes=[28, 24, 16],
                 target_parameter=None,
                 no_activation_en=False):
    # Parameters
    momentum = 0.85
    epsilon = 1e-4

    # Create
    model = Sequential()
    model.add(InputLayer(input_shape=(in_nodes,), name="inputs"))

    def _gen_bn_constraints():
        beta_constraint = None
        gamma_constraint = None

        if quantized_en:
            beta_constraint = QuantizedWeight(internal_weight_bw, internal_weight_integer, 1)
            gamma_constraint = QuantizedWeight(internal_weight_bw, internal_weight_integer, 1)

        return beta_constraint, gamma_constraint

    def _gen_nn_constraints():
        kernel_constraint = None
        bias_constraint = None

        if quantized_en:
            kernel_constraint = QuantizedWeight(internal_weight_bw, internal_weight_integer, 1)
            bias_constraint = QuantizedWeight(internal_weight_bw, internal_weight_integer, 1)

        return kernel_constraint, bias_constraint

    # Adding 1 BN layer right after the input layer
    if not fold_en:
        bn0_beta_constraint, bn0_gamma_constraint = _gen_bn_constraints()

        bn0_layer = BatchNormalization(
            epsilon=epsilon, momentum=momentum,
            beta_constraint=bn0_beta_constraint,
            gamma_constraint=bn0_gamma_constraint
        )
        model.add(bn0_layer)

    # Add Dense Layers
    for n_nodes in hidden_layer_nodes:
        kernel_constraint, bias_constraint = _gen_nn_constraints()

        model.add(Dense(
            n_nodes,
            kernel_initializer='glorot_uniform',
            kernel_constraint=kernel_constraint,
            bias_constraint=bias_constraint,
            use_bias=fold_en
        ))

        if not fold_en:
            beta_constraint, gamma_constraint = _gen_bn_constraints()

            model.add(BatchNormalization(
                epsilon=epsilon, momentum=momentum,
                beta_constraint=beta_constraint,
                gamma_constraint=gamma_constraint
            ))

        activation = Activation('relu')
        model.add(activation)

    # Output
    kernel_constraint, bias_constraint = _gen_nn_constraints()

    model.add(Dense(
        out_nodes,
        kernel_initializer='glorot_uniform',
        kernel_constraint=kernel_constraint,
        bias_constraint=bias_constraint,
        use_bias=False
    ))

    if target_parameter is None:
        loss_fn = qpt_loss
        loss_weight = 1

        model.add(HybridActivation(no_activation_en))
    elif target_parameter == 'qpt':
        loss_fn = qpt_loss
        loss_weight = 1000

        model.add(QPtActivation(quantized_en, no_activation_en))
    elif target_parameter == 'd0':
        loss_fn = d0_loss
        loss_weight = 1

        model.add(D0Activation(quantized_en, no_activation_en))
    else:
        raise ValueError('Invalid Target Parameter')

    model.compile(optimizer=optimizer, loss=loss_fn, loss_weights=loss_weight)
    model.summary()

    return model
