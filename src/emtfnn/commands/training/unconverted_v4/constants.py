# Training
learning_rate = 0.005

final_learning_rate = learning_rate * 0.02

gradient_clipnorm = 10.

warmup_epochs = 30

epochs = 300

batch_size = 2048

# Quantization
quant_width_bits = 24
quant_int_bits = 8
quant_frac_bits = (quant_width_bits - quant_int_bits)
max_quant_int = pow(2, quant_int_bits) - 1
max_quant_frac = 1 - pow(2, -quant_frac_bits)
max_quant_val = pow(2, quant_int_bits) - pow(2, -quant_frac_bits)
