import numpy as np
import tensorflow
from keras import regularizers
from keras.layers import InputLayer, BatchNormalization, Dense, Activation
from keras.models import Sequential

from emtfnn.keras.losses import logcosh_error
from emtfnn.keras.optimizers import Adamu, WarmupCosineDecay


###########################################################################
# Optimizers
###########################################################################

def create_lr_schedule(num_train_samples,
                       epochs=100,
                       warmup_epochs=30,
                       batch_size=32,
                       learning_rate=0.001,
                       final_learning_rate=0.00001):
    # Create learning rate schedule with warmup and cosine decay
    steps_per_epoch = int(np.ceil(num_train_samples / float(batch_size)))
    warmup_steps = steps_per_epoch * warmup_epochs
    total_steps = steps_per_epoch * epochs
    cosine_decay_alpha = final_learning_rate / learning_rate

    assert warmup_steps <= total_steps

    return WarmupCosineDecay(
        initial_learning_rate=learning_rate, warmup_steps=warmup_steps,
        decay_steps=(total_steps - warmup_steps), alpha=cosine_decay_alpha)


def create_optimizer(lr_schedule, gradient_clipnorm=10000):
    # Create optimizer with lr_schedule and clipnorm
    optimizer = Adamu(learning_rate=lr_schedule, clipnorm=gradient_clipnorm)

    return optimizer


###########################################################################
# Loss Functions
###########################################################################

def invpt_loss(y_true, y_pred):
    pt_true = y_true[:, 0]
    pt_pred = y_pred[:, 0]

    invpt_true = tensorflow.math.reciprocal_no_nan(pt_true + 1e-6)
    invpt_pred = tensorflow.math.reciprocal_no_nan(pt_pred + 1e-6)

    loss_invpt = logcosh_error(invpt_true / 0.01, invpt_pred / 0.01)

    return loss_invpt


def dxy_loss(y_true, y_pred):
    dxy_true = y_true[:, 0]
    dxy_pred = y_pred[:, 0]

    loss_dxy = logcosh_error(dxy_true, dxy_pred)

    return loss_dxy


###########################################################################
# Models
###########################################################################

def create_model(optimizer,
                 in_nodes=30,
                 out_nodes=2,
                 hidden_layer_nodes=[64, 32, 16],
                 target_parameter=None,
                 partner_model=None):
    # Common
    regularizer = regularizers.L1L2(l1=0, l2=0)

    # Parameters
    momentum = 0.85
    epsilon = 1e-4

    # Create
    model = Sequential()

    model.add(InputLayer(input_shape=(in_nodes,), name="input1"))

    # Adding 1 BN layer right after the input layer
    bn0_layer = BatchNormalization(epsilon=epsilon, momentum=momentum)
    model.add(bn0_layer)

    if partner_model is not None:
        bn0_weights = partner_model.get_layer(index=0).get_weights()
        bn0_layer(tensorflow.zeros((1, in_nodes)))
        bn0_layer.set_weights(bn0_weights)
        bn0_layer.trainable = False

    for n_nodes in hidden_layer_nodes:
        model.add(Dense(n_nodes, kernel_initializer='glorot_uniform', kernel_regularizer=regularizer, use_bias=False))
        model.add(BatchNormalization(epsilon=epsilon, momentum=momentum))
        model.add(Activation('relu'))

    # Output
    if target_parameter is None:
        loss_fn = invpt_loss
        model.add(Dense(out_nodes, activation='relu', kernel_initializer='glorot_uniform'))
    elif target_parameter == 'pt':
        loss_fn = invpt_loss
        model.add(Dense(out_nodes, activation='relu', kernel_initializer='glorot_uniform'))
    elif target_parameter == 'dxy':
        loss_fn = dxy_loss
        model.add(Dense(out_nodes, activation='relu', kernel_initializer='glorot_uniform'))
    else:
        raise ValueError('Invalid Target Parameter')

    model.compile(optimizer=optimizer, loss=loss_fn)
    model.summary()

    return model
