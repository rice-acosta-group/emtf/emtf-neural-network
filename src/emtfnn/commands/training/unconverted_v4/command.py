import numpy as np
from keras.callbacks import ModelCheckpoint
from keras.layers import BatchNormalization

from emtfnn.commands.training.unconverted_v4.constants import gradient_clipnorm, epochs, warmup_epochs, \
    final_learning_rate, batch_size
from emtfnn.commands.training.unconverted_v4.constants import learning_rate
from emtfnn.commands.training.unconverted_v4.datasets import TrainingDataset
from emtfnn.commands.training.unconverted_v4.phase1_zerobias_datasets import Phase1_ZeroBias_Dataset
from emtfnn.commands.training.unconverted_v4.models import create_model, create_lr_schedule, \
    create_optimizer, invpt_loss, dxy_loss
from emtfnn.commons.analyzers.udxy_training import UnsignedDxyTrainingAnalyzer
from emtfnn.commons.analyzers.pt_training import PtTrainingAnalyzer
from emtfnn.commons.analyzers.disp_nn_rates import DxyRateAnalyzer
from emtfnn.commons.plotting import plot_variables
from emtfnn.commons.trainers import ModelTrainer
from emtfnn.commons.workbench import dump_vars_params
from emtfnn.commons.workbench import load_model
from emtfnn.commons.workbench import save_model
from emtfnn.execution.runtime import get_logger
from emtfnn.keras.loggers import LearningRateLogger

from emtfnn.keras.optimizers import Adamu, WarmupCosineDecay

from emtfnn.hls.phase1_v4.build_hls_model import build_and_test_hls_model


def configure_parser(parser):
    parser.add_argument(dest='model_name', metavar='MODEL_NAME', type=str, help='Model Name')
    parser.add_argument(dest='neg_input_file', metavar='N_INPUT_FILE', type=str, help='Negative Endcap Input File')
    parser.add_argument(dest='pos_input_file', metavar='P_INPUT_FILE', type=str, help='Positive Endcap Input File')

    parser.add_argument('--nodes', dest='nodes', metavar='NODES',
                        type=str, default=None,
                        help='List of number of nodes')

    parser.add_argument('--target', dest='target', metavar='TARGET',
                        type=str, default=None,
                        help='target parameter')

    parser.add_argument('--partner-model', dest='partner_model', metavar='PARTNER_MODEL',
                        type=str, default=None,
                        help='partner model')

    parser.add_argument('--pt-model', dest='pt_model', metavar='PT_MODEL',
                        type=str, default=None,
                        help='pt model')

    parser.add_argument('--dxy-model', dest='dxy_model', metavar='DXY_MODEL',
                        type=str, default=None,
                        help='dxy model')

    parser.add_argument('--train', dest='train_en', action='store_const',
                        const=True, default=False,
                        help='Train')

    parser.add_argument('--merge', dest='merge_en', action='store_const',
                        const=True, default=False,
                        help='Merge')

    parser.add_argument('--use-unsigned-deltas', dest='use_unsigned_deltas', action='store_const',
                        const=True, default=False,
                        help='Use unsigned deltas')

    parser.add_argument('--use-unsigned-dxy', dest='use_unsigned_dxy', action='store_const',
                        const=True, default=False,
                        help='Use unsigned dxy')
    
    parser.add_argument('--build-hls-model', dest='build_hls_model', action='store_const',
                    const=True, default=False,
                    help='Run hls4ml model conversion')
    
    parser.add_argument('--test-rate', dest='test_rate', action='store_const',
                    const=True, default=False,
                    help='Run Rate Test - must input zero bias sample')


def run(args):
    # Unpack Args
    model_name = args.model_name
    neg_input_file = args.neg_input_file
    pos_input_file = args.pos_input_file

    nodes = args.nodes
    target = args.target
    partner_model = args.partner_model
    pt_model = args.pt_model
    dxy_model = args.dxy_model
    merge_en = args.merge_en
    train_en = args.train_en
    unsigned_deltas_en = args.use_unsigned_deltas
    unsigned_dxy_en = args.use_unsigned_dxy

    build_hls_en = args.build_hls_model
    test_rate_en = args.test_rate

    # Check inputs
    inputs_consistent = (neg_input_file is not None and pos_input_file is not None)

    assert inputs_consistent, "Inputs are invalid. Check you\'re not missing an inputfile or that they are not empty"

    # Sanitize Args
    if nodes is not None:
        nodes = nodes.split(',')
        nodes = [int(n) for n in nodes]

    # Load Dataset
    dataset = TrainingDataset(pos_input_file, neg_input_file)

    if test_rate_en:
        dataset = Phase1_ZeroBias_Dataset(pos_input_file)

    dataset.unsigned_deltas_en = unsigned_deltas_en
    dataset.unsigned_dxy_en = unsigned_dxy_en

    x_train, y_train, \
    x_test, y_test = dataset.get_samples()

    # Unpack parameters
    original_x_test = np.copy(x_test)
    original_y_test = np.copy(y_test)

    pt_train, dxy_train = y_train[:, 0], y_train[:, 1]
    pt_test, dxy_test = y_test[:, 0], y_test[:, 1]

    # Stack Parameters
    y_train = np.column_stack((pt_train, dxy_train))
    y_test = np.column_stack((pt_test, dxy_test))

    # Mask Parameters
    if target is None:
        y_mask = [True, True]
    elif target in ('pt'):
        y_mask = [True, False]
    elif target in ('dxy'):
        y_mask = [False, True]

    y_train = y_train[:, y_mask]
    y_test = y_test[:, y_mask]

    # Plot Variables
    plot_variables(x_train, 'variables.png')
    plot_variables(y_train, 'parameters.png')

    # Train
    if merge_en:
        merge_models(model_name,
                     pt_model,
                     dxy_model)
    elif train_en:
        train_submodel(model_name,
                       x_train, y_train,
                       x_test, y_test,
                       nodes=nodes,
                       target=target,
                       partner_model=partner_model)


    if build_hls_en:
        build_and_analyze_hls(model_name, x_train, x_test, original_x_test, original_y_test, y_mask)
    elif test_rate_en:
        test_model_rate(model_name, x_test, original_y_test)
    else:
        # Analyze Performance
        analyze_performance(model_name, original_x_test, original_y_test, y_mask)


def merge_models(
        model_name,
        pt_model_name,
        dxy_model_name):
    pt_model = _load_static_model(pt_model_name)
    dxy_model = _load_static_model(dxy_model_name)

    # Get weights
    def _get_weights(model):
        bn0_weights = model.get_layer(index=0).get_weights()  # First Batch Norm
        hl1_weights = model.get_layer(index=1).get_weights()  # First Dense
        bn1_weights = model.get_layer(index=2).get_weights()  # First Dense Batch Norm
        hl2_weights = model.get_layer(index=4).get_weights()  # Second Dense
        bn2_weights = model.get_layer(index=5).get_weights()  # Second Dense Batch Norm
        out_weights = model.get_layer(index=7).get_weights()  # Last Dense

        return (bn0_weights, hl1_weights, bn1_weights, hl2_weights, bn2_weights, out_weights)

    pt_weights = _get_weights(pt_model)
    dxy_weights = _get_weights(dxy_model)

    # Identify number of nodes
    def _get_dense_layer_nodes(weights):
        hl0_shape = weights[1][0].shape  # First Dense
        hl1_shape = weights[3][0].shape  # Second Dense
        hl2_shape = weights[5][0].shape  # Last Dense
        return (hl0_shape, hl1_shape, hl2_shape)

    pt_hl_nodes = _get_dense_layer_nodes(pt_weights)
    dxy_hl_nodes = _get_dense_layer_nodes(dxy_weights)

    assert pt_hl_nodes == dxy_hl_nodes, 'Should be of equal nodes'

    # Determine Internal Layer Nodes
    model_in_nodes = pt_weights[0][0].shape[-1]
    model_hl1_nodes = pt_weights[1][0].shape[-1] + dxy_weights[1][0].shape[-1]
    model_hl2_nodes = pt_weights[3][0].shape[-1] + dxy_weights[3][0].shape[-1]
    model_out_nodes = pt_weights[5][0].shape[-1] + dxy_weights[5][0].shape[-1]

    model_hl_nodes = (model_hl1_nodes, model_hl2_nodes)

    print(model_in_nodes, model_hl1_nodes, model_hl2_nodes, model_out_nodes)

    # Create Model
    lr_schedule = create_lr_schedule(
        0,
        epochs=epochs,
        warmup_epochs=warmup_epochs,
        batch_size=batch_size,
        learning_rate=learning_rate,
        final_learning_rate=final_learning_rate)

    optimizer = create_optimizer(
        lr_schedule,
        gradient_clipnorm=gradient_clipnorm)

    model = create_model(
        optimizer,
        in_nodes=model_in_nodes,
        out_nodes=model_out_nodes,
        hidden_layer_nodes=model_hl_nodes)

    # Get model weights
    model_weights = _get_weights(model)

    # Create Weight Container
    # Note: Output Nodes = 2
    model_bn0_weights = np.zeros((4, model_weights[0][0].shape[0]))
    model_hl1_kernel = np.zeros(model_weights[1][0].shape)
    model_bn1_weights = np.zeros((4, model_weights[2][0].shape[0]))
    model_hl2_kernel = np.zeros(model_weights[3][0].shape)
    model_bn2_weights = np.zeros((4, model_weights[4][0].shape[0]))
    model_out_kernel = np.zeros(model_weights[5][0].shape)
    model_out_bias = np.zeros(model_weights[5][1].shape)

    # Copy weights
    def _copy_bn_weights(source, target, offset):
        for param_id in range(len(source)):
            source_param_weights = source[param_id]
            target_param_weights = target[param_id]

            for weight_id in range(len(source_param_weights)):
                target_param_weights[weight_id + offset] = source_param_weights[weight_id]

    def _copy_kernel_weights(source, target, offset_in, offset_out):
        for in_id in range(len(source)):
            source_row_weights = source[in_id]
            target_row_weights = target[in_id + offset_in]

            # print(source_row_weights)
            # print(target_row_weights)

            for out_id in range(source_row_weights.shape[0]):
                target_row_weights[out_id + offset_out] = source_row_weights[out_id]

            # print(target_row_weights)

    def _copy_bias_weights(source, target, offset_out):
        for out_id in range(source.shape[0]):
            target[out_id + offset_out] = source[out_id]

        # print(target_row_weights)

    # Copy Zero BatchNorm
    # Note: Only copy from one, since they share same BN0 Layer
    layer_id = 0
    print('Copying zero-th batch normalization weights')
    _copy_bn_weights(dxy_weights[layer_id], model_bn0_weights, 0)

    print(pt_weights[layer_id])
    print(dxy_weights[layer_id])
    # print(model_bn0_weights)

    # Copy First Dense Kernel
    # Note: No input offset since both neural networks expect the same number of inputs
    layer_id = 1
    print('Copying first dense kernel')
    _copy_kernel_weights(pt_weights[layer_id][0], model_hl1_kernel, 0, 0)
    _copy_kernel_weights(dxy_weights[layer_id][0], model_hl1_kernel, 0, pt_weights[layer_id][0].shape[1])

    # print(model_hl1_kernel)

    # Copy First BatchNorm
    layer_id = 2
    print('Copying first batch normalization weights')
    _copy_bn_weights(pt_weights[layer_id], model_bn1_weights, 0)
    _copy_bn_weights(dxy_weights[layer_id], model_bn1_weights, len(pt_weights[layer_id][0]))

    # print(model_bn1_weights)

    # Copy Second Dense Kernel
    # Note: Input offset the first neural network will input half the previous layer and output half the next layer
    layer_id = 3
    print('Copying second dense kernel')
    _copy_kernel_weights(
        pt_weights[layer_id][0],
        model_hl2_kernel,
        0, 0
    )
    _copy_kernel_weights(
        dxy_weights[layer_id][0],
        model_hl2_kernel,
        len(pt_weights[layer_id][0]),  # Offset dxy inputs
        pt_weights[layer_id][0].shape[1]
    )

    # print(model_hl2_kernel)

    # Copy Second BatchNorm
    layer_id = 4
    print('Copying second batch normalization weights')
    _copy_bn_weights(pt_weights[layer_id], model_bn2_weights, 0)
    _copy_bn_weights(dxy_weights[layer_id], model_bn2_weights, len(pt_weights[layer_id][0]))

    # print(model_bn2_weights)

    # Copy Output Kernel
    # Note: Input offset the first neural network will input half the previous layer and output half the next layer
    layer_id = 5
    print('Copying output kernel')
    _copy_kernel_weights(
        pt_weights[layer_id][0],
        model_out_kernel,
        0, 0
    )

    _copy_kernel_weights(
        dxy_weights[layer_id][0],
        model_out_kernel,
        len(pt_weights[layer_id][0]),  # Offset dxy inputs
        pt_weights[layer_id][0].shape[1]
    )

    print(model_out_kernel)

    # Copy Output Bias
    layer_id = 5
    print('Copying output bias')
    _copy_bias_weights(
        pt_weights[layer_id][1],
        model_out_bias,
        0
    )

    _copy_bias_weights(
        dxy_weights[layer_id][1],
        model_out_bias,
        len(pt_weights[layer_id][1])
    )

    # print(model_out_bias)

    # Set weights
    model.get_layer(index=0).set_weights([model_bn0_weights[i] for i in range(4)])  # First Batch Norm
    model.get_layer(index=1).set_weights([model_hl1_kernel])  # First Dense
    model.get_layer(index=2).set_weights([model_bn1_weights[i] for i in range(4)])  # First Dense Batch Norm
    model.get_layer(index=4).set_weights([model_hl2_kernel])  # Second Dense
    model.get_layer(index=5).set_weights([model_bn2_weights[i] for i in range(4)])  # Second Dense Batch Norm
    model.get_layer(index=7).set_weights([model_out_kernel, model_out_bias])  # Last Dense

    # Save Model
    save_model(model, name=model_name)


def train_submodel(
        model_name,
        x_train, y_train,
        x_test, y_test,
        nodes=[20, 15],
        target='pt',
        partner_model=None):
    if partner_model is not None:
        partner_model = _load_static_model(partner_model)

    lr_schedule = create_lr_schedule(
        x_train.shape[0],
        epochs=epochs,
        warmup_epochs=warmup_epochs,
        batch_size=batch_size,
        learning_rate=learning_rate,
        final_learning_rate=final_learning_rate)

    optimizer = create_optimizer(
        lr_schedule,
        gradient_clipnorm=gradient_clipnorm)

    model = create_model(
        optimizer,
        in_nodes=x_train.shape[1],
        out_nodes=y_train.shape[1],
        hidden_layer_nodes=nodes,
        target_parameter=target,
        partner_model=partner_model)

    lr_logger = LearningRateLogger()
    model_best_check = ModelCheckpoint(filepath='model_bchk.h5', monitor='val_loss', verbose=1, save_best_only=True)
    model_best_check_weights = ModelCheckpoint(filepath='model_bchk_weights.h5', monitor='val_loss', verbose=1,
                                               save_best_only=True, save_weights_only=True)

    # Train Model
    get_logger().info('Training model')
    get_logger().info('learning_rate: {} final_learning_rate: {} epochs: {} batch_size: {}'.format(
        learning_rate, final_learning_rate, epochs, batch_size))

    history = ModelTrainer(model).fit(
        x_train, y_train,
        epochs=epochs,
        batch_size=batch_size,
        callbacks=[lr_logger, model_best_check, model_best_check_weights],
        validation_data=(x_test, y_test), shuffle=True,
        verbose=2)

    metrics = [len(history.history['loss']), history.history['loss'][-1], history.history['val_loss'][-1]]
    get_logger().info('Epoch {0}/{0} - loss: {1} - val_loss: {2}'.format(*metrics))

    # Save Model
    save_model(model, name=model_name)


def build_and_analyze_hls(
        model_name, x_train, x_test, original_x_test, original_y_test, y_mask):
    
    kmodel = _load_static_model(model_name) # Must be hls4ml compatible
    hls_model = build_and_test_hls_model(kmodel, x_train, x_test)

    predictions = hls_model.predict(np.ascontiguousarray(original_x_test))
    analyze_performance_from_predictions(predictions, original_x_test, original_y_test, y_mask)


def test_model_rate(
        model_name, x_test, original_y_test):

    # Must use a specific min-bias dataset
    evt_nums = original_y_test[:,7]
    modes = original_y_test[:,8]
    bxs = original_y_test[:,9]

    # Run all tracks, then zero out invalid modes and bxs
    loaded_model = _load_static_model(model_name)
    predictions = loaded_model.predict(x_test, batch_size=16384)
    valid_tracks_all = (bxs == 0) & ((modes == 11) | (modes == 13) | (modes == 14) | (modes == 15))

    for mode in [14,15,'highqual']:
        if mode == 'highqual':
            valid_tracks = valid_tracks_all
        else:
            valid_tracks = valid_tracks_all & (modes == mode)

        predictions_clean = np.where(valid_tracks.reshape((-1,1)), np.abs(predictions), 0)
        data = (predictions_clean[:,0], predictions_clean[:,1], evt_nums)
        rate_analyzer = DxyRateAnalyzer(True)
        rate_analyzer.process(data, mode=str(mode))


def analyze_performance(model_name, x_test, y_test, y_mask):
    # Load model and predict outputs
    loaded_model = _load_static_model(model_name)

    # Make predictions and unscale
    predictions = loaded_model.predict(x_test, batch_size=4096)

    analyze_performance_from_predictions(predictions, x_test, y_test, y_mask)


def analyze_performance_from_predictions(predictions, x_test, y_test, y_mask):
    # Unpack parameters
    param_id = 0

    if y_mask[0]:
        pt_pred = predictions[:, param_id]
        param_id += 1
    else:
        pt_pred = np.zeros((y_test.shape[0], 1))

    if y_mask[1]:
        dxy_pred = predictions[:, param_id]
        param_id += 1
    else:
        dxy_pred = np.zeros((y_test.shape[0], 1))

    # Analyze
    pt_test = y_test[:, 0]
    dxy_test = y_test[:, 1]
    eta_star_test = y_test[:, 2]
    dz_test = y_test[:, 6]

    test_param = np.column_stack((pt_test, dxy_test, eta_star_test, dz_test))
    pred_param = np.column_stack((pt_pred, dxy_pred))

    analyzers = list()

    if y_mask[0]:
        analyzers.append(PtTrainingAnalyzer())

    if y_mask[1]:
        analyzers.append(UnsignedDxyTrainingAnalyzer())

    for analyzer in analyzers:
        analyzer.process((test_param, pred_param))

    for analyzer in analyzers:
        analyzer.write()

    # Dump predictions
    dump_vars_params(x_test[:1000, :], pred_param[:1000, :], file_path='vld_prediction_dump.tsv')


# Utils
def _load_static_model(model_name):
    loaded_model = load_model(name=model_name, custom_objects={
        'Adamu': Adamu,
        'WarmupCosineDecay': WarmupCosineDecay,
        'BatchNormalization': BatchNormalization,
        'pt_loss': invpt_loss,
        'dxy_loss': dxy_loss,
    })

    loaded_model.trainable = False

    assert not loaded_model.updates

    print(loaded_model.summary())

    return loaded_model
