import math

import numpy as np

from emtfnn.commons.tools import safe_divide
from emtfnn.execution.runtime import get_logger


class TrainingDataset(object):

    def __init__(self, pos_input_file, neg_input_file, bkg_input_file):
        self.pos_input_file = pos_input_file
        self.neg_input_file = neg_input_file
        self.bkg_input_file = bkg_input_file

    def get_samples(self):
        pos_x_train, pos_y_train, \
        pos_x_test, pos_y_test = self._load_data(self.pos_input_file)
        neg_x_train, neg_y_train, \
        neg_x_test, neg_y_test = self._load_data(self.neg_input_file)
        bkg_x_train, bkg_y_train, \
        bkg_x_test, bkg_y_test = self._load_data(self.bkg_input_file)

        # Trim to same size and stack
        max_sig_length = int(math.floor(min(pos_x_train.shape[0], neg_x_train.shape[0]) * 2) / 2)
        max_bkg_length = int(math.floor(bkg_x_train.shape[0] * 2) / 2)

        pos_x_train = pos_x_train[:max_sig_length]
        pos_y_train = pos_y_train[:max_sig_length]
        neg_x_train = neg_x_train[:max_sig_length]
        neg_y_train = neg_y_train[:max_sig_length]
        bkg_x_train = bkg_x_train[:max_bkg_length]

        sig_x_train = np.concatenate((neg_x_train, pos_x_train))
        sig_y_train = np.concatenate((neg_y_train, pos_y_train))
        sig_x_test = np.concatenate((neg_x_test, pos_x_test))
        sig_y_test = np.concatenate((neg_y_test, pos_y_test))

        get_logger().info('Trimmed and merged signal variables to shape {0}'.format(sig_x_train.shape))
        get_logger().info('Trimmed and merged signal parameters to shape {0}'.format(sig_y_train.shape))
        get_logger().info('Trimmed and merged background variables to shape {0}'.format(bkg_x_train.shape))

        # Format variables
        sig_x_train = sig_x_train.astype(np.float32)
        sig_x_test = sig_x_test.astype(np.float32)
        bkg_x_train = bkg_x_train.astype(np.float32)

        # Return
        get_logger().info(
            'Loaded # of training and testing events: {0}'.format((sig_x_train.shape[0], sig_x_test.shape[0]))
        )

        return sig_x_train, sig_y_train, \
               sig_x_test, sig_y_test, \
               bkg_x_train

    def _load_data(self, input_file):
        get_logger().info('Loading data from {0} ...'.format(input_file))

        loaded_file = np.load(input_file)
        x_train = loaded_file['variables_train']
        y_train = loaded_file['parameters_train']
        x_test = loaded_file['variables_test']
        y_test = loaded_file['parameters_test']

        def _refine(x_train, y_train):
            ##########################################
            # Select Parameters
            ##########################################
            qpt = y_train[:, 0]
            dxy = y_train[:, 1]
            eta_star = y_train[:, 2]
            phi = y_train[:, 3]
            vx = y_train[:, 4]
            vy = y_train[:, 5]
            vz = y_train[:, 6]

            ##########################################
            # Pack Parameters
            ##########################################
            y_train = np.column_stack((safe_divide(1, qpt), dxy, eta_star, phi, vx, vy, vz))

            print('First variables')
            print(x_train[0, :])

            print('First parameters')
            print(y_train[0, :])

            # Return
            return x_train, y_train

        x_train, y_train = _refine(x_train, y_train)
        x_test, y_test = _refine(x_test, y_test)

        get_logger().info('Loaded the train variables with shape {0}'.format(x_train.shape))
        get_logger().info('Loaded the train parameters with shape {0}'.format(y_train.shape))
        get_logger().info('Loaded the test variables with shape {0}'.format(x_test.shape))
        get_logger().info('Loaded the test parameters with shape {0}'.format(y_test.shape))

        return x_train, y_train, x_test, y_test
