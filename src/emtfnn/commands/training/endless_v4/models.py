import numpy as np
import tensorflow as tf
from tensorflow.python.keras.losses import LogCosh

from emtfnn.commands.training.endless_v4.custom.activity_regularization import ActivityRegularization
from emtfnn.commands.training.endless_v4.custom.feature_normalization import FeatureNormalization
from emtfnn.commands.training.endless_v4.custom.mutated_batch_normalization import MutatedBatchNormalization
from emtfnn.commands.training.endless_v4.custom.mutated_dense import MutatedDense
from emtfnn.commands.training.endless_v4.custom.scale_activation import ScaleActivation
from emtfnn.commands.training.endless_v4.custom.tanh_activation import TanhActivation
from emtfnn.commons.tools import safe_divide
from emtfnn.keras.optimizers import Adamu
from emtfnn.keras.optimizers import WarmupCosineDecay


###########################################################################
# Layers
###########################################################################

def create_preprocessing_layer(x_train, axis=-1):
    # Find mean and variance for each channel
    reduction_axes = np.arange(x_train.ndim)
    reduction_axes = tuple(d for d in reduction_axes if d != reduction_axes[axis])

    # Randomize sign so that mean is zero
    sign_randomization = ((np.random.random_sample(x_train.shape) < 0.5) * 2.) - 1.
    x_adapt = x_train * sign_randomization

    # First pass: find mean & var without NaN
    _, var = (np.nanmean(x_adapt, axis=reduction_axes), np.nanvar(x_adapt, axis=reduction_axes))

    # Second pass: normalize, apply nonlinearity, find mean & var again
    normalize = lambda x: x * safe_divide(1., np.sqrt(var))  # ignore mean
    nonlinearity = lambda x: np.tanh(x * np.arctanh(3. / 6.) / 3.) * 6.  # linear up to +/-3, saturate at +/-6
    x_adapt = nonlinearity(normalize(x_adapt))
    _, var1 = (np.nanmean(x_adapt, axis=reduction_axes), np.nanvar(x_adapt, axis=reduction_axes))

    # Fake-quantize the weights
    quantize_fn = lambda x: np.clip(np.round(x * 1024.), 0., 1023.) / 1024.  # assume 10 fractional bits
    weights = [safe_divide(1., np.sqrt(var * var1)), (var1 * 0.)]  # kernel=1/sqrt(var), bias=0
    weights = list(map(quantize_fn, weights))

    # Create the layer
    preprocessing_layer = FeatureNormalization(axis=-1, name='preprocessing')
    preprocessing_layer.build(x_train.shape)
    preprocessing_layer.set_weights(weights)  # non-trainable weights

    return preprocessing_layer


def create_regularization_layer(noises, l1=1e-5, bias=14.0, batch_size=32, mask_value=999999):
    # Cast noises to float
    def _ismasked(t):
        return (t == mask_value)

    noises_mask = _ismasked(noises)
    noises = noises.astype(np.float32)
    noises[noises_mask] = np.nan

    # Convert to tensor
    noises = tf.convert_to_tensor(noises, name='noises')

    # Create the layer
    regularization_layer = ActivityRegularization(l1=l1, bias=bias, name='regularization')
    regularization_layer.set_data(noises, batch_size=batch_size)

    return regularization_layer


###########################################################################
# Optimizers
###########################################################################

def create_lr_schedule(num_train_samples,
                       epochs=100,
                       warmup_epochs=30,
                       batch_size=32,
                       learning_rate=0.001,
                       final_learning_rate=0.00001):
    # Create learning rate schedule with warmup and cosine decay
    steps_per_epoch = int(np.ceil(num_train_samples / float(batch_size)))
    warmup_steps = steps_per_epoch * warmup_epochs
    total_steps = steps_per_epoch * epochs
    cosine_decay_alpha = final_learning_rate / learning_rate

    assert warmup_steps <= total_steps

    return WarmupCosineDecay(
        initial_learning_rate=learning_rate, warmup_steps=warmup_steps,
        decay_steps=(total_steps - warmup_steps), alpha=cosine_decay_alpha)


def create_optimizer(lr_schedule,
                     gradient_clipnorm=10000):
    # Create optimizer with lr_schedule and clipnorm
    optimizer = Adamu(learning_rate=lr_schedule, clipnorm=gradient_clipnorm)

    return optimizer


###########################################################################
# Models
###########################################################################

def create_model(preprocessing_layer,
                 regularization_layer,
                 optimizer,
                 nodes0=28,
                 nodes1=24,
                 nodes2=16,
                 nodes_in=40,
                 nodes_out=1,
                 name='nnet_model',
                 target_parameter=None):
    # Sequential
    model = tf.keras.Sequential(name=name)
    regularization_layer.set_model(model)

    # Input layer
    model.add(tf.keras.layers.InputLayer(input_shape=(nodes_in,), name='inputs'))

    # Preprocessing
    model.add(preprocessing_layer)

    # Hidden layer 0
    model.add(MutatedDense(nodes0, kernel_initializer='glorot_uniform', use_bias=False, activation=None, name='dense'))
    model.add(MutatedBatchNormalization(momentum=0.99, epsilon=1e-4, name='batch_normalization'))
    model.add(TanhActivation(name='activation'))

    # Hidden layer 1
    model.add(
        MutatedDense(nodes1, kernel_initializer='glorot_uniform', use_bias=False, activation=None, name='dense_1'))
    model.add(MutatedBatchNormalization(momentum=0.99, epsilon=1e-4, name='batch_normalization_1'))
    model.add(TanhActivation(name='activation_1'))

    # Hidden layer 2
    model.add(
        MutatedDense(nodes2, kernel_initializer='glorot_uniform', use_bias=False, activation=None, name='dense_2'))
    model.add(MutatedBatchNormalization(momentum=0.99, epsilon=1e-4, name='batch_normalization_2'))
    model.add(TanhActivation(name='activation_2'))

    # Scale Factor
    if target_parameter is None:
        model.add(ScaleActivation(scale=1. / 64, name='rescaling'))
    elif target_parameter == 'qinvpt':
        model.add(ScaleActivation(scale=1. / 64, name='rescaling'))
    elif target_parameter == 'd0':
        pass
    else:
        raise ValueError('Invalid Target Parameter')

    # Last Dense Layer
    model.add(MutatedDense(nodes_out, kernel_initializer='glorot_uniform', use_bias=False, activation=None,
                           name='dense_final'))

    # Regularization Layer
    if target_parameter is None:
        model.add(regularization_layer)
    elif target_parameter == 'qinvpt':
        model.add(regularization_layer)
    elif target_parameter == 'd0':
        pass
    else:
        raise ValueError('Invalid Target Parameter')

    # Loss function & optimizer
    logcosh_loss = LogCosh()

    if target_parameter is None:
        loss_weight = 1
        model.add(regularization_layer)
    elif target_parameter == 'qinvpt':
        loss_weight = 100
        model.add(regularization_layer)
    elif target_parameter == 'd0':
        loss_weight = 1
    else:
        raise ValueError('Invalid Target Parameter')

    model.compile(optimizer=optimizer, loss=logcosh_loss, loss_weights=loss_weight)
    model.summary()

    return model
