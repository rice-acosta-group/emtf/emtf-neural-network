learning_rate = 0.005

final_learning_rate = learning_rate * 0.02

gradient_clipnorm = 10.

warmup_epochs = 30

epochs = 300

batch_size = 2048

reg_pt_scale = 1.

reg_dxy_scale = 1.
