import numpy as np
from keras.callbacks import ModelCheckpoint
from keras.layers import BatchNormalization
from qkeras import QBatchNormalization, QDense, quantized_bits, QActivation

from emtfnn.commands.training.endless_v4.constants import epochs, warmup_epochs, \
    final_learning_rate, batch_size, learning_rate, gradient_clipnorm
from emtfnn.commands.training.endless_v4.custom.activity_regularization import ActivityRegularization
from emtfnn.commands.training.endless_v4.custom.feature_normalization import FeatureNormalization
from emtfnn.commands.training.endless_v4.custom.mutated_batch_normalization import MutatedBatchNormalization
from emtfnn.commands.training.endless_v4.custom.mutated_dense import MutatedDense
from emtfnn.commands.training.endless_v4.custom.scale_activation import ScaleActivation
from emtfnn.commands.training.endless_v4.custom.tanh_activation import TanhActivation
from emtfnn.commands.training.endless_v4.datasets import TrainingDataset
from emtfnn.commands.training.endless_v4.models import create_model, \
    create_lr_schedule, create_optimizer, create_preprocessing_layer, create_regularization_layer
from emtfnn.commons.analyzers.dxy_training import DxyTrainingAnalyzer
from emtfnn.commons.analyzers.qpt_training import QPtTrainingAnalyzer
from emtfnn.commons.plotting import plot_variables
from emtfnn.commons.tools import safe_divide
from emtfnn.commons.trainers import ModelTrainer
from emtfnn.commons.workbench import dump_vars_params
from emtfnn.commons.workbench import load_model
from emtfnn.commons.workbench import save_model
from emtfnn.execution.runtime import get_logger
from emtfnn.keras.loggers import LearningRateLogger


def configure_parser(parser):
    parser.add_argument(dest='model_name', metavar='MODEL_NAME', type=str, help='Model Name')
    parser.add_argument(dest='pos_file', metavar='POS_FILE', type=str, help='Positive Endcap File')
    parser.add_argument(dest='neg_file', metavar='NEG_FILE', type=str, help='Negative Endcap File')
    parser.add_argument(dest='bkg_file', metavar='BKG_FILE', type=str, help='Background File')

    parser.add_argument('--target', dest='target', metavar='TARGET',
                        type=str, default=None,
                        help='target parameter')

    parser.add_argument('--train', dest='train_en', action='store_const',
                        const=True, default=False,
                        help='Train')


def run(args):
    # Unpack Args
    model_name = args.model_name
    pos_file = args.pos_file
    neg_file = args.neg_file
    bkg_file = args.bkg_file

    target = args.target
    train_en = args.train_en

    # Check inputs
    inputs_consistent = (pos_file is not None and neg_file is not None and bkg_file is not None)

    assert inputs_consistent, "Inputs are invalid. Check you\'re not missing an inputfile or that they are not empty"

    # Load Dataset
    dataset = TrainingDataset(pos_file, neg_file, bkg_file)

    x_train, y_train, \
    x_test, y_test, \
    x_bkg = dataset.get_samples()

    # Unpack parameters
    original_x_test = np.copy(x_test)
    original_y_test = np.copy(y_test)

    qinvpt_train, d0_train = y_train[:, 0], y_train[:, 1]
    qinvpt_test, d0_test = y_test[:, 0], y_test[:, 1]

    # Stack Parameters
    y_train = np.column_stack((qinvpt_train, d0_train))
    y_test = np.column_stack((qinvpt_test, d0_test))

    # Mask Parameters
    if target is None:
        y_mask = [True, True]
    elif target in ('qinvpt'):
        y_mask = [True, False]
    elif target in ('d0'):
        y_mask = [False, True]

    y_train = y_train[:, y_mask]
    y_test = y_test[:, y_mask]

    # Plot Variables
    plot_variables(x_train, 'sig_variables.png')
    plot_variables(x_bkg, 'bkg_variables.png')

    # Train
    if train_en:
        train_model(model_name,
                    x_train, y_train,
                    x_test, y_test,
                    x_bkg,
                    target=target)

    # Analyze Performance
    analyze_performance(model_name, original_x_test, original_y_test, y_mask, target=target)


def train_model(model_name,
                x_train, y_train,
                x_test, y_test,
                x_bkg,
                target='qinvpt'):
    preprocessing_layer = create_preprocessing_layer(x_train)

    regularization_layer = create_regularization_layer(x_bkg, batch_size=batch_size)

    lr_schedule = create_lr_schedule(
        x_train.shape[0],
        epochs=epochs,
        warmup_epochs=warmup_epochs,
        batch_size=batch_size,
        learning_rate=learning_rate,
        final_learning_rate=final_learning_rate)

    optimizer = create_optimizer(
        lr_schedule,
        gradient_clipnorm=gradient_clipnorm)

    model = create_model(
        preprocessing_layer,
        regularization_layer,
        optimizer,
        name=model_name,
        target_parameter=target)

    lr_logger = LearningRateLogger()
    model_best_check = ModelCheckpoint(filepath='model_bchk.h5', monitor='val_loss', verbose=1, save_best_only=True)
    model_best_check_weights = ModelCheckpoint(filepath='model_bchk_weights.h5', monitor='val_loss', verbose=1,
                                               save_best_only=True, save_weights_only=True)

    # Train Model
    get_logger().info('Training model')
    get_logger().info('learning_rate: {} final_learning_rate: {} epochs: {} batch_size: {}'.format(
        learning_rate, final_learning_rate, epochs, batch_size))

    history = ModelTrainer(model).fit(
        x_train, y_train,
        epochs=epochs,
        batch_size=batch_size,
        callbacks=[lr_logger, model_best_check, model_best_check_weights],
        validation_data=(x_test, y_test), shuffle=True,
        verbose=2)

    metrics = [len(history.history['loss']), history.history['loss'][-1], history.history['val_loss'][-1]]
    get_logger().info('Epoch {0}/{0} - loss: {1} - val_loss: {2}'.format(*metrics))

    # Save Model
    save_model(model, name=model_name)


def analyze_performance(model_name, x_test, y_test, y_mask, target='qinvpt'):
    # Load model and predict outputs
    loaded_model = load_model(
        name=model_name, custom_objects={
            'ActivityRegularization': ActivityRegularization,
            'FeatureNormalization': FeatureNormalization,
            'MutatedDense': MutatedDense,
            'MutatedBatchNormalization': MutatedBatchNormalization,
            'TanhActivation': TanhActivation,
            'ScaleActivation': ScaleActivation,
            'BatchNormalization': BatchNormalization,
            'QBatchNormalization': QBatchNormalization,
            'QActivation': QActivation,
            'QDense': QDense,
            'quantized_bits': quantized_bits,
        })
    loaded_model.trainable = False

    assert not loaded_model.updates

    print(loaded_model.summary())

    # Make predictions and unscale
    predictions = loaded_model.predict(x_test, batch_size=8192)

    # Unpack parameters
    param_id = 0

    if y_mask[0]:
        qinvpt_pred = predictions[:, param_id]
        pt_pred = safe_divide(1, qinvpt_pred + 1e-6)
        param_id += 1
    else:
        pt_pred = np.zeros((y_test.shape[0], 1))

    if y_mask[1]:
        d0_pred = predictions[:, param_id]
        param_id += 1
    else:
        d0_pred = np.zeros((y_test.shape[0], 1))

    # Analyze
    pt_test = safe_divide(1, y_test[:, 0])
    d0_test = y_test[:, 1]
    eta_star_test = y_test[:, 2]
    dz_test = y_test[:, 6]

    test_param = np.column_stack((pt_test, d0_test, eta_star_test, dz_test))
    pred_param = np.column_stack((pt_pred, d0_pred))

    if target == 'qinvpt':
        analyzer = QPtTrainingAnalyzer()
    elif target == 'd0':
        analyzer = DxyTrainingAnalyzer()
    else:
        raise ValueError('Invalid Target Parameter')

    analyzer.process((test_param, pred_param))
    analyzer.write()

    # Dump predictions
    dump_vars_params(x_test[:1000, :], pred_param[:1000, :], file_path='vld_prediction_dump.tsv')
