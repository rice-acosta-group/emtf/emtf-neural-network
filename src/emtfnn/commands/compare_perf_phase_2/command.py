import numpy as np
from matplotlib import cm as colors
from matplotlib import pyplot as plt

from emtfnn.commands.compare_perf_phase_2.comparison_data import *


def configure_parser(parser):
    pass


def run(args):
    # PT METRICS
    for output in pt_metrics.keys():

        fig, axs = plt.subplots(len(pt_metrics[output]), 1)
        n = 0

        for metric in pt_metrics[output]:

            arr_dataset_bin_id = np.arange(0, len(pt_datasets))
            arr_pt_bin_id = np.arange(0, len(pt_bins))

            arr_x = np.repeat(arr_dataset_bin_id, len(pt_bins))
            arr_y = np.tile(arr_pt_bin_id, len(pt_datasets))

            bin_x_edges = np.arange(-0.5, len(pt_datasets) + 0.5, step=1)
            bin_y_edges = np.arange(-0.5, len(pt_bins) + 0.5, step=1)

            arr_rmse_val_dataset = list()

            for label, values in pt_metrics[output][metric].items():
                arr_rmse_val_dataset += values

            minval = 0
            if "rmse" in metric:
                colorscheme = colors.Greens
                if "qinvpt" in output:
                    maxval = 0.1
                else:
                    maxval = 10000
            elif "median" in metric:
                colorscheme = colors.RdBu
                if "qinvpt" in output:
                    minval = -0.005
                    maxval = 0.005
                else:
                    minval = -1
                    maxval = 1
            else:
                colorscheme = colors.Greens
                if "qinvpt" in output:
                    maxval = 0.1

            hist, xbins, ybins, im = axs[n].hist2d(arr_x, arr_y,
                                                   bins=[bin_x_edges, bin_y_edges],
                                                   weights=arr_rmse_val_dataset,
                                                   cmap=colorscheme,
                                                   vmin=minval, vmax=maxval)

            plt.colorbar(im, ax=axs[n], label=pt_units[metric], cmap=colorscheme)

            axs[n].set_xticks(arr_dataset_bin_id, pt_datasets, rotation=-20)
            axs[n].set_yticks(arr_pt_bin_id, pt_bins, rotation=0)

            for i in range(len(ybins) - 1):
                for j in range(len(xbins) - 1):
                    num = '{:g}'.format(float('{:.5g}'.format(hist[j, i])))
                    axs[n].text(xbins[j] + 0.5, ybins[i] + 0.5, num, color="k", ha="center", va="center")

            n += 1
        fig.set_figheight(len(pt_metrics[output]) * 3)
        fig.set_figwidth(8)
        fig.tight_layout()
        fig.tight_layout()
        fig.tight_layout()
        plt.figtext(0.425, 0.982, f"{output} metrics")
        plt.savefig(output + "_metrics.png")

    # D0
    for output in d0_metrics.keys():

        fig, axs = plt.subplots(len(d0_metrics[output]), 1)
        n = 0

        for metric in d0_metrics[output]:

            arr_dataset_bin_id = np.arange(0, len(d0_datasets))
            arr_dxy_bin_id = np.arange(0, len(dxy_bins))

            arr_x = np.repeat(arr_dataset_bin_id, len(dxy_bins))
            arr_y = np.tile(arr_dxy_bin_id, len(d0_datasets))

            bin_x_edges = np.arange(-0.5, len(d0_datasets) + 0.5, step=1)
            bin_y_edges = np.arange(-0.5, len(dxy_bins) + 0.5, step=1)

            arr_metrics_val_dataset = list()

            for label, values in d0_metrics[output][metric].items():
                arr_metrics_val_dataset += values

            minval = 0
            if "rmse" in metric:
                colorscheme = colors.Greens
                maxval = 25
            elif "median" in metric:
                colorscheme = colors.RdBu
                minval = -1
                maxval = 1
            else:
                colorscheme = colors.Greens
                maxval = 10

            hist, xbins, ybins, im = axs[n].hist2d(arr_x, arr_y,
                                                   bins=[bin_x_edges, bin_y_edges],
                                                   weights=arr_metrics_val_dataset,
                                                   cmap=colorscheme,
                                                   vmin=minval, vmax=maxval)

            plt.colorbar(im, ax=axs[n], label=dxy_units[metric], cmap=colorscheme)

            axs[n].set_xticks(arr_dataset_bin_id, d0_datasets, rotation=-20)
            axs[n].set_yticks(arr_dxy_bin_id, dxy_bins, rotation=0)

            for i in range(len(ybins) - 1):
                for j in range(len(xbins) - 1):
                    num = '{:g}'.format(float('{:.5g}'.format(hist[j, i])))
                    axs[n].text(xbins[j] + 0.5, ybins[i] + 0.5, num, color="k", ha="center", va="center")

            n += 1

        fig.set_figheight(len(d0_metrics[output]) * 3 + 1)
        fig.set_figwidth(8)
        fig.tight_layout()
        fig.tight_layout()
        fig.tight_layout()
        plt.figtext(0.425, 0.982, "d0 metrics")
        plt.savefig(output + "_metrics.png")
