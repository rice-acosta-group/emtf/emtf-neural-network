import numpy as np
from keras.layers import BatchNormalization
from qkeras import QBatchNormalization, QActivation, QDense, quantized_bits

from emtfnn.commands.training.endless_v4.custom.activity_regularization import ActivityRegularization
from emtfnn.commands.training.endless_v4.custom.feature_normalization import FeatureNormalization
from emtfnn.commands.training.endless_v4.custom.mutated_batch_normalization import MutatedBatchNormalization
from emtfnn.commands.training.endless_v4.custom.mutated_dense import MutatedDense
from emtfnn.commands.training.endless_v4.custom.scale_activation import ScaleActivation
from emtfnn.commands.training.endless_v4.custom.tanh_activation import TanhActivation
from emtfnn.commons.analyzers.emulator import EmulatorAnalyzer
from emtfnn.commons.plotting import plot_variables
from emtfnn.commons.tools import safe_divide
from emtfnn.commons.workbench import dump_vars_params
from emtfnn.commons.workbench import load_data
from emtfnn.commons.workbench import load_model, load_protobuf_model


def configure_parser(parser):
    parser.add_argument(dest='model_name', metavar='MODEL_NAME', type=str, help='Model Name')
    parser.add_argument(dest='input_file', metavar='INPUT_FILE', type=str, help='Input File')

    parser.add_argument('--use-protobuf', dest='use_protobuf', action='store_const',
                        const=True, default=False,
                        help='Loads model from protobuf')


def run(args):
    model_name = args.model_name
    input_file = args.input_file
    use_protobuf = args.use_protobuf

    # Load Inputs and Outputs
    emulator_vars, emulator_param = load_data(input_file)

    emulator_vars = np.float32(emulator_vars)
    emulator_param = np.float32(emulator_param)

    plot_variables(emulator_vars)

    if use_protobuf:
        # Load model
        graph, session = load_protobuf_model(name=model_name)

        # Predict
        with session:
            input_tensor = graph.get_tensor_by_name('inputs:0')
            output_tensor = graph.get_tensor_by_name('Identity:0')
            workbench_param = session.run(output_tensor, feed_dict={input_tensor: emulator_vars})
    else:
        # Load model and custom objects
        loaded_model = load_model(name=model_name, custom_objects={
            'ActivityRegularization': ActivityRegularization,
            'FeatureNormalization': FeatureNormalization,
            'MutatedDense': MutatedDense,
            'MutatedBatchNormalization': MutatedBatchNormalization,
            'TanhActivation': TanhActivation,
            'ScaleActivation': ScaleActivation,
            'BatchNormalization': BatchNormalization,
            'QBatchNormalization': QBatchNormalization,
            'QActivation': QActivation,
            'QDense': QDense,
            'quantized_bits': quantized_bits,
        })
        loaded_model.trainable = False

        assert not loaded_model.updates

        # Make predictions
        workbench_param = loaded_model.predict(emulator_vars)

    # Format Predictions
    wb_invpt = workbench_param[:, 0]
    wb_pt = safe_divide(1, wb_invpt)
    # wb_dxy = workbench_param[:, 1]
    wb_dxy = np.zeros_like(wb_pt)

    workbench_param = np.column_stack((wb_pt, wb_dxy, wb_invpt))

    # Show Mismatch Entries
    mismatch_sel = []

    for i in range(emulator_vars.shape[0]):
        has_mismatch = False

        emu_pt = emulator_param[i, 2]
        wb_pt = workbench_param[i, 2]

        if wb_pt > 0:
            diff = abs(emu_pt / wb_pt - 1)

            if diff > 0.1:
                has_mismatch = True

        mismatch_sel.append(has_mismatch)

    mismatched_emulator_vars = emulator_vars[mismatch_sel, :]
    mismatched_emulator_param = emulator_param[mismatch_sel, :]
    mismatched_workbench_param = workbench_param[mismatch_sel, :]

    dump_vars_params(mismatched_emulator_vars,
                     np.column_stack((mismatched_emulator_param, mismatched_workbench_param)),
                     file_path='emu_mismatch.tsv')

    dump_vars_params(emulator_vars[:1000, :], workbench_param[:1000, :],
                     file_path='wb_predictions.tsv')

    # Analyze
    analyzer = EmulatorAnalyzer()
    analyzer.process((workbench_param, emulator_param))
    analyzer.write()
