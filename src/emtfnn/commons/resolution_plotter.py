import warnings
import matplotlib.pyplot as plt
import numpy as np #to plot the histogram and the Gaussian together, add below:
from scipy.optimize import curve_fit
from scipy.optimize import OptimizeWarning
import sys
from matplotlib.backends.backend_pdf import PdfPages
import os




def make_resolution_pdf(pT_predictions, pT_true, eta_true):

    # Use pT not qpT
    pT_predictions_abs = np.abs(pT_predictions)
    pT_true_abs = np.abs(pT_true)
    eta_true_abs = np.abs(eta_true)

    # Bin Each Resolution Plot
    eta_bins = [(1.25, 1.6), (1.6, 2.1), (2.1, 2.5)]
    pt_bins = [(0, 5), (4, 6), (5, 10), (10, 20), (20, 50), (50, 250)]

    # Initialize PdfPages object outside the loop
    pp = openPdfPages('resolution_plots', "Resolution_Plot", verbose=True)

    for pt_bin in pt_bins:
        for eta_bin in eta_bins:
            pt_mask = (pT_true >= pt_bin[0]) & (pT_true_abs < pt_bin[1])
            eta_mask = (eta_true_abs >= eta_bin[0]) & (eta_true_abs < eta_bin[1])   
            combined_mask = pt_mask & eta_mask

            # Generate invpt resolution plot
            title = f"1/pT Resolution Plot (pt: {pt_bin}, eta: {eta_bin})"
            textStr = f"Fit Data"
            res_plot_pt, means_pt_elem, std_pt_elem = makeResolutionPlot(pT_true_abs[combined_mask], pT_predictions_abs[combined_mask], title, textStr, "1/pT")
            pp.savefig(res_plot_pt)
            plt.close(res_plot_pt)

            # Generate pT resolution plot 
            title = f"pT Resolution Plot (pt: {pt_bin}, eta: {eta_bin})"
            textStr = f"Fit Data"
            res_plot_pt, means_pt_elem, std_pt_elem = makeResolutionPlot(pT_true_abs[combined_mask], pT_predictions_abs[combined_mask], title, textStr, "pT")
            pp.savefig(res_plot_pt)
            plt.close(res_plot_pt)
    
    pp.close()


def guassian(x, A, mu, sigma):
    return A * np.exp(-1.0 * (x - mu)**2 / (2 * sigma**2))


# Define Plotter here:
def makeResolutionPlot(unbinned_GEN_pt, unbinned_NN_pt, title, textStr, resType, verbose=False):
    """
       makeResolutionVsPtPlot creates a binned histogram plot of the pseudo-bend resolution (1-GEN/NN)
       and calls getResolutionHist. ResolutionHist will inform the scaling factor for the efficiency plots (2*sigma = scaling factor).

       INPUT:
             unbinned_GEN_pt - TYPE: numpy array-like
             unbinned_NN_pt - TYPE: numpy array-like
             title - TYPE: String (Plot Title)
             textStr - TYPE: String (Text Box info)

        OUTPUT:
             fig - TYPE: MatPlotLib PyPlot Figure containing resolution plot
    """
    if(verbose):
        print("\nInitializing Figures and Binning Pt Histograms")
    
    # Define resolution (unbinned)
    unbinned_NN_pt = np.squeeze(unbinned_NN_pt)

    res_unbinned = 1 - np.divide(unbinned_GEN_pt, unbinned_NN_pt) #inverse pT relationship proportional to bending res
    
    if(resType == "pT"):
        res_unbinned = 1 - np.divide(unbinned_NN_pt, unbinned_GEN_pt)
    
    # Initializing bins and binning histograms from unbinned entries
    res_binned, res_bins = np.histogram(res_unbinned, 127, (-10,10)) #res_unbinned, bins=50, range (min - max pT)
 
    popt = [0,10,-1] # Randomly chosen so it will be off axes if no good fit was made
    with warnings.catch_warnings():
        warnings.simplefilter("error", OptimizeWarning)
        try:
            # Fit a the distribution to a guassian
            popt, pcov = curve_fit(guassian, xdata=[res_bins[i]+(res_bins[i+1]-res_bins[i])/2 for i in range(0, len(res_bins)-1)], ydata=res_binned,  p0=[80000, 0, .18])
            #print(popt)
        except OptimizeWarning:
            print("Curve Fit Failed")
            popt = [0,10,-1]  # Randomly chosen so it will be off axes if no good fit was made

    x_fit = np.linspace(-10, 10, num=1000)
    y_fit = guassian(x_fit, popt[0], popt[1], popt[2])

    if(verbose):
        print("Generating Resolution Plot")
    fig2, ax = plt.subplots(1) #no subplots for resolution
    fig2.suptitle(title) #change to CMSSW_12_1_0_pre3 IIRC Res Sample
    ax.plot(x_fit, y_fit, label="Fit", linewidth=.25)

    # Plotting errors
    ax.errorbar([res_bins[i]+(res_bins[i+1]-res_bins[i])/2 for i in range(0, len(res_bins)-1)],
                    res_binned, xerr=[(res_bins[i+1] - res_bins[i])/2 for i in range(0, len(res_bins)-1)], #xerr = 10.24
                    linestyle="", marker=".", markersize=3, elinewidth = .5)

    # Setting labels and plot configs
    ax.set_ylabel("$N_{events}$") #y-axis = number of events
    ax.set_xlabel("$(1/p_T^{GEN} - 1/p_T^{NN})/(1/p_T^{GEN})$") #x-axis = binned resolution
    if(resType == "pT"):
        ax.set_xlabel("$(p_T^{GEN} - p_T^{NN})/(p_T^{GEN})$")
    ax.grid(color='lightgray', linestyle='--', linewidth=.25)
    
    # Adding a text box to bottom right
    props = dict(boxstyle='square', facecolor='white', alpha=1.0)
    ax.text(0.95, 0.95, textStr + "\n \n A=" + str(round(popt[0],4)) + "\n $\mu$=" + str(round(popt[1],4)) +"\n $\sigma$ = " + str(round(popt[2],5)), transform=ax.transAxes, fontsize=10, verticalalignment='top', horizontalalignment='right', bbox=props)
    ax.set_xlim([-12,12])
    # Setting all font sizes to be small (Less distracting)
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(8)


    if(verbose):
        print("Finished Creating Res Figures\n")
    # Returning the final figure with both plots drawn
    return fig2, popt[1], popt[2]


def openPdfPages(directory, file_name, verbose):
    file_name_mod = ""
    file_name_index = 0
    if(not os.path.isdir(directory)):
        os.mkdir(directory)
    while(os.path.isfile(directory + "/" + file_name + str(file_name_mod) + ".pdf")):
        file_name_mod = str(file_name_index)
        file_name_index += 1

    if(verbose):
        print("\nOpening PDF:" + directory + "/" + file_name + str(file_name_mod) + ".pdf" + "\n")

    pdf_pages = PdfPages(directory + "/" + file_name + str(file_name_mod) + ".pdf")
    return pdf_pages


