from ROOT import TCanvas
from ROOT import TLegend
from ROOT import gPad
from ROOT import kBlack
from ROOT import kFullDotLarge

from emtfnn.analysis.analyzers import AbstractAnalyzer
from emtfnn.analysis.plotters import *
from emtfnn.analysis.root import cm_tab20
from emtfnn.commons.parameters import *
from emtfnn.commons.tools import safe_divide
from emtfnn.execution.runtime import get_logger


class DxyTrainingAnalyzer(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        self.param_d0_err_plotters = dict()
        self.sqr_sum_count = dict()  # num of valid entries in each bin
        self.d0_err_sqr_sum = dict()  # sum of squared d0 errors in each bin
        self.d0_err_lists = dict()  # list of errors for each d0 val in each bin

        for dxy_range in dxy_ranges:
            dxy_low = dxy_range[0]
            dxy_up = dxy_range[1]

            dxy_up_str = ('inf' if dxy_up is None else dxy_up)

            d0_err_plot = self.checkout_plotter(
                Hist1DPlotter,
                'dxy_%s_to_%s' % (dxy_low, dxy_up_str),
                'd_{0} Error for d_{xy} in (%s , %s)' % (dxy_low, dxy_up_str),
                'Pred d_{0} - True d_{0} [cm]', 'a.u.',
                xbins=model_val_d0_err_bins,
                logy=True,
                density=True)

            self.param_d0_err_plotters[dxy_range] = d0_err_plot

            self.sqr_sum_count[dxy_range] = 0
            self.d0_err_sqr_sum[dxy_range] = 0
            self.d0_err_lists[dxy_range] = list()

        self.d0_pred_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'd0_pred',
            'Model Prediction d_{0}',
            'd_{0} [cm]', 'a.u.',
            xbins=model_val_d0_bins,
            logy=True,
            density=True)

        self.d0_true_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'd0_true',
            'Model Test d_{0}',
            'd_{0} [cm]', 'a.u.',
            xbins=model_val_d0_bins,
            logy=True,
            density=True)

        self.vld_d0_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_d0', 'Model Validation : d_{0} Correlation',
            'True d_{0} [cm]', 'Model d_{0} [cm]',
            xbins=model_val_d0_bins,
            ybins=model_val_d0_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.vld_d0_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_d0_err', 'Model Validation : d_{0} Error',
            'True d_{0} [cm]', 'Pred d_{0} - True d_{0}',
            xbins=model_val_d0_bins,
            ybins=model_val_d0_err_bins,
            min_val=model_val_mismatch_threshold,
            log_scale=True)

        # Define efficiencies
        self.eff_definitions = []

        dxy_thresholds = [
            8,
            16,
            32,
            48,
            64,
        ]

        eta_thresholds = [
            (1.24, 1.6),
            (1.6, 2.1),
            (2.1, 2.4),
            (1.24, 2.4),
        ]

        def denom_pred_factory(eta_low, eta_up, dxy_low=None):
            def pred(entry):
                base_cut = (1.24 <= abs(entry['true_eta_st2']) < 2.4) \
                           and abs(entry['true_dxy']) < 120 \
                           and abs(entry['true_dz']) < 120

                if dxy_low is not None:
                    base_cut = base_cut and abs(entry['true_dxy']) >= dxy_low

                return base_cut and (eta_low <= abs(entry['true_eta_st2']) < eta_up)

            return pred

        def num_pred_factory(dxy_threshold):
            def pred(entry):
                return abs(entry['dxy']) >= dxy_threshold

            return pred

        for eta_threshold in eta_thresholds:

            eta_low = eta_threshold[0]
            eta_up = eta_threshold[1]

            for dxy_threshold in dxy_thresholds:
                name = 'num_dxy_ge_%d' % dxy_threshold
                name += '_denom_eta_ge_%s_lt_%s' % (str(eta_low).replace('.', 'p'), str(eta_up).replace('.', 'p'))

                num_label = 'd_{xy} #geq %d cm' % dxy_threshold
                denom_label = '%0.2f #leq #left| #eta_{st2} #right| < %0.2f' % (eta_low, eta_up)

                self.eff_definitions.append({
                    'name': name,
                    'num_label': num_label,
                    'denom_label': denom_label,
                    'numerator': num_pred_factory(dxy_threshold),
                    'denominator': denom_pred_factory(eta_low, eta_up),
                })

            for dxy_threshold in dxy_thresholds:
                name = 'num_dxy_ge_%d' % dxy_threshold
                name += '_denom_dxy_ge_%d_eta_ge_%s_lt_%s' % (
                    dxy_threshold + 5,
                    str(eta_low).replace('.', 'p'),
                    str(eta_up).replace('.', 'p')
                )

                num_label = 'd_{xy} #geq %d cm' % dxy_threshold
                denom_label = '%0.2f #leq #left| #eta_{st2} #right| < %0.2f' % (eta_low, eta_up)

                self.eff_definitions.append({
                    'name': name,
                    'num_label': num_label,
                    'denom_label': denom_label,
                    'numerator': num_pred_factory(dxy_threshold),
                    'denominator': denom_pred_factory(eta_low, eta_up, dxy_low=dxy_threshold + 5),
                })

        # Init Eff plots
        self.eff_vs_pt_plots = dict()

        for eff_definition in self.eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'vld_eff_vs_pt_%s' % eff_definition['name'], '',
                'True p_{T} [GeV]', 'Efficiency',
                xbins=pt_eff_bins)

            eff_plot.write_en = False

            self.eff_vs_pt_plots[eff_definition['name']] = eff_plot

        self.eff_vs_dxy_plots = dict()

        for eff_definition in self.eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'vld_eff_vs_dxy_%s' % eff_definition['name'], '',
                'True d_{xy} [cm]', 'Efficiency',
                xbins=model_val_dxy_bins)

            eff_plot.write_en = False

            self.eff_vs_dxy_plots[eff_definition['name']] = eff_plot

        self.eff_vs_eta_star_plots = dict()

        for eff_definition in self.eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'vld_eff_vs_eta_st2_%s' % eff_definition['name'], '',
                'True #left| #eta_{st2} #right|', 'Efficiency',
                xbins=abs_eta_bins)

            eff_plot.write_en = False

            self.eff_vs_eta_star_plots[eff_definition['name']] = eff_plot

    def process(self, data):
        true_values = data[0]
        pred_values = data[1]

        n_entries = true_values.shape[0]
        n_entries_chk = pred_values.shape[0]

        assert n_entries_chk == n_entries, 'Entries don\'t match in length'

        # find true/false positive rates at given triggers
        triggers = dict()
        for pt_threshold in roc_pt_thresholds:
            for dxy_threshold in roc_dxy_thresholds:
                name = 'pt_%d_dxy_%d' % (pt_threshold, dxy_threshold)
                triggers[name] = {'trigger': (pt_threshold, dxy_threshold),
                                  'true_pass': 0,
                                  'true_fail': 0,
                                  'pred_pass': 0,
                                  'pred_fail': 0,
                                  'tp': 0,
                                  'fp': 0}

        for i in range(n_entries):
            true_d0 = true_values[i, 1]
            pred_d0 = pred_values[i, 1]
            pred_pt = abs(pred_values[i, 0])

            true_dxy = abs(true_d0)
            pred_dxy = abs(pred_d0)

            err_dxy = pred_dxy - true_dxy
            err_d0 = pred_d0 - true_d0

            true_pt = abs(true_values[i, 0])
            true_eta_star = abs(true_values[i, 2])
            true_dz = abs(true_values[i, 3])

            self.d0_true_plotter.fill(true_d0)
            self.d0_pred_plotter.fill(pred_d0)

            event = {
                'dxy': pred_dxy,
                'true_pt': true_pt,
                'true_dxy': true_dxy,
                'true_eta_st2': true_eta_star,
                'true_dz': true_dz,
            }

            # ROC plot: test each trigger on each track
            for name in triggers.keys():
                true_pass = triggers[name]['trigger'][0] < true_pt and triggers[name]['trigger'][1] < true_dxy
                pred_pass = triggers[name]['trigger'][0] < pred_pt and triggers[name]['trigger'][1] < pred_dxy
                trigger = triggers[name]
                if true_pass:
                    trigger['true_pass'] += 1
                    if pred_pass:
                        trigger['pred_pass'] += 1
                        trigger['tp'] += 1

                else:
                    trigger['true_fail'] += 1
                    if pred_pass:
                        trigger['pred_pass'] += 1
                        trigger['fp'] += 1

            # dxy bins
            for dxy_range in dxy_ranges:
                dxy_low = dxy_range[0]
                dxy_up = dxy_range[1]

                if not (dxy_low <= true_dxy and (dxy_up is None or true_dxy < dxy_up)):
                    continue

                d0_err_plot = self.param_d0_err_plotters[dxy_range]
                d0_err_plot.fill(err_d0)

                self.sqr_sum_count[dxy_range] += 1
                self.d0_err_sqr_sum[dxy_range] += err_d0 ** 2
                self.d0_err_lists[dxy_range].append(err_d0)

            # dxy
            self.vld_d0_plotter.fill(true_d0, pred_d0)
            self.vld_d0_err_plotter.fill(true_d0, err_d0)

            # Efficiency
            for eff_definition in self.eff_definitions:
                passes_denominator = eff_definition['denominator'](event)

                if not passes_denominator:
                    continue

                passes_numerator = eff_definition['numerator'](event)

                # Eff vs pt
                eff_plot = self.eff_vs_pt_plots[eff_definition['name']]
                eff_plot.fill(passes_numerator, true_pt)

                # Eff vs dxy
                eff_plot = self.eff_vs_dxy_plots[eff_definition['name']]
                eff_plot.fill(passes_numerator, true_dxy)

                # Eff vs eta*
                eff_plot = self.eff_vs_eta_star_plots[eff_definition['name']]
                eff_plot.fill(passes_numerator, true_eta_star)

        tpr_vals = list()
        fpr_vals = list()

        roc_plot = self.checkout_plotter(
            ROCPlotter,
            'roc',
            'ROC Plot for joint pt and dxy triggers',
            'False Positive Rate ', 'True Positive Rate')

        for trigger in triggers.keys():
            tpr = triggers[trigger]['tp'] / triggers[trigger]['true_pass']
            fpr = triggers[trigger]['fp'] / triggers[trigger]['true_fail']
            tpr_vals.append(tpr)
            fpr_vals.append(fpr)
            roc_plot.fill(fpr, tpr)

        roc_plot.write()

    def post_production(self):
        #####################################################################
        # METRICS
        #####################################################################
        d0_median_residual = dict()
        d0_residual_mad = dict()
        d0_residual_std_dev = dict()
        d0_residual_gaussianness = dict()

        metrics = [
            ('d0_rmse', self.d0_err_sqr_sum),
            ('d0_median_residual', d0_median_residual),
            ('d0_residual_mad', d0_residual_mad),
            ('d0_residual_std_dev', d0_residual_std_dev),
            ('d0_residual_gaussianness', d0_residual_gaussianness)
        ]

        for dxy_range in dxy_ranges:
            d0_err_arr = np.asarray(self.d0_err_lists[dxy_range])

            d0_err_median = np.median(d0_err_arr)

            k = 1 / 0.6744897501960817  # scale factor
            # for if the data is normally distributed, sd/(mad*k) is close to 1

            d0_median_residual[dxy_range] = d0_err_median
            d0_residual_mad[dxy_range] = np.median(np.abs(d0_err_arr - d0_err_median))
            d0_residual_std_dev[dxy_range] = np.std(d0_err_arr)
            d0_residual_gaussianness[dxy_range] = d0_residual_std_dev[dxy_range] / (d0_residual_mad[dxy_range] * k)
        get_logger().info("Statistics calculated on true & predicted d0, sorted by dxy bin")
        get_logger().info('dxy_ranges: {0}'.format(dxy_ranges))

        for param, metric in metrics:
            error_list = list()
            for dxy_range in dxy_ranges:
                if 'rmse' in param:
                    bin_n = self.sqr_sum_count[dxy_range]
                    bin_sum = metric[dxy_range]
                    rmse = math.sqrt(safe_divide(bin_sum, bin_n))
                    error_list.append(rmse)
                else:
                    error_list.append(metric[dxy_range])
            get_logger().info('{0}: {1}'.format(param, error_list))

        #####################################################################
        # Efficiency Plots
        #####################################################################
        self.plot_efficiencies(
            '',
            lambda eff_def: 'denom_eta_ge_1p24_lt_2p4' in eff_def['name']
                            and 'num_dxy_ge_8' not in eff_def['name'],
            lambda eff_def: eff_def['num_label'],
            dxy_plot_en=True,
            pt_plot_en=False,
            eta_star_plot_en=False)

        self.plot_efficiencies(
            '',
            lambda eff_def: 'denom_dxy_ge_' in eff_def['name']
                            and '_eta_ge_1p24_lt_2p4' in eff_def['name']
                            and 'num_dxy_ge_8' not in eff_def['name'],
            lambda eff_def: eff_def['num_label'],
            dxy_plot_en=False,
            pt_plot_en=True,
            eta_star_plot_en=True)

        self.plot_efficiencies(
            '_dxy_ge_16cm',
            lambda eff_def: 'denom_dxy_ge_' in eff_def['name']
                            and '_eta_ge_1p24_lt_2p4' not in eff_def['name']
                            and 'num_dxy_ge_16' in eff_def['name'],
            lambda eff_def: eff_def['denom_label'],
            'Eff: d_{xy} #geq 16 cm',
            dxy_plot_en=False,
            pt_plot_en=True,
            eta_star_plot_en=False)

        self.plot_efficiencies(
            '_dxy_ge_32cm',
            lambda eff_def: 'denom_dxy_ge_' in eff_def['name']
                            and '_eta_ge_1p24_lt_2p4' not in eff_def['name']
                            and 'num_dxy_ge_32' in eff_def['name'],
            lambda eff_def: eff_def['denom_label'],
            'Eff: d_{xy} #geq 32 cm',
            dxy_plot_en=False,
            pt_plot_en=True,
            eta_star_plot_en=False)

    def plot_efficiencies(self, suffix, group_pred, label_fn,
                          comment=None,
                          pt_plot_en=True,
                          dxy_plot_en=True,
                          eta_star_plot_en=True):
        #####################################################################
        # Selection
        #####################################################################
        selected_eff = []

        for eff_definition in self.eff_definitions:
            if not group_pred(eff_definition):
                continue

            selected_eff.append(eff_definition)

        #####################################################################
        # Efficiency vs pT
        #####################################################################
        if pt_plot_en:
            plot_canvas = TCanvas('vld_eff_vs_pt_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_pt_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(pt_eff_bins[0], 0, pt_eff_bins[-1], 1.15, 'Model Validation')
            frame.GetXaxis().SetTitle('True p_{T} [GeV]')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.550
            legend_y0 = 0.225

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.375, legend_y0 + 0.225)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.1)
            legend.SetBorderSize(0)
            legend.SetTextSize(.035)

            cm = iter(cm_tab20)

            # Stack Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_pt_plots[eff_definition['name']].plot.GetPaintedGraph()

                eff_plot.SetMarkerColor(next(cm))
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(next(cm))
                eff_plot.SetLineWidth(2)

                eff_plot.Draw('P SAME')

                legend.AddEntry(eff_plot, label_fn(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('vld_dxy_eff_vs_pt' + suffix + '.png')

        #####################################################################
        # Eff vs dxy
        #####################################################################
        if dxy_plot_en:
            plot_canvas = TCanvas('vld_eff_vs_dxy_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_dxy_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(model_val_dxy_bins[0], 0, model_val_dxy_bins[-1], 1.15,
                                          'Model Validation')
            frame.GetXaxis().SetTitle('True d_{xy} [cm]')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.225
            legend_y0 = 0.225

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.375, legend_y0 + 0.225)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.1)
            legend.SetBorderSize(0)
            legend.SetTextSize(.035)

            cm = iter(cm_tab20)

            # Stack Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_dxy_plots[eff_definition['name']].plot.GetPaintedGraph()

                eff_plot.SetMarkerColor(next(cm))
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(next(cm))
                eff_plot.SetLineWidth(2)

                eff_plot.Draw('P SAME')

                legend.AddEntry(eff_plot, label_fn(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('vld_dxy_eff_vs_dxy' + suffix + '.png')

        #####################################################################
        # Eff vs eta
        #####################################################################
        if eta_star_plot_en:
            plot_canvas = TCanvas('vld_eff_vs_eta_st2_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_eta_star_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(abs_eta_bins[0], 0, abs_eta_bins[-1], 1.15, 'Model Validation')
            frame.GetXaxis().SetTitle('True #left| #eta_{st2} #right|')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.450
            legend_y0 = 0.225

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.375, legend_y0 + 0.225)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.1)
            legend.SetBorderSize(0)
            legend.SetTextSize(.035)

            cm = iter(cm_tab20)

            # Stack Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_eta_star_plots[eff_definition['name']].plot.GetPaintedGraph()

                eff_plot.SetMarkerColor(next(cm))
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(next(cm))
                eff_plot.SetLineWidth(2)

                eff_plot.Draw('P SAME')

                legend.AddEntry(eff_plot, label_fn(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('vld_dxy_eff_vs_eta_st2' + suffix + '.png')
