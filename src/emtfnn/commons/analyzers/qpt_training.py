from ROOT import TCanvas
from ROOT import TLegend
from ROOT import gPad
from ROOT import kBlack
from ROOT import kFullDotLarge

from emtfnn.analysis.analyzers import AbstractAnalyzer
from emtfnn.analysis.plotters import *
from emtfnn.analysis.root import cm_tab20
from emtfnn.commons.parameters import *
from emtfnn.commons.tools import safe_divide
from emtfnn.execution.runtime import get_logger


class QPtTrainingAnalyzer(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        self.param_pt_err_plotters = dict()
        self.param_pt_rerr_plotters = dict()
        self.param_invpt_err_plotters = dict()

        self.sqr_sum_count = dict()  # num of valid entries in each bin
        self.qpt_err_sqr_sum = dict()  # sum of squared qpt errors in each bin
        self.qinvpt_err_sqr_sum = dict()  # sum of squared qinvpt errors in each bin

        self.qpt_err_lists = dict()  # list of errors for each qpt val in each bin
        self.qinvpt_err_lists = dict()  # list of errors for each qinvpt val in each bin

        for pt_range in pt_ranges:
            pt_low = pt_range[0]
            pt_up = pt_range[1]

            pt_up_str = ('inf' if pt_up is None else pt_up)

            pt_err_plot = self.checkout_plotter(
                Hist1DPlotter,
                'qpt_%s_to_%s' % (pt_low, pt_up_str),
                'qp_{T} Error',
                'Pred-True [GeV]', 'a.u.',
                xbins=model_val_pt_err_bins,
                logy=True,
                density=True)

            pt_rerr_plot = self.checkout_plotter(
                Hist1DPlotter,
                'qpt_rel_%s_to_%s' % (pt_low, pt_up_str),
                'qp_{T} Relative Error',
                '{Pred-True \over True}', 'a.u.',
                xbins=model_val_pt_rerr_bins,
                logy=True,
                density=True)

            invpt_err_plot = self.checkout_plotter(
                Hist1DPlotter,
                'qinvpt_%s_to_%s' % (pt_low, pt_up_str),
                'q/p_{T} Error',
                'Pred-True [GeV^{-1}]', 'a.u.',
                xbins=model_val_invpt_err_bins,
                logy=True,
                density=True)

            self.param_pt_err_plotters[pt_range] = pt_err_plot
            self.param_pt_rerr_plotters[pt_range] = pt_rerr_plot
            self.param_invpt_err_plotters[pt_range] = invpt_err_plot

            self.sqr_sum_count[pt_range] = 0
            self.qpt_err_sqr_sum[pt_range] = 0
            self.qinvpt_err_sqr_sum[pt_range] = 0
            self.qpt_err_lists[pt_range] = list()
            self.qinvpt_err_lists[pt_range] = list()

        self.true_qpt_vs_dxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'qpt_true_qpt_vs_dxy_plotter', 'True Correlation',
            'True #left| d_{xy} #right| [cm]', 'True qp_{T} [GeV]',
            xbins=model_val_dxy_bins,
            ybins=model_val_qpt_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.qpt_pred_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'qpt_pred',
            'Model Prediction',
            'qp_{T} [GeV]', 'a.u.',
            xbins=model_val_qpt_bins,
            logy=True,
            density=True)

        self.qpt_true_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'qpt_true',
            'Model Test',
            'qp_{T} [GeV]', 'a.u.',
            xbins=model_val_qpt_bins,
            logy=True,
            density=True)

        self.qinvpt_pred_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'qinvpt_pred',
            'Model Prediction',
            'q/p_{T} [GeV^{-1}]', 'a.u.',
            xbins=model_val_qinvpt_bins,
            logy=True,
            density=True)

        self.qinvpt_true_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'qinvpt_true',
            'Model Test',
            'q/p_{T} [GeV^{-1}]', 'a.u.',
            xbins=model_val_qinvpt_bins,
            logy=True,
            density=True)

        self.vld_qpt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qpt', 'Model Validation',
            'True qp_{T} [GeV]', 'Model qp_{T} [GeV]',
            xbins=model_val_qpt_bins,
            ybins=model_val_qpt_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.vld_pt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pt', 'Model Validation',
            'True p_{T} [GeV]', 'Model p_{T} [GeV]',
            xbins=model_val_pt_bins,
            ybins=model_val_pt_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.vld_qpt_vs_dxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qpt_vs_dxy', 'Model Validation',
            'True #left| d_{xy} #right| [cm]', 'Model qp_{T} [GeV]',
            xbins=model_val_dxy_bins,
            ybins=model_val_qpt_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.vld_qpt_vs_eta_star_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qpt_vs_eta_star', 'Model Validation',
            'True #left| #eta^{*} #right|', 'Model qp_{T} [GeV]',
            xbins=abs_eta_bins,
            ybins=model_val_qpt_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.vld_qpt_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qpt_err', 'Model Validation',
            'True qp_{T} [GeV]', 'Pred-True',
            xbins=model_val_qpt_bins,
            ybins=model_val_qpt_err_bins,
            min_val=model_val_mismatch_threshold,
            log_scale=True)

        self.vld_qpt_zoom_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qpt_zoom', 'Model Validation',
            'True qp_{T} [GeV]', 'Model qp_{T} [GeV]',
            xbins=model_val_qpt_zoom_bins,
            ybins=model_val_qpt_zoom_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.vld_qpt_err_zoom_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qpt_err_zoom', 'Model Validation',
            'True qp_{T} [GeV]', 'Pred-True',
            xbins=model_val_qpt_zoom_bins,
            ybins=model_val_qpt_err_zoom_bins,
            min_val=model_val_mismatch_threshold,
            log_scale=True)

        self.vld_qpt_err_vs_dxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qpt_err_vs_dxy', 'Model Validation',
            'True #left| d_{xy} #right| [cm]', 'Pred-True (qp_{T}) [GeV]',
            xbins=model_val_dxy_bins,
            ybins=model_val_qpt_err_bins,
            min_val=model_val_mismatch_threshold,
            log_scale=True)

        self.vld_qpt_err_vs_eta_star_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qpt_err_vs_eta_star', 'Model Validation',
            'True #left| #eta^{*} #right|', 'Pred-True (qp_{T}) [GeV]',
            xbins=abs_eta_bins,
            ybins=model_val_qpt_err_bins,
            min_val=model_val_mismatch_threshold,
            log_scale=True)

        self.vld_qinvpt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qinvpt', 'Model Validation',
            'True 1/qp_{T} [GeV^{-1}]', 'Model 1/qp_{T} [GeV^{-1}]',
            xbins=model_val_qinvpt_bins,
            ybins=model_val_qinvpt_bins,
            min_val=model_val_min_threshold,
            floor_min=True,
            log_scale=True)

        self.vld_qinvpt_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qinvpt_err', 'Model Validation',
            'True 1/qp_{T} [GeV^{-1}]', 'Pred-True',
            xbins=model_val_qinvpt_bins,
            ybins=model_val_qinvpt_err_bins,
            min_val=model_val_mismatch_threshold,
            floor_min=True,
            log_scale=True)

        # Define efficiencies
        self.eff_definitions = []

        pt_thresholds = [
            3,
            5,
            7,
            10,
            15,
            22,
        ]

        eta_thresholds = [
            (1.24, 1.6),
            (1.6, 2.1),
            (2.1, 2.4),
            (1.24, 2.4),
        ]

        def denom_pred_factory(eta_low, eta_up, pt_low=None):
            def pred(entry):
                base_cut = (1.24 <= abs(entry['true_eta_star']) < 2.4) \
                           and abs(entry['true_dxy']) < 120 \
                           and abs(entry['true_dz']) < 120

                if pt_low is not None:
                    base_cut = base_cut and abs(entry['true_pt']) >= pt_low

                return base_cut and (eta_low <= abs(entry['true_eta_star']) < eta_up)

            return pred

        def num_pred_factory(pt_threshold):
            def pred(entry):
                return abs(entry['pt']) >= pt_threshold

            return pred

        for eta_threshold in eta_thresholds:

            eta_low = eta_threshold[0]
            eta_up = eta_threshold[1]

            for pt_threshold in pt_thresholds:
                name = 'num_pt_ge_%d' % pt_threshold
                name += '_denom_eta_ge_%s_lt_%s' % (str(eta_low).replace('.', 'p'), str(eta_up).replace('.', 'p'))

                num_label = 'p_{T} #geq %d GeV' % pt_threshold
                denom_label = '%0.2f #leq #left| #eta^{*} #right| < %0.2f' % (eta_low, eta_up)

                self.eff_definitions.append({
                    'name': name,
                    'num_label': num_label,
                    'denom_label': denom_label,
                    'numerator': num_pred_factory(pt_threshold),
                    'denominator': denom_pred_factory(eta_low, eta_up),
                })

            for pt_threshold in pt_thresholds:
                name = 'num_pt_ge_%d' % pt_threshold
                name += '_denom_pt_ge_%d_eta_ge_%s_lt_%s' % (
                    pt_threshold,
                    str(eta_low).replace('.', 'p'),
                    str(eta_up).replace('.', 'p')
                )

                num_label = 'p_{T} #geq %d GeV' % pt_threshold
                denom_label = '%0.2f #leq #left| #eta^{*} #right| < %0.2f' % (eta_low, eta_up)

                self.eff_definitions.append({
                    'name': name,
                    'num_label': num_label,
                    'denom_label': denom_label,
                    'numerator': num_pred_factory(pt_threshold),
                    'denominator': denom_pred_factory(eta_low, eta_up, pt_low=(pt_threshold + 5)),
                })

        # Init Eff plots
        self.eff_vs_pt_plots = dict()

        for eff_definition in self.eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'vld_eff_vs_pt_%s' % eff_definition['name'], '',
                'True p_{T} [GeV]', 'Efficiency',
                xbins=pt_eff_bins)

            eff_plot.write_en = False

            self.eff_vs_pt_plots[eff_definition['name']] = eff_plot

        self.eff_vs_dxy_plots = dict()

        for eff_definition in self.eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'vld_eff_vs_dxy_%s' % eff_definition['name'], '',
                'True #left| d_{xy} #right| [cm]', 'Efficiency',
                xbins=model_val_dxy_bins)

            eff_plot.write_en = False

            self.eff_vs_dxy_plots[eff_definition['name']] = eff_plot

        self.eff_vs_eta_star_plots = dict()

        for eff_definition in self.eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'vld_eff_vs_eta_star_%s' % eff_definition['name'], '',
                'True #left| #eta^{*} #right|', 'Efficiency',
                xbins=abs_eta_bins)

            eff_plot.write_en = False

            self.eff_vs_eta_star_plots[eff_definition['name']] = eff_plot

    def process(self, data):
        true_values = data[0]
        pred_values = data[1]

        n_entries = true_values.shape[0]
        n_entries_chk = pred_values.shape[0]

        assert n_entries_chk == n_entries, 'Entries don\'t match in length'

        for i in range(n_entries):
            # Unpack signed values
            true_qpt = true_values[i, 0]
            pred_qpt = pred_values[i, 0]

            true_pt = abs(true_qpt)
            pred_pt = abs(pred_qpt)

            # Signed qpt errors
            err_qpt = pred_qpt - true_qpt
            rel_err_qpt = err_qpt / true_qpt
            pred_qinvpt = safe_divide(1, pred_qpt)
            true_qinvpt = safe_divide(1, true_qpt)
            err_qinvpt = pred_qinvpt - true_qinvpt

            true_dxy = abs(true_values[i, 1])
            true_eta_star = abs(true_values[i, 2])
            true_dz = abs(true_values[i, 3])

            event = {
                'pt': pred_pt,
                'true_pt': true_pt,
                'true_dxy': true_dxy,
                'true_eta_star': true_eta_star,
                'true_dz': true_dz,
            }

            # Distributions
            self.qpt_pred_plotter.fill(pred_qpt)

            if pred_qpt != 0:
                self.qinvpt_pred_plotter.fill(pred_qinvpt)

            self.qpt_true_plotter.fill(true_qpt)

            if true_qpt != 0:
                self.qinvpt_true_plotter.fill(true_qinvpt)

            # pT Bins
            for pt_range in pt_ranges:
                pt_low = pt_range[0]
                pt_up = pt_range[1]

                if not (pt_low <= true_pt and (pt_up is None or true_pt < pt_up)):
                    continue

                pt_err_plot = self.param_pt_err_plotters[pt_range]
                pt_err_plot.fill(err_qpt)

                pt_rerr_plot = self.param_pt_rerr_plotters[pt_range]
                pt_rerr_plot.fill(rel_err_qpt)

                invpt_err_plot = self.param_invpt_err_plotters[pt_range]
                invpt_err_plot.fill(err_qinvpt)

                self.sqr_sum_count[pt_range] += 1
                self.qpt_err_sqr_sum[pt_range] += err_qpt ** 2
                self.qinvpt_err_sqr_sum[pt_range] += err_qinvpt ** 2
                self.qpt_err_lists[pt_range].append(err_qpt)
                self.qinvpt_err_lists[pt_range].append(err_qinvpt)

            # True
            self.true_qpt_vs_dxy_plotter.fill(true_dxy, true_qpt)

            # pT
            self.vld_pt_plotter.fill(true_pt, pred_pt)
            self.vld_qpt_plotter.fill(true_qpt, pred_qpt)
            self.vld_qpt_vs_dxy_plotter.fill(true_dxy, pred_qpt)
            self.vld_qpt_vs_eta_star_plotter.fill(true_eta_star, pred_qpt)
            self.vld_qpt_err_plotter.fill(true_qpt, err_qpt)
            self.vld_qpt_err_vs_dxy_plotter.fill(true_dxy, err_qpt)
            self.vld_qpt_err_vs_eta_star_plotter.fill(true_eta_star, err_qpt)

            self.vld_qpt_zoom_plotter.fill(true_qpt, pred_qpt)
            self.vld_qpt_err_zoom_plotter.fill(true_qpt, err_qpt)

            # Inv. pT
            if true_qpt != 0 and pred_qpt != 0:
                self.vld_qinvpt_plotter.fill(true_qinvpt, pred_qinvpt)
                self.vld_qinvpt_err_plotter.fill(true_qinvpt, err_qinvpt)

            # Efficiency
            for eff_definition in self.eff_definitions:
                passes_denominator = eff_definition['denominator'](event)

                if not passes_denominator:
                    continue

                passes_numerator = eff_definition['numerator'](event)

                # Eff vs pt
                eff_plot = self.eff_vs_pt_plots[eff_definition['name']]
                eff_plot.fill(passes_numerator, true_pt)

                # Eff vs dxy
                eff_plot = self.eff_vs_dxy_plots[eff_definition['name']]
                eff_plot.fill(passes_numerator, true_dxy)

                # Eff vs eta*
                eff_plot = self.eff_vs_eta_star_plots[eff_definition['name']]
                eff_plot.fill(passes_numerator, true_eta_star)

    def post_production(self):
        #####################################################################
        # METRICS
        #####################################################################
        qpt_residual_median = dict()
        qinvpt_residual_median = dict()
        qinvpt_residual_mad = dict()
        qinvpt_residual_std_dev = dict()
        qinvpt_residual_gaussianness = dict()

        metrics = [
            ('qpt_rmse', self.qpt_err_sqr_sum),
            ('qpt_median_residual', qpt_residual_median),
            ('qinvpt_rmse', self.qinvpt_err_sqr_sum),
            ('qinvpt_median_residual', qinvpt_residual_median),
            ('qinvpt_residual_mad', qinvpt_residual_mad),
            ('qinvpt_residual_std_dev', qinvpt_residual_std_dev),
            ('qinvpt_residual_gaussianness', qinvpt_residual_gaussianness)
        ]

        for pt_range in pt_ranges:
            qpt_err_arr = np.asarray(self.qpt_err_lists[pt_range])
            qinvpt_err_arr = np.asarray(self.qinvpt_err_lists[pt_range])

            k = 1 / 0.6744897501960817  # scale factor
            # for if the data is normally distributed, sd/(mad*k) is close to 1
            qpt_residual_median[pt_range] = np.median(qpt_err_arr)

            qinvpt_err_median = np.median(qinvpt_err_arr)
            qinvpt_residual_median[pt_range] = qinvpt_err_median

            qinvpt_residual_mad[pt_range] = np.median(np.abs(qinvpt_err_arr - qinvpt_err_median))
            qinvpt_residual_std_dev[pt_range] = np.std(qinvpt_err_arr)
            qinvpt_residual_gaussianness[pt_range] = qinvpt_residual_std_dev[pt_range] / (
                    qinvpt_residual_mad[pt_range] * k)

        get_logger().info('pt_ranges: {0}'.format(pt_ranges))
        for param, metric in metrics:
            error_list = list()

            for pt_range in pt_ranges:
                if 'rmse' in param:
                    bin_n = self.sqr_sum_count[pt_range]
                    bin_sum = metric[pt_range]
                    rmse = math.sqrt(safe_divide(bin_sum, bin_n))
                    error_list.append(rmse)
                else:
                    error_list.append(metric[pt_range])

            get_logger().info('{0}: {1}'.format(param, error_list))

        #####################################################################
        # Efficiency Plots
        #####################################################################
        self.plot_efficiencies(
            '',
            lambda eff_def: 'denom_eta_ge_1p24_lt_2p4' in eff_def['name']
                            and 'num_pt_ge_10' not in eff_def['name'],
            lambda eff_def: eff_def['num_label'],
            dxy_plot_en=False,
            eta_star_plot_en=False)

        self.plot_efficiencies(
            '',
            lambda eff_def: 'denom_pt_ge_' in eff_def['name']
                            and '_eta_ge_1p24_lt_2p4' in eff_def['name']
                            and 'num_pt_ge_10' not in eff_def['name'],
            lambda eff_def: eff_def['num_label'],
            pt_plot_en=False)

        self.plot_efficiencies(
            '_pt_ge_10GeV',
            lambda eff_def: 'denom_pt_ge_' in eff_def['name']
                            and '_eta_ge_1p24_lt_2p4' not in eff_def['name']
                            and 'num_pt_ge_10' in eff_def['name'],
            lambda eff_def: eff_def['denom_label'],
            'Eff: p_{T} #geq 10 GeV',
            pt_plot_en=False,
            eta_star_plot_en=False)

        self.plot_efficiencies(
            '_pt_ge_10GeV',
            lambda eff_def: 'denom_eta_' in eff_def['name']
                            and '_eta_ge_1p24_lt_2p4' not in eff_def['name']
                            and 'num_pt_ge_10' in eff_def['name'],
            lambda eff_def: eff_def['denom_label'],
            'Eff: p_{T} #geq 10 GeV',
            pt_plot_en=True,
            dxy_plot_en=False,
            eta_star_plot_en=False)

        self.plot_efficiencies(
            '_pt_ge_22GeV',
            lambda eff_def: 'denom_pt_ge_' in eff_def['name']
                            and '_eta_ge_1p24_lt_2p4' not in eff_def['name']
                            and 'num_pt_ge_22' in eff_def['name'],
            lambda eff_def: eff_def['denom_label'],
            'Eff: p_{T} #geq 22 GeV',
            pt_plot_en=False,
            eta_star_plot_en=False)

        self.plot_efficiencies(
            '_pt_ge_22GeV',
            lambda eff_def: 'denom_eta_' in eff_def['name']
                            and '_eta_ge_1p24_lt_2p4' not in eff_def['name']
                            and 'num_pt_ge_22' in eff_def['name'],
            lambda eff_def: eff_def['denom_label'],
            'Eff: p_{T} #geq 22 GeV',
            pt_plot_en=True,
            dxy_plot_en=False,
            eta_star_plot_en=False)

    def plot_efficiencies(self, suffix, group_pred, label_fn,
                          comment=None,
                          pt_plot_en=True,
                          dxy_plot_en=True,
                          eta_star_plot_en=True):
        #####################################################################
        # Selection
        #####################################################################
        selected_eff = []

        for eff_definition in self.eff_definitions:
            if not group_pred(eff_definition):
                continue

            selected_eff.append(eff_definition)

        #####################################################################
        # Efficiency vs pT
        #####################################################################
        if pt_plot_en:
            plot_canvas = TCanvas('vld_eff_vs_pt_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_pt_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(pt_eff_bins[0], 0, pt_eff_bins[-1], 1.15, 'Model Validation')
            frame.GetXaxis().SetTitle('True p_{T} [GeV]')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.550
            legend_y0 = 0.225

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.375, legend_y0 + 0.225)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.1)
            legend.SetBorderSize(0)
            legend.SetTextSize(.035)

            cm = iter(cm_tab20)

            # Stack Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_pt_plots[eff_definition['name']].plot.GetPaintedGraph()

                eff_plot.SetMarkerColor(next(cm))
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(next(cm))
                eff_plot.SetLineWidth(2)

                eff_plot.Draw('P SAME')

                legend.AddEntry(eff_plot, label_fn(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('vld_pt_eff_vs_pt' + suffix + '.png')

        #####################################################################
        # Eff vs dxy
        #####################################################################
        if dxy_plot_en:
            plot_canvas = TCanvas('vld_eff_vs_dxy_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_dxy_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(model_val_dxy_bins[0], 0, model_val_dxy_bins[-1], 1.15, 'Model Validation')
            frame.GetXaxis().SetTitle('True #left| d_{xy} #right| [cm]')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.225
            legend_y0 = 0.225

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.375, legend_y0 + 0.225)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.1)
            legend.SetBorderSize(0)
            legend.SetTextSize(.035)

            cm = iter(cm_tab20)

            # Stack Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_dxy_plots[eff_definition['name']].plot.GetPaintedGraph()

                eff_plot.SetMarkerColor(next(cm))
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(next(cm))
                eff_plot.SetLineWidth(2)

                eff_plot.Draw('P SAME')

                legend.AddEntry(eff_plot, label_fn(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('vld_pt_eff_vs_dxy' + suffix + '.png')

        #####################################################################
        # Eff vs eta
        #####################################################################
        if eta_star_plot_en:
            plot_canvas = TCanvas('vld_eff_vs_eta_star_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_eta_star_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(abs_eta_bins[0], 0, abs_eta_bins[-1], 1.15, 'Model Validation')
            frame.GetXaxis().SetTitle('True #left| #eta^{*} #right|')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.450
            legend_y0 = 0.225

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.375, legend_y0 + 0.225)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.1)
            legend.SetBorderSize(0)
            legend.SetTextSize(.035)

            cm = iter(cm_tab20)

            # Stack Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_eta_star_plots[eff_definition['name']].plot.GetPaintedGraph()

                eff_plot.SetMarkerColor(next(cm))
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(next(cm))
                eff_plot.SetLineWidth(2)

                eff_plot.Draw('P SAME')

                legend.AddEntry(eff_plot, label_fn(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('vld_pt_eff_vs_eta_star' + suffix + '.png')
