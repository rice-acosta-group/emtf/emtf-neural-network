from ROOT import TCanvas
from ROOT import TLegend
from ROOT import gPad
from ROOT import kBlack
from ROOT import kFullDotLarge

from emtfnn.analysis.analyzers import AbstractAnalyzer
from emtfnn.analysis.plotters import *
from emtfnn.analysis.root import cm_tab20
from emtfnn.commons.parameters import *
from emtfnn.commons.tools import safe_divide
from emtfnn.execution.runtime import get_logger


class PtTrainingAnalyzer(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        self.param_pt_err_plotters = dict()
        self.param_pt_rerr_plotters = dict()
        self.param_invpt_err_plotters = dict()
        self.param_dxy_err_plotters = dict()

        self.pt_abs_err = dict()
        self.sqr_sum_count = dict()
        self.pt_err_sqr_sum = dict()
        self.pt_rerr_sqr_sum = dict()
        self.invpt_err_sqr_sum = dict()
        self.dxy_err_sqr_sum = dict()

        for pt_range in pt_ranges:
            pt_low = pt_range[0]
            pt_up = pt_range[1]

            pt_up_str = ('inf' if pt_up is None else pt_up)

            pt_err_plot = self.checkout_plotter(
                Hist1DPlotter,
                'pt_%s_to_%s' % (pt_low, pt_up_str),
                'p_{T} Error',
                'Pred-True [GeV]', 'a.u.',
                xbins=model_val_qpt_err_bins,
                logy=True,
                density=True)

            pt_rerr_plot = self.checkout_plotter(
                Hist1DPlotter,
                'pt_rel_%s_to_%s' % (pt_low, pt_up_str),
                'p_{T} Relative Error',
                '{Pred-True \over True}', 'a.u.',
                xbins=model_val_pt_rerr_bins,
                logy=True,
                density=True)

            invpt_err_plot = self.checkout_plotter(
                Hist1DPlotter,
                'invpt_%s_to_%s' % (pt_low, pt_up_str),
                '1/p_{T} Error',
                'Pred-True [GeV^{-1}]', 'a.u.',
                xbins=model_val_invpt_err_bins,
                logy=True,
                density=True)

            dxy_err_plot = self.checkout_plotter(
                Hist1DPlotter,
                'dxy_%s_to_%s' % (pt_low, pt_up_str),
                '#left| d_{xy} #right| Error',
                'Pred-True [cm]', 'a.u.',
                xbins=model_val_dxy_err_bins,
                logy=True,
                density=True)

            self.param_pt_err_plotters[pt_range] = pt_err_plot
            self.param_pt_rerr_plotters[pt_range] = pt_rerr_plot
            self.param_invpt_err_plotters[pt_range] = invpt_err_plot
            self.param_dxy_err_plotters[pt_range] = dxy_err_plot

            self.pt_abs_err[pt_range] = list()
            self.sqr_sum_count[pt_range] = 0
            self.pt_err_sqr_sum[pt_range] = 0
            self.pt_rerr_sqr_sum[pt_range] = 0
            self.invpt_err_sqr_sum[pt_range] = 0
            self.dxy_err_sqr_sum[pt_range] = 0

        self.true_pt_vs_dxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_pt_vs_dxy_plotter', 'True Correlation',
            'True #left| d_{xy} #right| [cm]', 'True p_{T} [GeV]',
            xbins=model_val_dxy_bins,
            ybins=model_val_qpt_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.pt_pred_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'pt_pred',
            'Model Prediction',
            'p_{T} [GeV]', 'a.u.',
            xbins=model_val_qpt_bins,
            logy=True,
            density=True)

        self.pt_true_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'pt_true',
            'Model Test',
            'p_{T} [GeV]', 'a.u.',
            xbins=model_val_qpt_bins,
            logy=True,
            density=True)

        self.invpt_pred_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'invpt_pred',
            'Model Prediction',
            '1/p_{T} [GeV^{-1}]', 'a.u.',
            xbins=model_val_qinvpt_bins,
            logy=True,
            density=True)

        self.invpt_true_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'invpt_true',
            'Model Test',
            '1/p_{T} [GeV^{-1}]', 'a.u.',
            xbins=model_val_qinvpt_bins,
            logy=True,
            density=True)

        self.dxy_pred_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'dxy_pred',
            'Model Prediction',
            '#left| d_{xy} #right| [cm]', 'a.u.',
            xbins=model_val_dxy_bins,
            logy=True,
            density=True)

        self.dxy_true_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'dxy_true',
            'Model Test',
            '#left| d_{xy} #right| [cm]', 'a.u.',
            xbins=model_val_dxy_bins,
            logy=True,
            density=True)

        self.vld_qpt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qpt', 'Model Validation',
            'True q #cdot p_{T} [GeV]', 'Model q #cdot p_{T} [GeV]',
            xbins=model_val_qpt_bins,
            ybins=model_val_qpt_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.vld_pt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pt', 'Model Validation',
            'True p_{T} [GeV]', 'Model p_{T} [GeV]',
            xbins=model_val_qpt_bins,
            ybins=model_val_qpt_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.vld_pt_vs_dxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pt_vs_dxy', 'Model Validation',
            'True #left| d_{xy} #right| [cm]', 'Model p_{T} [GeV]',
            xbins=model_val_dxy_bins,
            ybins=model_val_qpt_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.vld_pt_vs_eta_star_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pt_vs_eta_star', 'Model Validation',
            'True #left| #eta^{*} #right|', 'Model p_{T} [GeV]',
            xbins=abs_eta_bins,
            ybins=model_val_qpt_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.vld_pt_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pt_err', 'Model Validation',
            'True p_{T} [GeV]', 'Pred-True',
            xbins=model_val_qpt_bins,
            ybins=model_val_qpt_err_bins,
            min_val=model_val_mismatch_threshold,
            log_scale=True)

        self.vld_pt_zoom_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pt_zoom', 'Model Validation',
            'True p_{T} [GeV]', 'Model p_{T} [GeV]',
            xbins=model_val_pt_zoom_bins,
            ybins=model_val_pt_zoom_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.vld_pt_err_zoom_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pt_err_zoom', 'Model Validation',
            'True p_{T} [GeV]', 'Pred-True',
            xbins=model_val_pt_zoom_bins,
            ybins=model_val_pt_err_zoom_bins,
            min_val=model_val_mismatch_threshold,
            log_scale=True)

        self.vld_pt_err_vs_dxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pt_err_vs_dxy', 'Model Validation',
            'True #left| d_{xy} #right| [cm]', 'Pred-True (p_{T}) [GeV]',
            xbins=model_val_dxy_bins,
            ybins=model_val_qpt_err_bins,
            min_val=model_val_mismatch_threshold,
            log_scale=True)

        self.vld_pt_err_vs_eta_star_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pt_err_vs_eta_star', 'Model Validation',
            'True #left| #eta^{*} #right|', 'Pred-True (p_{T}) [GeV]',
            xbins=abs_eta_bins,
            ybins=model_val_qpt_err_bins,
            min_val=model_val_mismatch_threshold,
            log_scale=True)

        self.vld_invpt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_invpt', 'Model Validation',
            'True 1/p_{T} [GeV^{-1}]', 'Model 1/p_{T} [GeV^{-1}]',
            xbins=model_val_qinvpt_bins,
            ybins=model_val_qinvpt_bins,
            min_val=model_val_min_threshold,
            floor_min=True,
            log_scale=True)

        self.vld_invpt_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_invpt_err', 'Model Validation',
            'True 1/p_{T} [GeV^{-1}]', 'Pred-True',
            xbins=model_val_qinvpt_bins,
            ybins=model_val_invpt_err_bins,
            min_val=model_val_mismatch_threshold,
            floor_min=True,
            log_scale=True)

        self.vld_dxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_dxy', 'Model Validation',
            'True #left| d_{xy} #right| [cm]', 'Model #left| d_{xy} #right| [cm]',
            xbins=model_val_dxy_bins,
            ybins=model_val_dxy_bins,
            min_val=model_val_min_threshold,
            log_scale=True)

        self.vld_dxy_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_dxy_err', 'Model Validation',
            'True #left| d_{xy} #right| [cm]', 'Pred-True',
            xbins=model_val_dxy_bins,
            ybins=model_val_dxy_err_bins,
            min_val=model_val_mismatch_threshold,
            log_scale=True)

        # Define efficiencies
        self.eff_definitions = []

        pt_thresholds = [
            3,
            5,
            7,
            10,
            15,
            22,
        ]

        eta_thresholds = [
            (1.24, 1.6),
            (1.6, 2.1),
            (2.1, 2.4),
            (1.24, 2.4),
        ]

        def denom_pred_factory(eta_low, eta_up, pt_low=None):
            def pred(entry):
                base_cut = (1.24 <= abs(entry['true_eta_star']) < 2.4) \
                           and abs(entry['true_dxy']) < 120 \
                           and abs(entry['true_dz']) < 120

                if pt_low is not None:
                    base_cut = base_cut and abs(entry['true_pt']) >= pt_low

                return base_cut and (eta_low <= abs(entry['true_eta_star']) < eta_up)

            return pred

        def num_pred_factory(pt_threshold):
            def pred(entry):
                return abs(entry['pt']) >= pt_threshold

            return pred

        for eta_threshold in eta_thresholds:

            eta_low = eta_threshold[0]
            eta_up = eta_threshold[1]

            for pt_threshold in pt_thresholds:
                name = 'num_pt_ge_%d' % pt_threshold
                name += '_denom_eta_ge_%s_lt_%s' % (str(eta_low).replace('.', 'p'), str(eta_up).replace('.', 'p'))

                num_label = 'p_{T} #geq %d GeV' % pt_threshold
                denom_label = '%0.2f #leq #left| #eta^{*} #right| < %0.2f' % (eta_low, eta_up)

                self.eff_definitions.append({
                    'name': name,
                    'num_label': num_label,
                    'denom_label': denom_label,
                    'numerator': num_pred_factory(pt_threshold),
                    'denominator': denom_pred_factory(eta_low, eta_up),
                })

            for pt_threshold in pt_thresholds:
                name = 'num_pt_ge_%d' % pt_threshold
                name += '_denom_pt_ge_%d_eta_ge_%s_lt_%s' % (
                    pt_threshold,
                    str(eta_low).replace('.', 'p'),
                    str(eta_up).replace('.', 'p')
                )

                num_label = 'p_{T} #geq %d GeV' % pt_threshold
                denom_label = '%0.2f #leq #left| #eta^{*} #right| < %0.2f' % (eta_low, eta_up)

                self.eff_definitions.append({
                    'name': name,
                    'num_label': num_label,
                    'denom_label': denom_label,
                    'numerator': num_pred_factory(pt_threshold),
                    'denominator': denom_pred_factory(eta_low, eta_up, pt_low=(pt_threshold + 5)),
                })

        # Init Eff plots
        self.eff_vs_pt_plots = dict()

        for eff_definition in self.eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'vld_eff_vs_pt_%s' % eff_definition['name'], '',
                'True p_{T} [GeV]', 'Efficiency',
                xbins=pt_eff_bins)

            eff_plot.write_en = False

            self.eff_vs_pt_plots[eff_definition['name']] = eff_plot

        self.eff_vs_dxy_plots = dict()

        for eff_definition in self.eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'vld_eff_vs_dxy_%s' % eff_definition['name'], '',
                'True #left| d_{xy} #right| [cm]', 'Efficiency',
                xbins=model_val_dxy_bins)

            eff_plot.write_en = False

            self.eff_vs_dxy_plots[eff_definition['name']] = eff_plot

        self.eff_vs_eta_star_plots = dict()

        for eff_definition in self.eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'vld_eff_vs_eta_star_%s' % eff_definition['name'], '',
                'True #left| #eta^{*} #right|', 'Efficiency',
                xbins=abs_eta_bins)

            eff_plot.write_en = False

            self.eff_vs_eta_star_plots[eff_definition['name']] = eff_plot

    def process(self, data):
        true_values = data[0]
        pred_values = data[1]

        n_entries = true_values.shape[0]
        n_entries_chk = pred_values.shape[0]

        assert n_entries_chk == n_entries, 'Entries don\'t match in length'

        for i in range(n_entries):
            # Unpack values
            true_pt = true_values[i, 0]
            pred_pt = pred_values[i, 0]
            err_pt = pred_pt - true_pt
            rel_err_pt = err_pt / true_pt

            true_invpt = safe_divide(1, true_pt)
            pred_invpt = safe_divide(1, pred_pt)
            err_invpt = pred_invpt - true_invpt

            true_dxy = true_values[i, 1]
            pred_dxy = pred_values[i, 1]
            err_dxy = pred_dxy - true_dxy

            true_eta_star = abs(true_values[i, 2])
            true_dz = abs(true_values[i, 3])

            event = {
                'pt': pred_pt,
                'dxy': pred_dxy,
                'true_pt': true_pt,
                'true_dxy': true_dxy,
                'true_eta_star': true_eta_star,
                'true_dz': true_dz,
            }

            # Distributions
            self.pt_pred_plotter.fill(pred_pt)

            if pred_pt != 0:
                self.invpt_pred_plotter.fill(pred_invpt)

            self.dxy_pred_plotter.fill(pred_dxy)

            self.pt_true_plotter.fill(true_pt)

            if true_pt != 0:
                self.invpt_true_plotter.fill(true_invpt)

            self.dxy_true_plotter.fill(true_dxy)

            # pT Bins
            for pt_range in pt_ranges:
                pt_low = pt_range[0]
                pt_up = pt_range[1]

                if not (pt_low <= true_pt and (pt_up is None or true_pt < pt_up)):
                    continue

                pt_err_plot = self.param_pt_err_plotters[pt_range]
                pt_err_plot.fill(err_pt)

                pt_rerr_plot = self.param_pt_rerr_plotters[pt_range]
                pt_rerr_plot.fill(rel_err_pt)

                invpt_err_plot = self.param_invpt_err_plotters[pt_range]
                invpt_err_plot.fill(err_invpt)

                dxy_err_plot = self.param_dxy_err_plotters[pt_range]
                dxy_err_plot.fill(err_dxy)

                self.pt_abs_err[pt_range].append(err_invpt)
                self.sqr_sum_count[pt_range] += 1
                self.pt_err_sqr_sum[pt_range] += (err_pt ** 2)
                self.pt_rerr_sqr_sum[pt_range] += (rel_err_pt ** 2)
                self.invpt_err_sqr_sum[pt_range] += (err_invpt ** 2)
                self.dxy_err_sqr_sum[pt_range] += (err_dxy ** 2)

            # True
            self.true_pt_vs_dxy_plotter.fill(true_dxy, true_pt)

            # pT
            self.vld_pt_plotter.fill(true_pt, pred_pt)
            self.vld_pt_vs_dxy_plotter.fill(true_dxy, pred_pt)
            self.vld_pt_vs_eta_star_plotter.fill(true_eta_star, pred_pt)
            self.vld_pt_err_plotter.fill(true_pt, err_pt)
            self.vld_pt_err_vs_dxy_plotter.fill(true_dxy, err_pt)
            self.vld_pt_err_vs_eta_star_plotter.fill(true_eta_star, err_pt)

            self.vld_pt_zoom_plotter.fill(true_pt, pred_pt)
            self.vld_pt_err_zoom_plotter.fill(true_pt, err_pt)

            # Inv. pT
            if true_pt != 0 and pred_pt != 0:
                self.vld_invpt_plotter.fill(true_invpt, pred_invpt)
                self.vld_invpt_err_plotter.fill(true_invpt, err_invpt)

            # dxy
            self.vld_dxy_plotter.fill(true_dxy, pred_dxy)
            self.vld_dxy_err_plotter.fill(true_dxy, err_dxy)

            # Efficiency
            for eff_definition in self.eff_definitions:
                passes_denominator = eff_definition['denominator'](event)

                if not passes_denominator:
                    continue

                passes_numerator = eff_definition['numerator'](event)

                # Eff vs pt
                eff_plot = self.eff_vs_pt_plots[eff_definition['name']]
                eff_plot.fill(passes_numerator, abs(true_pt))

                # Eff vs dxy
                eff_plot = self.eff_vs_dxy_plots[eff_definition['name']]
                eff_plot.fill(passes_numerator, true_dxy)

                # Eff vs eta*
                eff_plot = self.eff_vs_eta_star_plots[eff_definition['name']]
                eff_plot.fill(passes_numerator, true_eta_star)

    def post_production(self):
        #####################################################################
        # RMSE
        #####################################################################
        sums = [
            ('pt_err', self.pt_err_sqr_sum),
            ('pt_rerr', self.pt_rerr_sqr_sum),
            ('invpt_err', self.invpt_err_sqr_sum),
            ('dxy_err', self.dxy_err_sqr_sum)
        ]

        for pt_range in pt_ranges:
            invk = 0.6744897501960817
            pt_abs_err = np.asarray(self.pt_abs_err[pt_range])
            pt_abs_err = pt_abs_err[abs(pt_abs_err) < 0.3]

            mad = np.median(np.abs(pt_abs_err))
            stdev = np.std(pt_abs_err)

            print(mad, stdev, stdev / mad * invk)

        for param, sum in sums:
            rmse_list = list()

            for pt_range in pt_ranges:
                bin_n = self.sqr_sum_count[pt_range]
                bin_sum = sum[pt_range]
                rmse = math.sqrt(safe_divide(bin_sum, bin_n))
                rmse_list.append(rmse)

            get_logger().info('{0} rmse: {1}'.format(param, rmse_list))

        #####################################################################
        # Efficiency Plots
        #####################################################################
        self.plot_efficiencies(
            '',
            lambda eff_def: 'denom_eta_ge_1p24_lt_2p4' in eff_def['name']
                            and 'num_pt_ge_10' not in eff_def['name'],
            lambda eff_def: eff_def['num_label'],
            dxy_plot_en=False,
            eta_star_plot_en=False)

        self.plot_efficiencies(
            '',
            lambda eff_def: 'denom_pt_ge_' in eff_def['name']
                            and '_eta_ge_1p24_lt_2p4' in eff_def['name']
                            and 'num_pt_ge_10' not in eff_def['name'],
            lambda eff_def: eff_def['num_label'],
            pt_plot_en=False)

        self.plot_efficiencies(
            '_pt_ge_10GeV',
            lambda eff_def: 'denom_pt_ge_' in eff_def['name']
                            and '_eta_ge_1p24_lt_2p4' not in eff_def['name']
                            and 'num_pt_ge_10' in eff_def['name'],
            lambda eff_def: eff_def['denom_label'],
            'Eff: p_{T} #geq 10 GeV',
            pt_plot_en=False,
            dxy_plot_en=True,
            eta_star_plot_en=False)

        self.plot_efficiencies(
            '_pt_ge_10GeV',
            lambda eff_def: 'denom_eta_' in eff_def['name']
                            and '_eta_ge_1p24_lt_2p4' not in eff_def['name']
                            and 'num_pt_ge_10' in eff_def['name'],
            lambda eff_def: eff_def['denom_label'],
            'Eff: p_{T} #geq 10 GeV',
            pt_plot_en=True,
            dxy_plot_en=False,
            eta_star_plot_en=False)

        self.plot_efficiencies(
            '_pt_ge_22GeV',
            lambda eff_def: 'denom_pt_ge_' in eff_def['name']
                            and '_eta_ge_1p24_lt_2p4' not in eff_def['name']
                            and 'num_pt_ge_22' in eff_def['name'],
            lambda eff_def: eff_def['denom_label'],
            'Eff: p_{T} #geq 22 GeV',
            pt_plot_en=False,
            eta_star_plot_en=False)

        self.plot_efficiencies(
            '_pt_ge_22GeV',
            lambda eff_def: 'denom_eta_' in eff_def['name']
                            and '_eta_ge_1p24_lt_2p4' not in eff_def['name']
                            and 'num_pt_ge_22' in eff_def['name'],
            lambda eff_def: eff_def['denom_label'],
            'Eff: p_{T} #geq 22 GeV',
            pt_plot_en=True,
            dxy_plot_en=False,
            eta_star_plot_en=False)

    def plot_efficiencies(self, suffix, group_pred, label_fn,
                          comment=None,
                          pt_plot_en=True,
                          dxy_plot_en=True,
                          eta_star_plot_en=True):
        #####################################################################
        # Selection
        #####################################################################
        selected_eff = []

        for eff_definition in self.eff_definitions:
            if not group_pred(eff_definition):
                continue

            selected_eff.append(eff_definition)

        #####################################################################
        # Efficiency vs pT
        #####################################################################
        if pt_plot_en:
            plot_canvas = TCanvas('vld_eff_vs_pt_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_pt_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(pt_eff_bins[0], 0, pt_eff_bins[-1], 1.15, 'Model Validation')
            frame.GetXaxis().SetTitle('True p_{T} [GeV]')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.550
            legend_y0 = 0.225

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.375, legend_y0 + 0.225)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.1)
            legend.SetBorderSize(0)
            legend.SetTextSize(.035)

            cm = iter(cm_tab20)

            # Stack Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_pt_plots[eff_definition['name']].plot.GetPaintedGraph()

                eff_plot.SetMarkerColor(next(cm))
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(next(cm))
                eff_plot.SetLineWidth(2)

                eff_plot.Draw('P SAME')

                legend.AddEntry(eff_plot, label_fn(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('vld_eff_vs_pt' + suffix + '.png')

        #####################################################################
        # Eff vs dxy
        #####################################################################
        if dxy_plot_en:
            plot_canvas = TCanvas('vld_eff_vs_dxy_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_dxy_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(model_val_dxy_bins[0], 0, model_val_dxy_bins[-1], 1.15, 'Model Validation')
            frame.GetXaxis().SetTitle('True #left| d_{xy} #right| [cm]')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.225
            legend_y0 = 0.225

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.375, legend_y0 + 0.225)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.1)
            legend.SetBorderSize(0)
            legend.SetTextSize(.035)

            cm = iter(cm_tab20)

            # Stack Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_dxy_plots[eff_definition['name']].plot.GetPaintedGraph()

                eff_plot.SetMarkerColor(next(cm))
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(next(cm))
                eff_plot.SetLineWidth(2)

                eff_plot.Draw('P SAME')

                legend.AddEntry(eff_plot, label_fn(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('vld_eff_vs_dxy' + suffix + '.png')

        #####################################################################
        # Eff vs eta
        #####################################################################
        if eta_star_plot_en:
            plot_canvas = TCanvas('vld_eff_vs_eta_star_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_eta_star_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(abs_eta_bins[0], 0, abs_eta_bins[-1], 1.15, 'Model Validation')
            frame.GetXaxis().SetTitle('True #left| #eta^{*} #right|')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.450
            legend_y0 = 0.225

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.375, legend_y0 + 0.225)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.1)
            legend.SetBorderSize(0)
            legend.SetTextSize(.035)

            cm = iter(cm_tab20)

            # Stack Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_eta_star_plots[eff_definition['name']].plot.GetPaintedGraph()

                eff_plot.SetMarkerColor(next(cm))
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(next(cm))
                eff_plot.SetLineWidth(2)

                eff_plot.Draw('P SAME')

                legend.AddEntry(eff_plot, label_fn(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('vld_eff_vs_eta_star' + suffix + '.png')
