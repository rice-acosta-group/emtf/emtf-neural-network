import math

from emtfnn.analysis.analyzers import AbstractAnalyzer
from emtfnn.analysis.plotters import *
from emtfnn.analysis.root import cm_tab20
from emtfnn.commons.parameters import *
from emtfnn.commons.tools import safe_divide
from emtfnn.execution.runtime import get_logger


class DxyRateAnalyzer(AbstractAnalyzer):

    def __init__(self, displaced_en=True):
        super().__init__()

        self.displaced_en = displaced_en

        self.max_pT_threshold = 40

        self.base_pT_threshold = 7
        self.base_dxy_threshold = 32

        self.dxy_thresholds = [32,64,96]


    def process(self, data, mode='all'):
        pT = data[0]
        dxy = data[1]
        evt_nums = data[2]

        orbit_freq = 11.246  # kHz
        n_coll_bunches = 2345
        n_zero_bias_events = int(np.max(evt_nums) + 1) # based on number of unique evt numbers

        conv_factor = orbit_freq * n_coll_bunches / n_zero_bias_events # Fails if no events

        # Trigger Thresholds
        for dxy_threshold in self.dxy_thresholds:

            tmp_rate_plotter = self.checkout_plotter(
                Rate1DPlotter,
                'vld_rate_vs_pt_dxyThresh' + str(dxy_threshold) + '_mode_' + mode,
                'NN Rate - dxy Threshold ' + str(dxy_threshold) + 'cm',
                'L1 pT Threshold [GeV]', 'Rate [kHz]',
                xbins=np.linspace(0,self.max_pT_threshold,self.max_pT_threshold + 1),
                min_val=0.1,
                max_val=1000,
                target_val=20,
                logy=True,
                density=False)
        

            # Get the number of trigger events
            for pT_threshold in range(self.max_pT_threshold):

                threshold_mask = (pT >= pT_threshold) & (dxy >= dxy_threshold)
                triggered_evt_nums = evt_nums[threshold_mask]
                num_triggered_evts = len(np.unique(triggered_evt_nums))
                
                rate = conv_factor * num_triggered_evts

                for i in range(int(rate)):
                    tmp_rate_plotter.fill(pT_threshold)

                
                if self.base_pT_threshold == pT_threshold and dxy_threshold == self.base_dxy_threshold:
                    rate_err = math.sqrt(num_triggered_evts) * conv_factor

                    print('\n\n' + 'Mode ' + mode + ' - Rates at Default Threshold','dxy-' + str(dxy_threshold),'pT-' + str(pT_threshold))
                    print('Rate:',rate)
                    print('Rate Error:',rate_err)

                    print('Total Events:', n_zero_bias_events)
                    print('Tracks above threshold:', np.sum(threshold_mask))
                    print('Events Triggered:', num_triggered_evts)


            tmp_rate_plotter.write()




    def post_production(self):
        pass
