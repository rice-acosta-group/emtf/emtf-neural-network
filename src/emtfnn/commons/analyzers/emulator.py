from emtfnn.analysis.analyzers import AbstractAnalyzer
from emtfnn.analysis.plotters import *
from emtfnn.commons.parameters import emu_val_min_threshold, emu_val_mismatch_threshold, emu_val_pt_bins, \
    emu_val_dxy_bins, emu_val_pt_err_bins, emu_val_dxy_err_bins, emu_val_invpt_err_bins, emu_val_invpt_bins
from emtfnn.commons.tools import safe_divide


class EmulatorAnalyzer(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        self.pt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'emu_chk_pt', 'Emulator Check',
            'Workbench p_{T} [GeV]', 'Emulator p_{T} [GeV]',
            xbins=emu_val_pt_bins,
            ybins=emu_val_pt_bins,
            min_val=emu_val_min_threshold,
            log_scale=True)

        self.pt_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'emu_chk_pt_err', 'Emulator Check (Mismatch Only)',
            'Workbench p_{T} [GeV]', 'EM-WB',
            xbins=emu_val_pt_bins,
            ybins=emu_val_pt_err_bins,
            min_val=emu_val_mismatch_threshold,
            log_scale=True)

        self.invpt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'emu_chk_invpt', 'Emulator Check',
            'Workbench 1/p_{T} [GeV^{-1}]', 'Emulator 1/p_{T} [GeV^{-1}]',
            xbins=emu_val_invpt_bins,
            ybins=emu_val_invpt_bins,
            min_val=emu_val_min_threshold,
            log_scale=True)

        self.invpt_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'emu_chk_invpt_err', 'Emulator Check (Mismatch Only)',
            'Workbench 1/p_{T} [GeV^{-1}]', 'EM-WB',
            xbins=emu_val_invpt_bins,
            ybins=emu_val_invpt_err_bins,
            min_val=emu_val_mismatch_threshold,
            log_scale=True)

        self.dxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'emu_chk_dxy', 'Emulator Check',
            'Workbench d_{xy} [cm]', 'Emulator d_{xy} [cm]',
            xbins=emu_val_dxy_bins,
            ybins=emu_val_dxy_bins,
            min_val=emu_val_min_threshold,
            log_scale=True)

        self.dxy_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'emu_chk_dxy_err', 'Emulator Check (Mismatch Only)',
            'Workbench d_{xy} [cm]', 'EM-WB',
            xbins=emu_val_dxy_bins,
            ybins=emu_val_dxy_err_bins,
            min_val=emu_val_mismatch_threshold,
            log_scale=True)

    def process(self, data):
        workbench_pred = data[0]
        emulator_pred = data[1]

        n_entries = workbench_pred.shape[0]
        n_entries_chk = emulator_pred.shape[0]

        assert n_entries_chk == n_entries, 'Entries don\'t match in length'

        for i in range(n_entries):
            workbench_pt = workbench_pred[i, 0]
            emulator_pt = emulator_pred[i, 0]
            err_pt = emulator_pt - workbench_pt

            workbench_invpt = safe_divide(1, workbench_pt)
            emulator_invpt = safe_divide(1, emulator_pt)
            err_invpt = emulator_invpt - workbench_invpt

            workbench_dxy = workbench_pred[i, 1]
            emulator_dxy = emulator_pred[i, 1]
            err_dxy = workbench_dxy - emulator_dxy

            # pT performance
            self.pt_plotter.fill(
                workbench_pt,
                emulator_pt)

            if workbench_pt > 0:
                rel_err_pt = err_pt / workbench_pt

                if abs(rel_err_pt) >= emu_val_mismatch_threshold:
                    self.pt_err_plotter.fill(
                        workbench_pt,
                        err_pt)

            # Inv. pT
            if workbench_invpt > 0:
                rel_err_invpt = err_invpt / workbench_invpt

                self.invpt_plotter.fill(
                    workbench_invpt,
                    emulator_invpt)

                if abs(rel_err_invpt) >= emu_val_mismatch_threshold:
                    self.invpt_err_plotter.fill(
                        workbench_invpt,
                        err_invpt)

            # dxy performance
            self.dxy_plotter.fill(
                workbench_dxy,
                emulator_dxy)

            if workbench_dxy > 0:
                rel_err_dxy = err_dxy / workbench_dxy

                if abs(rel_err_dxy) >= emu_val_mismatch_threshold:
                    self.dxy_err_plotter.fill(
                        workbench_dxy,
                        err_dxy)

    def post_production(self):
        pass
