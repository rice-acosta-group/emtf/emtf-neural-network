import numpy as np


def quantize_value(value, width_bits, int_bits, has_sign_bit=False):
    po2_int = pow(2, int_bits)
    po2_frac = pow(2, width_bits - int_bits - has_sign_bit)

    if has_sign_bit:
        value_sign = np.sign(value)
    else:
        value_sign = 1

    max_int = po2_int - 1
    max_frac = (po2_frac - 1) / po2_frac
    max_val = max_int + max_frac

    value = np.floor(np.clip(np.abs(value), 0, max_val) * po2_frac) / po2_frac

    return value * value_sign


def gen_fixed_width_bin_edges(begin, end, step):
    return np.arange(begin - step / 2, end + step * 3 / 2, step)


def safe_divide(num, denom):
    num, denom = np.asarray(num), np.asarray(denom)

    # IF DENOM IS A SCALAR AND DENOM=0 RETURN A ZERO ARRAY WITH THE SHAPE OF NUM
    # ELSE RETURN NUM DIVIDED BY DENOM
    if denom.ndim == 0:
        return (num - num) if denom == 0 else (num / denom)

    # MAKE SURE THAT X AND Y HAVE THE SAME SHAPE
    if num.ndim > 0 and num.shape != denom.shape:
        raise ValueError('Inconsistent shapes: x.shape={0} y.shape={1}'.format(
            num.shape, denom.shape))

    # TRUE DIVIDE
    mask = (denom == 0)

    # MAKE ALL NUM WHERE DENOM IS 0, 0
    num = num - (num * mask)

    # MAKE ALL THE DENOM WHERE DENOM IS 0, 1
    denom = denom + 1.0 * mask

    return num / denom
