import numpy as np

from emtfnn.commons.tools import gen_fixed_width_bin_edges

me2_z = 808.0

pt_ranges = [(0, None), (0, 10), (10, 20), (20, None)]
dxy_ranges = [(0, None), (0, 10), (10, 20), (20, 32), (32, None)]
eta_thresholds = [1.24, 1.6, 2.1, 2.4]
pt_thresholds = [3, 5, 7, 10, 15, 22]
dxy_thresholds = [16, 32, 48, 64]

pt_eff_bins = np.asarray([
    0., 0.5, 1., 1.5, 2., 2.5, 3., 3.5, 4., 4.5, 5., 5.5, 6., 6.5, 7., 7.5, 8.,
    10., 11., 12, 13, 14., 15, 16., 17, 18., 19., 20.,
    22., 24., 26., 28.,
    30., 34., 38,
    40., 48.,
    60., 80.,
    100., 120])

abs_eta_bins = np.linspace(1.24, 2.4, 100)

# Model Validation
model_val_min_threshold = 1e-5
model_val_mismatch_threshold = 1e-5

model_val_q_bins = gen_fixed_width_bin_edges(-1, 1, 1)

model_val_qpt_bins = gen_fixed_width_bin_edges(-120, 120, 2)
model_val_qpt_err_bins = gen_fixed_width_bin_edges(-120, 120, 2)
model_val_qpt_zoom_bins = gen_fixed_width_bin_edges(-20, 20, 1)
model_val_qpt_err_zoom_bins = gen_fixed_width_bin_edges(-20, 20, 1)
model_val_qinvpt_bins = gen_fixed_width_bin_edges(-0.50, 0.50, 0.02)
model_val_qinvpt_err_bins = gen_fixed_width_bin_edges(-0.50, 0.50, 0.02)

model_val_pt_bins = gen_fixed_width_bin_edges(0, 120, 1)
model_val_pt_err_bins = gen_fixed_width_bin_edges(-120, 120, 2)
model_val_pt_rerr_bins = gen_fixed_width_bin_edges(-1, 2, 0.05)
model_val_pt_zoom_bins = gen_fixed_width_bin_edges(0, 20, 1)
model_val_pt_err_zoom_bins = gen_fixed_width_bin_edges(-20, 20, 1)
model_val_invpt_bins = gen_fixed_width_bin_edges(0, 0.50, 0.01)
model_val_invpt_err_bins = gen_fixed_width_bin_edges(-0.50, 0.50, 0.02)

model_val_dxy_bins = gen_fixed_width_bin_edges(0, 120, 1)
model_val_dxy_err_bins = gen_fixed_width_bin_edges(-120, 120, 2)

model_val_d0_bins = gen_fixed_width_bin_edges(-150, 150, 2)
model_val_d0_err_bins = gen_fixed_width_bin_edges(-200, 200, 2)

model_val_phi_bins = gen_fixed_width_bin_edges(-180, 180, 1)
model_val_phi_err_bins = gen_fixed_width_bin_edges(-180, 180, 2)
model_val_eta_bins = gen_fixed_width_bin_edges(1.24, 2.40, 0.01)
model_val_eta_err_bins = gen_fixed_width_bin_edges(-2.40, 2.40, 0.02)
model_val_theta_bins = gen_fixed_width_bin_edges(0, 60, 1)
model_val_theta_err_bins = gen_fixed_width_bin_edges(-30, 30, 1)

roc_pt_thresholds = [5, 10, 20, 30, 40, 50]
roc_dxy_thresholds = [16, 24, 32, 48, 64, 80]

# Emulator Validation
emu_val_min_threshold = 1e-5
emu_val_mismatch_threshold = 1e-5

emu_val_q_bins = gen_fixed_width_bin_edges(-1, 1, 1)
emu_val_qpt_bins = gen_fixed_width_bin_edges(-120, 120, 2)
emu_val_qpt_err_bins = gen_fixed_width_bin_edges(-120, 120, 2)
emu_val_qinvpt_bins = gen_fixed_width_bin_edges(-0.50, 0.50, 0.02)
emu_val_qinvpt_err_bins = gen_fixed_width_bin_edges(-0.50, 0.50, 0.02)

emu_val_pt_bins = gen_fixed_width_bin_edges(0, 120, 1)
emu_val_pt_err_bins = gen_fixed_width_bin_edges(-120, 120, 2)
emu_val_pt_rerr_bins = gen_fixed_width_bin_edges(-1, 2, 0.05)
emu_val_pt_zoom_bins = gen_fixed_width_bin_edges(0, 20, 1)
emu_val_pt_err_zoom_bins = gen_fixed_width_bin_edges(-20, 20, 1)
emu_val_invpt_bins = gen_fixed_width_bin_edges(0, 0.50, 0.01)
emu_val_invpt_err_bins = gen_fixed_width_bin_edges(-0.50, 0.50, 0.02)

emu_val_dxy_bins = gen_fixed_width_bin_edges(0, 120, 1)
emu_val_dxy_err_bins = gen_fixed_width_bin_edges(-120, 120, 2)

emu_val_phi_bins = gen_fixed_width_bin_edges(-180, 180, 1)
emu_val_phi_err_bins = gen_fixed_width_bin_edges(-180, 180, 2)
emu_val_eta_bins = gen_fixed_width_bin_edges(1.24, 2.40, 0.01)
emu_val_eta_err_bins = gen_fixed_width_bin_edges(-2.40, 2.40, 0.02)
emu_val_theta_bins = gen_fixed_width_bin_edges(0, 60, 1)
emu_val_theta_err_bins = gen_fixed_width_bin_edges(-30, 30, 1)
