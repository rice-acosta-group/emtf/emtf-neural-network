import itertools

import cmsml
import keras
import numpy as np

from emtfnn.execution.runtime import get_logger


#################################################################
# Model Functions
#################################################################

def save_model(model, name='model'):
    # Store model to file
    model.save('model')
    model.save(name + '.h5')
    model.save_weights(name + '_weights.h5')

    # Store model to json
    with open(name + '.json', 'w') as outfile:
        outfile.write(model.to_json())

    # Save Protobuf
    cmsml.tensorflow.save_graph(name + '.pb.txt', model, variables_to_constants=True)
    cmsml.tensorflow.save_graph(name + '.pb', model, variables_to_constants=True)


def load_model(name='model', custom_objects=None):
    model = keras.models.load_model(name + '.h5', custom_objects=custom_objects)
    model.load_weights(name + '_weights.h5')
    return model


def load_protobuf_model(name='model'):
    return cmsml.tensorflow.load_graph(name + '.pb', create_session=True)


#################################################################
# Input Files
#################################################################

def load_data(data_file):
    try:
        get_logger().info('Loading data from {0} ...'.format(data_file))
        loaded = np.load(data_file)
        variables = loaded['variables']
        parameters = loaded['parameters']

        get_logger().info('Loaded the variables with shape {0}'.format(variables.shape))
        get_logger().info('Loaded the parameters with shape {0}'.format(parameters.shape))
    except:
        get_logger().error('Failed to load data from file: {0}'.format(data_file))

    assert variables is not None and parameters is not None \
           and (variables.shape[0] == parameters.shape[0])

    # Return
    return variables, parameters


def train_test_split(*arrays, test_size=0.25, batch_size=32, shuffle=True):
    """Split arrays into train and test subsets."""

    if not len(arrays) >= 2:
        raise ValueError('Expect more than 2 array-like objects.')

    num_samples = arrays[0].shape[0]
    num_train_samples = int(np.ceil(num_samples * (1. - test_size)))
    num_train_samples = int(np.ceil(num_train_samples / float(batch_size)) * batch_size)
    index_array = np.arange(num_samples)

    if shuffle:
        np.random.shuffle(index_array)

    index_array_train = index_array[:num_train_samples]
    index_array_test = index_array[num_train_samples:]
    train_test_pairs = ((arr[index_array_train], arr[index_array_test]) for arr in arrays)

    return tuple(itertools.chain.from_iterable(train_test_pairs))


#################################################################
# Data
#################################################################

def dump_vars_params(vars, param, file_path='dump.tsv'):
    headers = ''
    line_format = ''

    # Build Var Header
    first_head = True

    for i_header in range(vars.shape[1]):
        if first_head:
            first_head = False
        else:
            headers += '\t'
            line_format += '\t'

        headers += 'var%d' % i_header
        line_format += '%s'

    # Build Pred Header
    for i_header in range(param.shape[1]):
        if first_head:
            first_head = False
        else:
            headers += '\t'
            line_format += '\t'

        headers += 'param%d' % i_header
        line_format += '%s'

    # Write
    with open(file_path, 'w') as f:
        f.write('%s\n' % headers)

        for i in range(vars.shape[0]):
            args = tuple(list(vars[i, :]) + list(param[i, :]))
            line = line_format % args
            f.write('%s\n' % line)
