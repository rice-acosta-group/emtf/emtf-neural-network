import math

import numpy as np
from matplotlib import pyplot as plt


def plot_variables(variables, filename='variables.png'):
    n_variables = variables.shape[1]
    n_plot_rows = int(math.ceil(n_variables / 6))
    n_plot_cols = 6

    fig, axs = plt.subplots(n_plot_rows, n_plot_cols,
                            figsize=(n_plot_rows, n_plot_cols * n_plot_rows / n_plot_cols))

    for plot_rows in range(n_plot_rows):
        for plot_cols in range(n_plot_cols):
            i_plot = (plot_rows * n_plot_cols) + plot_cols

            # Get ax
            if axs.ndim == 2:
                ax = axs[plot_rows, plot_cols]
            elif axs.ndim == 1:
                ax = axs[plot_cols]
            else:
                ax = axs

            # Remove Ticks
            ax.set_xticklabels([])
            ax.set_yticklabels([])

            # Short-Circuit: No more variables
            if i_plot >= n_variables:
                break

            # Select Values
            x = variables[:, i_plot]
            x = x[np.bitwise_not(np.isnan(x))]

            # Plot var
            ax.hist(x, color='tab:blue', density=True, bins=32)

    fig.set_figheight(10)
    fig.set_figwidth(10)
    plt.rcParams['figure.dpi'] = 700
    plt.savefig(filename)
