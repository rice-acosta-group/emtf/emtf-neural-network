#ifndef EMTFNN_QPTACTIVATION_H_
#define EMTFNN_QPTACTIVATION_H_

#include "nnet_common.h"
#include <cmath>


namespace nnet {


struct qpt_activation_config {
    static const unsigned n_in = 2;
    typedef ap_fixed<10,1> addr_t; // Address Type
    typedef ap_uint<addr_t::width> addr_int_t; // Address integer type
    typedef ap_uint<8> table_t; // output type
    static const unsigned table_size = (1u << addr_t::width);
};


template <typename CONFIG_T, int N>
void init_qpt_activation_table(typename CONFIG_T::table_t table[N]){
  typename CONFIG_T::addr_t addr_val = 0;

  for (unsigned i = 0; i < N; i++) {
    ap_uint<CONFIG_T::addr_t::width> addr_i = i;    // Cast from unsigned to ap_uint
    addr_val.range() = addr_i.range();  // Reinterpret ap_uint as ap_fixed - (get value addr represents)
    float x = static_cast<float>(addr_val); // Convert to float

    float y = floor((1 / (fabs(x) + 1e-6)) + 1); // Floored Activation Function w/ abs()
    
    // Handle overflow above 8 bits
    if(y >= 255)
        table[i] = 255;
    else
        table[i] = static_cast<typename CONFIG_T::table_t>(y);
  }
}


template<class data_T, class res_T, typename CONFIG_T> void qpt_activation(data_T input[CONFIG_T::n_in], res_T output[CONFIG_T::n_in]){

#ifndef __SYNTHESIS__
      static bool initialized = false; // Saves lots of time in sw
      static typename CONFIG_T::table_t qpt_activation_LUT[CONFIG_T::table_size];
#else
      bool initialized = false;
      typename CONFIG_T::table_t qpt_activation_LUT[CONFIG_T::table_size];
#endif 

    // Use this pragma to force use of LUTRAM
    //#pragma HLS ARRAY_PARTITION variable=qpt_activation_LUT complete dim=0
    #pragma HLS INLINE

    if (!initialized) {
        initialized = true;
        init_qpt_activation_table<CONFIG_T, CONFIG_T::table_size>(qpt_activation_LUT);
    }   


    for(int i=0; i<CONFIG_T::n_in; i++){
        #pragma HLS UNROLL

        // Get pT from NN output - This allows address to be all decimals and 1 sign bit
        if(input[i] >= 2 or input[i] <= -2)
            output[i] = 1; // 1GeV min
        else if(input[i] >= 1 or input[i] <= -1)
            output[i] = 2; // 2GeV if it is between 1 and 2
        else{
            typename CONFIG_T::addr_int_t addr_int;
            typename CONFIG_T::addr_t addr_fixed = input[i];
            addr_int.range() = addr_fixed.range(); // Convert dense output (fixed point number) into a LUT address (same as shifting by number of decimal bits)
            output[i] = qpt_activation_LUT[addr_int]; // res_T should be the same as table_t
        }  
        
    }
 

}



}

#endif
