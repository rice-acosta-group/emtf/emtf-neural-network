#ifndef EMTFNN_D0ACTIVATION_H_
#define EMTFNN_D0ACTIVATION_H_

#include "nnet_common.h"
#include <cmath>


namespace nnet {


struct d0_activation_config {
    static const unsigned n_in = 1;
};


template<class data_T, class res_T, typename CONFIG_T> void d0_activation(data_T input[CONFIG_T::n_in], res_T output[CONFIG_T::n_in]){

    #pragma HLS INLINE

    // Get integer dxy from fixed point d0
    for(int i=0; i<CONFIG_T::n_in; i++){
        #pragma HLS UNROLL    
        
        int d0 = static_cast<int>(static_cast<ap_fixed<24,16>>(input[i]) << 7); // Need enough space for full shift

        if(d0 <= -127 or d0 >= 127)
            output[i] = 127;
        else if(d0 < 0)
            output[i] = static_cast<res_T>(d0 * -1); 
        else
            output[i] = static_cast<res_T>(d0);
    }

}



}

#endif
