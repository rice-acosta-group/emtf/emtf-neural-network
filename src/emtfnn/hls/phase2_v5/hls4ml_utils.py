import os
import hls4ml
import json
from hls4ml.model.profiling import numerical
import matplotlib.pyplot as plt
import numpy as np
from keras import backend as K

cwd = os.getcwd()
hls_headers_folder = cwd + '/../../src/emtfnn/hls/phase2_v5/'

def create_hls_model(model,output_dir='emtfnn_model'):
    default_precision = 'fixed<20,8>' # sets precision for weights, biases, mult and accum results, and layer outputs (unless overwritten)
    
    config = hls4ml.utils.config_from_keras_model(model, 
                                                  granularity='name', 
                                                  default_reuse_factor=1,
                                                  default_precision=default_precision)

    # Since everything is inlined, make sure accum and activation are different bit-widths
    #       - This prevents them from being optimized together (which performs worse)
    config['LayerName']['dense']['Precision']['accum'] = 'fixed<17,7>'
    config['LayerName']['dense']['Precision']['result'] = 'fixed<16,7>'
    config['LayerName']['dense_linear']['Precision']['result'] = 'fixed<16,7>'
    config['LayerName']['activation']['Precision']['result'] = 'fixed<16,7>'

    config['LayerName']['dense_1']['Precision']['accum'] = 'fixed<16,7>'
    config['LayerName']['dense_1']['Precision']['result'] = 'fixed<15,7>'
    config['LayerName']['dense_1_linear']['Precision']['result'] = 'fixed<15,7>'
    config['LayerName']['activation_1']['Precision']['result'] = 'fixed<15,7>' 

    config['LayerName']['dense_2']['Precision']['accum'] = 'fixed<15,9>'
    config['LayerName']['dense_2']['Precision']['result'] = 'fixed<14,9>'
    config['LayerName']['dense_2_linear']['Precision']['result'] = 'fixed<14,9>'
    config['LayerName']['activation_2']['Precision']['result'] = 'fixed<14,9>'

    config['LayerName']['dense_3']['Precision']['accum'] = 'fixed<14,12>'
    config['LayerName']['dense_3']['Precision']['result'] = 'fixed<10,10,AP_TRN,AP_SAT>'
    config['LayerName']['dense_3_linear']['Precision']['result'] = 'fixed<10,10>'

    # Set input and output precision to uints
    first_layer_name = list(config['LayerName'].keys())[0]
    last_layer_name = list(config['LayerName'].keys())[-1]

    # Inputs
    config['LayerName'][first_layer_name]['Precision']['result'] = 'uint<13>'
    
    # Add tracing for profiling
    config['Model']['TraceOutput'] = True
    for layer_name in config['LayerName']:
        config['LayerName'][layer_name]['Trace'] = True
    
    #Last Layer
    config['LayerName'][last_layer_name]['Precision']['result'] = 'uint<8>'
    config['LayerName'][last_layer_name]['Trace'] = False

    # pretty print config
    print(json.dumps(config, indent=4))

    hls_model = hls4ml.converters.convert_from_keras_model(model,
                                                           output_dir=output_dir,
                                                           project_name='emtfnn',
                                                           hls_config=config,
                                                           backend='Vivado',
                                                           part='xc7vx690tffg1927-2',
                                                           clock_period = 8,
                                                           io_type='io_parallel',
                                                          )
    
    hls_model.compile()
    
    return hls_model


def add_hybrid_activation_to_hls4ml():
    # hls4ml layer implementation
    class HHybridActivation(hls4ml.model.layers.Layer):
        def initialize(self):
            inp = self.get_input_variable()
            shape = inp.shape
            dims = inp.dim_names
            self.add_output_variable(shape, dims)

    class HD0Activation(hls4ml.model.layers.Layer):
        def initialize(self):
            inp = self.get_input_variable()
            shape = inp.shape
            dims = inp.dim_names
            self.add_output_variable(shape, dims)

    class HQPtActivation(hls4ml.model.layers.Layer):
        def initialize(self):
            inp = self.get_input_variable()
            shape = inp.shape
            dims = inp.dim_names
            self.add_output_variable(shape, dims)
        
        
    # Parser for converter
    def parse_hybridAct_layer(keras_layer, input_names, input_shapes, data_reader):
        layer = {}
        layer['class_name'] = 'HHybridActivation'
        layer['name'] = keras_layer['config']['name']
        layer['n_in'] = input_shapes[0][1]

        if input_names is not None:
            layer['inputs'] = input_names

        return layer, [shape for shape in input_shapes[0]]
    
    # Parser for converter
    def parse_d0Act_layer(keras_layer, input_names, input_shapes, data_reader):
        layer = {}
        layer['class_name'] = 'HD0Activation'
        layer['name'] = keras_layer['config']['name']
        layer['n_in'] = input_shapes[0][1]

        if input_names is not None:
            layer['inputs'] = input_names

        return layer, [shape for shape in input_shapes[0]]

    # Parser for converter
    def parse_qptAct_layer(keras_layer, input_names, input_shapes, data_reader):
        layer = {}
        layer['class_name'] = 'HQPtActivation'
        layer['name'] = keras_layer['config']['name']
        layer['n_in'] = input_shapes[0][1]

        if input_names is not None:
            layer['inputs'] = input_names

        return layer, [shape for shape in input_shapes[0]]



    hybridAct_config_template = """
        struct config{index} : nnet::hybrid_activation_config {{
            static const unsigned n_in = {n_in};
            typedef ap_uint<8> table_t; // output type
            static const unsigned table_size = (1u << 10); 
        }};\n
    """

    d0Act_config_template = """
        struct config{index} : nnet::d0_activation_config {{
            static const unsigned n_in = {n_in}; 
        }};\n
    """

    qptAct_config_template = """
        struct config{index} : nnet::qpt_activation_config {{
            static const unsigned n_in = {n_in};
            typedef ap_fixed<10,1> addr_t; // Address Type
            typedef ap_uint<addr_t::width> addr_int_t; // Address integer type
            typedef ap_uint<8> table_t; // output type
            static const unsigned table_size = (1u << addr_t::width); 
        }};\n
    """

    hybridAct_function_template = 'nnet::hybrid_activation<{input_t}, {output_t}, {config}>({input}, {output});'
    hybridAct_include_list = ['nnet_utils/nnet_hybrid_activation.h']

    d0Act_function_template = 'nnet::d0_activation<{input_t}, {output_t}, {config}>({input}, {output});'
    d0Act_include_list = ['nnet_utils/nnet_d0_activation.h']

    qptAct_function_template = 'nnet::qpt_activation<{input_t}, {output_t}, {config}>({input}, {output});'
    qptAct_include_list = ['nnet_utils/nnet_qpt_activation.h']


    class HHybridConfigTemplate(hls4ml.backends.template.LayerConfigTemplate):
        def __init__(self):
            super().__init__(HHybridActivation)
            self.template = hybridAct_config_template

        def format(self, node):
            params = self._default_config_params(node)
            return self.template.format(**params)

    class HD0ConfigTemplate(hls4ml.backends.template.LayerConfigTemplate):
        def __init__(self):
            super().__init__(HD0Activation)
            self.template = d0Act_config_template

        def format(self, node):
            params = self._default_config_params(node)
            return self.template.format(**params)
        
    class HQPtConfigTemplate(hls4ml.backends.template.LayerConfigTemplate):
        def __init__(self):
            super().__init__(HQPtActivation)
            self.template = qptAct_config_template

        def format(self, node):
            params = self._default_config_params(node)
            return self.template.format(**params)
        


    class HHybridFunctionTemplate(hls4ml.backends.template.FunctionCallTemplate):
        def __init__(self):
            super().__init__(HHybridActivation, include_header=hybridAct_include_list)
            self.template = hybridAct_function_template

        def format(self, node):
            params = self._default_function_params(node)
            return self.template.format(**params)

    class HD0FunctionTemplate(hls4ml.backends.template.FunctionCallTemplate):
        def __init__(self):
            super().__init__(HD0Activation, include_header=d0Act_include_list)
            self.template = d0Act_function_template

        def format(self, node):
            params = self._default_function_params(node)
            return self.template.format(**params)
        
    
    class HQPtFunctionTemplate(hls4ml.backends.template.FunctionCallTemplate):
        def __init__(self):
            super().__init__(HQPtActivation, include_header=qptAct_include_list)
            self.template = qptAct_function_template

        def format(self, node):
            params = self._default_function_params(node)
            return self.template.format(**params)


        
    # Register the Hybrid Layer
    hls4ml.converters.register_keras_layer_handler('HybridActivation', parse_hybridAct_layer)
    hls4ml.model.layers.register_layer('HHybridActivation', HHybridActivation)
    backend = hls4ml.backends.get_backend('Vivado')
    backend.register_template(HHybridConfigTemplate)
    backend.register_template(HHybridFunctionTemplate)
    backend.register_source(hls_headers_folder + 'nnet_hybrid_activation.h')

    # Register the D0 Layer
    hls4ml.converters.register_keras_layer_handler('D0Activation', parse_d0Act_layer)
    hls4ml.model.layers.register_layer('HD0Activation', HD0Activation)
    backend = hls4ml.backends.get_backend('Vivado')
    backend.register_template(HD0ConfigTemplate)
    backend.register_template(HD0FunctionTemplate)
    backend.register_source(hls_headers_folder + '/nnet_d0_activation.h')

    # Register the QPT Layer
    hls4ml.converters.register_keras_layer_handler('QPtActivation', parse_qptAct_layer)
    hls4ml.model.layers.register_layer('HQPtActivation', HQPtActivation)
    backend = hls4ml.backends.get_backend('Vivado')
    backend.register_template(HQPtConfigTemplate)
    backend.register_template(HQPtFunctionTemplate)
    backend.register_source(hls_headers_folder + '/nnet_qpt_activation.h')


def get_activation_ranges(kmodel, x_train, x_test):
    # Use both x_train and x_test so you have as many tracks as possible.
    #   - This info is used to set integer bit-widths
    print('Checking Activation Ranges')
    for i in range(len(kmodel.layers)):
        get_layer_output = K.function([kmodel.layers[0].input], [kmodel.layers[i].output])
        layer_output = get_layer_output([np.concatenate((x_train,x_test),axis=0)])[0]
        print('Layer',i,'\tMax:',np.max(layer_output), '\tAbs max:',np.max(np.abs(layer_output)), '\tMin:',np.min(layer_output))


def get_hls4ml_profile(kmodel, hls_model, x_test):
    plots = numerical(model=kmodel, hls_model=hls_model, X=x_test)
    for i,fig in enumerate(plots):
        fig.savefig('hls4ml_profile_' + str(i) + '.png') 


def plot_keras_vs_hls(kmodel, hls_model, x_test):
    kmodel_preds = kmodel.predict(x_test)
    hls_preds = hls_model.predict(np.ascontiguousarray(x_test))

    plt.clf() # Clear plot
    plt.scatter(kmodel_preds[:,0], hls_preds[:,0],s=0.2,)
    plt.xlim([0,260])
    plt.ylim([0,260])
    plt.grid(True)
    plt.xlabel ('Keras Output')
    plt.ylabel ('HLS Output')
    plt.savefig('kmodel_vs_hlsmodel_pT.png')
    plt.clf()

    plt.scatter(kmodel_preds[:,0], hls_preds[:,0],s=0.2,)
    plt.xlim([0,30])
    plt.ylim([0,30])
    plt.grid(True)
    plt.xlabel ('Keras Output')
    plt.ylabel ('HLS Output')
    plt.savefig('kmodel_vs_hlsmodel_pT_zoom.png')
    plt.clf()

    h, _,_,_ = plt.hist2d(kmodel_preds[:,0], hls_preds[:,0],bins=np.arange(35))
    plt.xlim([0,30])
    plt.ylim([0,30])
    plt.xlabel ('Keras Output')
    plt.ylabel ('HLS Output')
    plt.savefig('kmodel_vs_hlsmodel_hist_pT_zoom.png')
    plt.clf()

    h = np.rot90(h)
    np.savetxt('pt_hist.txt',h,fmt='%16d')


    plt.scatter(kmodel_preds[:,1], hls_preds[:,1],s=0.2)
    plt.xlim([0,130])
    plt.ylim([0,130])
    plt.grid(True)
    plt.xlabel ('Keras Output')
    plt.ylabel ('HLS Output')
    plt.savefig('kmodel_vs_hlsmodel_dxy.png')
    plt.clf()

    plt.hist2d(kmodel_preds[:,1], hls_preds[:,1],bins=np.arange(130))
    plt.xlim([0,130])
    plt.ylim([0,130])
    plt.grid(True)
    plt.xlabel ('Keras Output')
    plt.ylabel ('HLS Output')
    plt.savefig('kmodel_vs_hlsmodel_hist_dxy.png')
    plt.clf()


