import os
import hls4ml
import json
from hls4ml.model.profiling import numerical
import matplotlib.pyplot as plt
import numpy as np
from keras import backend as K


def create_hls_model(model,output_dir='emtfnn_model'):
    default_precision = 'fixed<25,9>' # sets precision for weights, biases, mult and accum results, and layer outputs (unless overwritten)
    
    config = hls4ml.utils.config_from_keras_model(model, 
                                                  granularity='name', 
                                                  default_reuse_factor=1,
                                                  default_precision=default_precision)

    # Overwrite specific precisions
    config['Model']['Precision'] = 'fixed<25,10>'
    config['LayerName']['dense_2']['Precision']['result'] = 'fixed<24,9,AP_TRN, AP_SAT>'
    config['LayerName']['activation']['Precision']['result'] = 'fixed<18,8>'
    config['LayerName']['activation_1']['Precision']['result'] = 'fixed<18,8>'

    # Set input and output precision to uints
    first_layer_name = list(config['LayerName'].keys())[0]
    last_layer_name = list(config['LayerName'].keys())[-1]

    # Inputs
    config['LayerName'][first_layer_name]['Precision']['result'] = 'uint<13>'
    
    # Add tracing for profiling
    config['Model']['TraceOutput'] = True
    for layer_name in config['LayerName']:
        config['LayerName'][layer_name]['Trace'] = True
    
    #Last Layer
    config['LayerName'][last_layer_name]['Precision']['result'] = 'uint<8>'
    config['LayerName'][last_layer_name]['Trace'] = False

    # pretty print config
    print(json.dumps(config, indent=4))

    hls_model = hls4ml.converters.convert_from_keras_model(model,
                                                           output_dir=output_dir,
                                                           project_name='emtfnn',
                                                           hls_config=config,
                                                           backend='Vivado',
                                                           part='xc7vx690tffg1927-2',
                                                           clock_period = 7.5,
                                                           io_type='io_parallel',
                                                          )
    
    hls_model.compile()
    
    return hls_model


def get_activation_ranges(kmodel, x_train, x_test):
    # Use both x_train and x_test so you have as many tracks as possible.
    #   - This info is used to set integer bit-widths
    print('Checking Activation Ranges')
    for i in range(len(kmodel.layers)):
        get_layer_output = K.function([kmodel.layers[0].input], [kmodel.layers[i].output])
        layer_output = get_layer_output([np.concatenate((x_train,x_test),axis=0)])[0]
        print('Layer',i,'\tMax:',np.max(layer_output), '\tAbs max:',np.max(np.abs(layer_output)), '\tMin:',np.min(layer_output))


def get_hls4ml_profile(kmodel, hls_model, x_test):
    plots = numerical(model=kmodel, hls_model=hls_model, X=x_test)
    for i,fig in enumerate(plots):
        fig.savefig('hls4ml_profile_' + str(i) + '.png') 


def plot_keras_vs_hls(kmodel, hls_model, x_test):
    kmodel_preds = kmodel.predict(x_test)
    hls_preds = hls_model.predict(np.ascontiguousarray(x_test))

    plt.clf() # Clear plot
    plt.scatter(kmodel_preds[:,0], hls_preds[:,0],s=0.2,)
    plt.xlim([0,260])
    plt.ylim([0,260])
    plt.grid(True)
    plt.xlabel ('Keras Output')
    plt.ylabel ('HLS Output')
    plt.savefig('kmodel_vs_hlsmodel_pT.png')
    plt.clf()

    plt.scatter(kmodel_preds[:,0], hls_preds[:,0],s=0.2,)
    plt.xlim([0,30])
    plt.ylim([0,30])
    plt.grid(True)
    plt.xlabel ('Keras Output')
    plt.ylabel ('HLS Output')
    plt.savefig('kmodel_vs_hlsmodel_pT_zoom.png')
    plt.clf()

    h, _,_,_ = plt.hist2d(kmodel_preds[:,0], hls_preds[:,0],bins=np.arange(35))
    plt.xlim([0,30])
    plt.ylim([0,30])
    plt.xlabel ('Keras Output')
    plt.ylabel ('HLS Output')
    plt.savefig('kmodel_vs_hlsmodel_hist_pT_zoom.png')
    plt.clf()

    h = np.rot90(h)
    np.savetxt('pt_hist.txt',h,fmt='%16d')


    plt.scatter(kmodel_preds[:,1], hls_preds[:,1],s=0.2)
    plt.xlim([0,130])
    plt.ylim([0,130])
    plt.grid(True)
    plt.xlabel ('Keras Output')
    plt.ylabel ('HLS Output')
    plt.savefig('kmodel_vs_hlsmodel_dxy.png')
    plt.clf()

    plt.hist2d(kmodel_preds[:,1], hls_preds[:,1],bins=np.arange(130))
    plt.xlim([0,130])
    plt.ylim([0,130])
    plt.grid(True)
    plt.xlabel ('Keras Output')
    plt.ylabel ('HLS Output')
    plt.savefig('kmodel_vs_hlsmodel_hist_dxy.png')
    plt.clf()



