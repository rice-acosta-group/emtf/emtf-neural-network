import json
from emtfnn.hls.phase1_v4.hls4ml_utils import get_activation_ranges, create_hls_model, plot_keras_vs_hls, \
                                            get_hls4ml_profile

run_check_activation_ranges = True
run_hls4ml_profiling = True
run_synthesize_hls_model = False


def build_and_test_hls_model(kmodel, x_train, x_test):

    # Check activation ranges
    if run_check_activation_ranges:
        get_activation_ranges(kmodel, x_train, x_test)
    

    # Build the hls model using hls4ml
    hls_model = create_hls_model(kmodel, output_dir='emtfnn_model')


    # Run hls4ml profiling tool
    if run_hls4ml_profiling:
        get_hls4ml_profile(kmodel, hls_model, x_test)


    # Run keras vs hls plots
    plot_keras_vs_hls(kmodel, hls_model, x_test)


    # Synthesize the model    
    if run_synthesize_hls_model:
        results = hls_model.build(csim=True,vsynth=True, export=False)

        # Print and save results
        json_string = json.dumps(results, indent=4)
        print(json_string)

        with open('hls_build_report.json', 'w') as outfile:
            outfile.write(json_string)

    return hls_model
    
