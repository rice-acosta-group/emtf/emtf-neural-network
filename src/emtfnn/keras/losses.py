import tensorflow as tf
from keras import backend
from tensorflow.python.framework import ops
from tensorflow.python.ops import math_ops


def huber_loss(y_true, y_pred, delta=1.345):
    x = backend.abs(y_true - y_pred)
    squared_loss = 0.5 * backend.square(x)
    absolute_loss = delta * (x - 0.5 * delta)
    xx = tf.where(x < delta, squared_loss, absolute_loss)  # needed for tensorflow

    return backend.mean(xx, axis=-1)


def huber_error(y_true, y_pred, delta):
    x = backend.abs(y_true - y_pred)
    squared_loss = 0.5 * backend.square(x)
    absolute_loss = delta * (x - 0.5 * delta)
    xx = tf.where(x < delta, squared_loss, absolute_loss)  # needed for tensorflow

    return xx


def logcosh_error(y_true, y_pred):
    y_pred = ops.convert_to_tensor(y_pred)
    y_true = math_ops.cast(y_true, y_pred.dtype)

    def _logcosh(x):
        return (x + math_ops.softplus(-2. * x) - math_ops.cast(
            math_ops.log(2.), x.dtype))

    return _logcosh(y_pred - y_true)
