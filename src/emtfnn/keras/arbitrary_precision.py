import tensorflow as tf

from emtfnn.keras.math_utils import clip_by_value_preserve_gradient


def clip_uint(int_val, bit_width):
    max = (1 << bit_width) - 1

    return clip_by_value_preserve_gradient(int_val, 0, max)


def clip_int(uint_val, bit_width):
    po2 = (1 << (bit_width - 1))

    return clip_by_value_preserve_gradient(uint_val, -po2, po2 - 1)


def wrap_uint(int_val, bit_width):
    int_casted = tf.cast(int_val, tf.int32)
    po2 = tf.cast(1 << bit_width, tf.int32)

    n_wrap = tf.cast(tf.math.ceil(tf.abs(int_casted) / po2), tf.int32)
    neg_sol = int_casted + po2 * n_wrap

    n_wrap = tf.cast(tf.math.floor(tf.abs(int_casted) / po2), tf.int32)
    pos_sol = int_casted - po2 * n_wrap

    wrapped = tf.where(int_casted < 0, neg_sol, pos_sol)
    wrapped = tf.cast(wrapped, int_val.dtype)

    return int_val + tf.stop_gradient(wrapped - int_val)


def convert_uint_to_int(uint_val, bit_width):
    uint_casted = tf.cast(uint_val, tf.int32)

    uint_casted = wrap_uint(uint_casted, bit_width)

    neg_mask = tf.cast(1 << (bit_width - 1), tf.int32)
    pos_mask = tf.cast(neg_mask - 1, tf.int32)

    neg_part = tf.bitwise.bitwise_and(uint_casted, neg_mask)
    pos_part = tf.bitwise.bitwise_and(uint_casted, pos_mask)

    int_val = pos_part - neg_part
    int_val = tf.cast(int_val, uint_val.dtype)

    return uint_val + tf.stop_gradient(int_val - uint_val)


def convert_int_to_uint(int_val, bit_width):
    int_casted = tf.cast(int_val, tf.int32)

    neg_mask = tf.cast(1 << (bit_width - 1), tf.int32)
    pos_mask = tf.cast(neg_mask - 1, tf.int32)

    pos_part = tf.bitwise.bitwise_and(int_casted, pos_mask)

    uint = tf.where(int_casted < 0, neg_mask + pos_part, pos_part)
    uint = wrap_uint(uint, bit_width)
    uint = tf.cast(uint, int_val.dtype)

    return int_val + tf.stop_gradient(uint - int_val)
