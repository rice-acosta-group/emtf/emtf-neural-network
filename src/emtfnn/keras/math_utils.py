import tensorflow as tf


def floor_preserve_gradient(t, scale=1):
    rounded = tf.floor(t / scale) * scale
    return t + tf.stop_gradient(rounded - t)


def round_preserve_gradient(t, scale=1):
    rounded = tf.round(t / scale) * scale
    return t + tf.stop_gradient(rounded - t)


def clip_by_value_preserve_gradient(t, clip_value_min, clip_value_max):
    clip_t = tf.clip_by_value(t, clip_value_min, clip_value_max)

    return t + tf.stop_gradient(clip_t - t)
