import argparse
import os
import sys

import matplotlib
import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt


def main():
    # CONFIGURE RUN
    if sys.version_info[0] < 3:
        raise AssertionError('Please run this code with Python 3.')

    # Get runtime
    from emtfnn.execution import runtime

    # Get plot style and color map
    plt.style.use(runtime.resource('tdrstyle.mplstyle'))

    # Get logger
    from emtfnn.execution.runtime import get_logger

    get_logger().info('Debug Mode       : {}'.format(runtime.debug_en))
    get_logger().info('Using CMSSW      : {}'.format(os.environ.get('CMSSW_VERSION', 'N/A')))
    get_logger().info('Using python     : {}'.format(sys.version.replace('\n', '')))
    get_logger().info('Using numpy      : {}'.format(np.__version__))
    get_logger().info('Using matplotlib : {}'.format(matplotlib.__version__))
    get_logger().info('Using tensorflow : {}'.format(tf.__version__))
    get_logger().info('Using keras      : {}'.format(tf.keras.__version__))
    get_logger().info('Devices          : {}'.format(tf.config.list_physical_devices()))

    # CONFIGURE ROOT
    from emtfnn.analysis import root

    root.configure()

    # CONFIGURE PARSER
    parser = argparse.ArgumentParser(prog=runtime.project_name, description='EMTF Analysis')

    parser.add_argument('--debug', dest='debug_en', action='store_const',
                        const=True, default=False,
                        help='Enable debug mode')

    subparsers = parser.add_subparsers(dest='parser', metavar="COMMAND", help='Command to be run')

    # CONFIGURE COMMANDS
    from emtfnn.commands.check_emulator import command as check_emulator
    from emtfnn.commands.compare_perf import command as compare_perf_command
    from emtfnn.commands.compare_perf_phase_2 import command as compare_perf_p2_command
    from emtfnn.commands.training.endless_v4 import command as endless_v4_command
    from emtfnn.commands.training.endless_v5 import command as endless_v5_command
    from emtfnn.commands.training.unconverted_v4 import command as unconverted_v4_command
    from emtfnn.execution import runtime

    command_entries = {
        # General
        'check-emulator': ('Checks emulator evaluation with workbench evaluation', check_emulator),
        'compare-performance': ('Compare performance between all models', compare_perf_command),
        'compare-phase2-performance': ('Compare performance between phase-2 models', compare_perf_p2_command),
        # Run-3 Models
        'unconverted-v4': ('Train models using unconverted inputs v4', unconverted_v4_command),
        # Phase-2 Models
        'endless-v4': ('Train models using endless v4', endless_v4_command),
        'endless-v5': ('Train models using endless v5', endless_v5_command),
    }

    for key, command_entry in command_entries.items():
        subparser = subparsers.add_parser(key, help=command_entry[0])
        command_entry[1].configure_parser(subparser)

    args = parser.parse_args()

    # DEBUG MODE
    if args.debug_en:
        runtime.debug_en = True

    # RUN COMMAND
    command_entry = command_entries.get(args.parser)

    if command_entry is None:
        parser.print_help()
        exit(1)

    # RUN
    command_entry[1].run(args)
